package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.util.List;

public class ProcessosJudiciais implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numero;
	
	private List<String> assuntos;
	
	public ProcessosJudiciais() {
		super();
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public List<String> getAssuntos() {
		return assuntos;
	}

	public void setAssuntos(List<String> assuntos) {
		this.assuntos = assuntos;
	}

}
