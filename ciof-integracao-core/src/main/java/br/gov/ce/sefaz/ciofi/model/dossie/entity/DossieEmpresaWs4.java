package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscal;

public class DossieEmpresaWs4 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<NotaFiscal> nfsSemregistroSitram;

	public List<NotaFiscal> getNfsSemregistroSitram() {
		return nfsSemregistroSitram;
	}

	public void setNfsSemregistroSitram(List<NotaFiscal> nfsSemregistroSitram) {
		this.nfsSemregistroSitram = nfsSemregistroSitram;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
