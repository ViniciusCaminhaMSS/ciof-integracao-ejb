package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class EmpresaNFe implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String destinatario;
	private String emitente;
	private String numeroChaveAcesso;
	private String ufDestinatario;
	private String ufEmitente;
	
	public String getDestinatario() {
		return destinatario;
	}
	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}
	public String getEmitente() {
		return emitente;
	}
	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}
	
	public String getNumeroChaveAcesso() {
		return numeroChaveAcesso;
	}
	public void setNumeroChaveAcesso(String numeroChaveAcesso) {
		this.numeroChaveAcesso = numeroChaveAcesso;
	}
	public String getUfDestinatario() {
		return ufDestinatario;
	}
	public void setUfDestinatario(String ufDestinatario) {
		this.ufDestinatario = ufDestinatario;
	}
	public String getUfEmitente() {
		return ufEmitente;
	}
	public void setUfEmitente(String ufEmitente) {
		this.ufEmitente = ufEmitente;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
