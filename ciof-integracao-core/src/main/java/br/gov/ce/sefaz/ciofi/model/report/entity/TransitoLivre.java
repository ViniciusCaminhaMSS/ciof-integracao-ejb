package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class TransitoLivre implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String chave;
	private String numero;
	private String acaoFiscal;
	
	public TransitoLivre() {
		super();
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getAcaoFiscal() {
		return acaoFiscal;
	}

	public void setAcaoFiscal(String acaoFiscal) {
		this.acaoFiscal = acaoFiscal;
	}
}
