package br.gov.ce.sefaz.ciofi.model.bean;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.gov.ce.sefaz.ciofi.model.entity.MunicipioSeta;
import br.gov.ce.sefaz.ciofi.model.repository.MunicipioRepository;
import br.gov.ce.sefaz.jee.business.model.BusinessObjectImpl;

@Stateless
public class MunicipioBO extends BusinessObjectImpl<MunicipioSeta> {

	private static String FINDALL_MUNICIPIOS = "SELECT m.codigo as codigo, m.descricao as descricao, m.cep as cep FROM MunicipioSeta m ";
	
	private EntityManager em;
	
	public MunicipioBO() {

	}

	@Inject
	public MunicipioBO(EntityManager em, MunicipioRepository repository) {
		super(repository);
		this.em = em;
	}
	
	
	public List<MunicipioSeta> findAll(){
		return em.createQuery(FINDALL_MUNICIPIOS).getResultList();
	}
	
	
}
