package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class TermosDeAcordo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numTermoAcordo;
	
	private String assunto;
	
	public TermosDeAcordo() {
		super();
	}

	public String getNumTermoAcordo() {
		return numTermoAcordo;
	}

	public void setNumTermoAcordo(String numTermoAcordo) {
		this.numTermoAcordo = numTermoAcordo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

}
