package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class RestResponse implements Serializable {

	private static final long serialVersionUID = 5075915449282386146L;

	// private String [] messages;
	private Result result;

	/*
	 * public String[] getMessages() { return messages; } public void
	 * setMessages(String[] messages) { this.messages = messages; }
	 */
	public Result getResult() {
		return result;
	}

	public void setResult(Result result) {
		this.result = result;
	}

}
