package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Solicitacao implements Serializable {

	private static final long serialVersionUID = 7495668226318780206L;

	private String requestNum;
	private String product;
	private String requestType;
	private String customerId;
	private String helpDeskId;
	private String description;
	private String details;
	private String createdOn;
	private String requestPriority;
	private String engagementId;
	private String requestStatus;

	public String getRequestNum() {
		return requestNum;
	}

	public void setRequestNum(String requestNum) {
		this.requestNum = requestNum;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getRequestType() {
		return requestType;
	}

	public void setRequestType(String requestType) {
		this.requestType = requestType;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getEngagementId() {
		return engagementId;
	}

	public void setEngagementId(String engagementId) {
		this.engagementId = engagementId;
	}

	public String getRequestStatus() {
		return requestStatus;
	}

	public void setRequestStatus(String requestStatus) {
		this.requestStatus = requestStatus;
	}

	public String getHelpDeskId() {
		return helpDeskId;
	}

	public void setHelpDeskId(String helpDeskId) {
		this.helpDeskId = helpDeskId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public String getRequestPriority() {
		return requestPriority;
	}

	public void setRequestPriority(String requestPriority) {
		this.requestPriority = requestPriority;
	}

}
