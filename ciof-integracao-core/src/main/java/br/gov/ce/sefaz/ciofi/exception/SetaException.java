package br.gov.ce.sefaz.ciofi.exception;

import br.gov.ce.sefaz.jee.exception.SefazException;

public class SetaException extends SefazException {

  private static final long serialVersionUID = -2738190227535254489L;

  public SetaException(Throwable ex) {
    super(ex);
  }

  public SetaException(String mensagem) {
    super(mensagem);
  }

  public SetaException(String mensagem, Object... parametros) {
    super(mensagem, parametros);
  }

  public SetaException(String message, Throwable cause) {
    super(message, cause);
  }
}
