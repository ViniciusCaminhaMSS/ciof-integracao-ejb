package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;
import java.util.List;

public class RegistroPlacaNFeUltimasXHorasReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String quantidadeNfs;
	private String valorNfs;
	private List<NotaFiscalSitram> registroPlacaNF;
	private String[] registroPassagemSitram;
	
	
	public String getQuantidadeNfs() {
		return quantidadeNfs;
	}
	public void setQuantidadeNfs(String quantidadeNfs) {
		this.quantidadeNfs = quantidadeNfs;
	}
	public String getValorNfs() {
		return valorNfs;
	}
	public void setValorNfs(String valorNfs) {
		this.valorNfs = valorNfs;
	}
	public List<NotaFiscalSitram> getRegistroPlacaNF() {
		return registroPlacaNF;
	}
	public void setRegistroPlacaNF(List<NotaFiscalSitram> registroPlacaNF) {
		this.registroPlacaNF = registroPlacaNF;
	}
	public String[] getRegistroPassagemSitram() {
		return registroPassagemSitram;
	}
	public void setRegistroPassagemSitram(String[] registroPassagemSitram) {
		this.registroPassagemSitram = registroPassagemSitram;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
	
}
