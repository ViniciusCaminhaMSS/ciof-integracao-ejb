package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class NfsVinculadasMDFe implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String chave;
	
	private String numero;
	
	private String dataEmissao;
	
	private String situacaoDebito;
	
	private String valor;

	public NfsVinculadasMDFe() {
		super();
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getSituacaoDebito() {
		return situacaoDebito;
	}

	public void setSituacaoDebito(String situacaoDebito) {
		this.situacaoDebito = situacaoDebito;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}

}