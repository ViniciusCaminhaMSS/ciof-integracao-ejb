package br.gov.ce.sefaz.ciofi.model.entity;

public class RegimeTributacao {
	
	private String assunto;
	private Integer termoAcordo;
	
	public String getAssunto() {
		return assunto;
	}
	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}
	public Integer getTermoAcordo() {
		return termoAcordo;
	}
	public void setTermoAcordo(Integer termoAcordo) {
		this.termoAcordo = termoAcordo;
	}
	
}
