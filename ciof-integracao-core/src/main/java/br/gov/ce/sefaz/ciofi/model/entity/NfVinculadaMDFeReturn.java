package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;
import java.util.List;

public class NfVinculadaMDFeReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String quantidadeNfs;
	private List<NotaFiscalSitram> nfeVinculadasMDFe;
	
	public String getQuantidadeNfs() {
		return quantidadeNfs;
	}
	public void setQuantidadeNfs(String quantidadeNfs) {
		this.quantidadeNfs = quantidadeNfs;
	}
	public List<NotaFiscalSitram> getNfeVinculadasMDFe() {
		return nfeVinculadasMDFe;
	}
	public void setNfeVinculadasMDFe(List<NotaFiscalSitram> nfeVinculadasMDFe) {
		this.nfeVinculadasMDFe = nfeVinculadasMDFe;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	
}
