package br.gov.ce.sefaz.ciofi.model.entity;

public class ContribuinteSiget {
	private String cnpj;
	private String cgf;
	private String situacaoCnpj;
	private String cnae;
	private String razaoSocial;
	private String nomeFantasia;
	private String ancora;
	private String simplesNacional;
	private String dataSimplesNacional;
	private String regimeRecolhimento;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String municipio;
	private String uf;
	private String cep;
	private String orgaoMonitoramento;

	
	//	private String ;
//	private String ;
//	private String ;
//	private String ;
//	private String ;
//	private String ;
	
	private String cadine;
	private String inscritoNaDivida;
	private String regimeEspecial;
	private String termoDeAcordo;
	private String tipotermoAcordo;
	private String dataInicioAncora;
	private String credenciado;
	private String tipoCredenciamento;
	private String fdi;
	
	public String getCadine() {
		return cadine;
	}
	public void setCadine(String cadine) {
		this.cadine = cadine;
	}
	public String getRegimeEspecial() {
		return regimeEspecial;
	}
	public void setRegimeEspecial(String regimeEspecial) {
		this.regimeEspecial = regimeEspecial;
	}
	public String getTermoDeAcordo() {
		return termoDeAcordo;
	}
	public void setTermoDeAcordo(String termoDeAcordo) {
		this.termoDeAcordo = termoDeAcordo;
	}
	public String getTipotermoAcordo() {
		return tipotermoAcordo;
	}
	public void setTipotermoAcordo(String tipotermoAcordo) {
		this.tipotermoAcordo = tipotermoAcordo;
	}
	public String getDataInicioAncora() {
		return dataInicioAncora;
	}
	public void setDataInicioAncora(String dataInicioAncora) {
		this.dataInicioAncora = dataInicioAncora;
	}
	public String getCredenciado() {
		return credenciado;
	}
	public void setCredenciado(String credenciado) {
		this.credenciado = credenciado;
	}
	public String getTipoCredenciamento() {
		return tipoCredenciamento;
	}
	public void setTipoCredenciamento(String tipoCredenciamento) {
		this.tipoCredenciamento = tipoCredenciamento;
	}
	public String getFdi() {
		return fdi;
	}
	public void setFdi(String fdi) {
		this.fdi = fdi;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCgf() {
		return cgf;
	}
	public void setCgf(String cgf) {
		this.cgf = cgf;
	}
	public String getSituacaoCnpj() {
		return situacaoCnpj;
	}
	public void setSituacaoCnpj(String situacaoCnpj) {
		this.situacaoCnpj = situacaoCnpj;
	}
	public String getCnae() {
		return cnae;
	}
	public void setCnae(String cnae) {
		this.cnae = cnae;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getAncora() {
		return ancora;
	}
	public void setAncora(String ancora) {
		this.ancora = ancora;
	}
	public String getSimplesNacional() {
		return simplesNacional;
	}
	public void setSimplesNacional(String simplesNacional) {
		this.simplesNacional = simplesNacional;
	}
	public String getDataSimplesNacional() {
		return dataSimplesNacional;
	}
	public void setDataSimplesNacional(String dataSimplesNacional) {
		this.dataSimplesNacional = dataSimplesNacional;
	}
	public String getRegimeRecolhimento() {
		return regimeRecolhimento;
	}
	public void setRegimeRecolhimento(String regimeRecolhimento) {
		this.regimeRecolhimento = regimeRecolhimento;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getOrgaoMonitoramento() {
		return orgaoMonitoramento;
	}
	public void setOrgaoMonitoramento(String orgaoMonitoramento) {
		this.orgaoMonitoramento = orgaoMonitoramento;
	}
	public String getInscritoNaDivida() {
		return inscritoNaDivida;
	}
	public void setInscritoNaDivida(String inscritoNaDivida) {
		this.inscritoNaDivida = inscritoNaDivida;
	}
	
	
	
	

}
