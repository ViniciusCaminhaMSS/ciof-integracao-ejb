package br.gov.ce.sefaz.ciofi.model.bean;

import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


public class CdiRepositoryConfig {

  @Produces
  @Dependent
  @PersistenceContext(unitName = "seta-persistence-unit")
  public EntityManager entityManager;

}
