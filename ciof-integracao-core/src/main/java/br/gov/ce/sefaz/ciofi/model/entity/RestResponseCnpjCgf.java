package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class RestResponseCnpjCgf implements Serializable {

	private static final long serialVersionUID = 5075915449282386146L;

	// private String [] messages;
	private ResultCnpjCgf result;

	public ResultCnpjCgf getResult() {
		return result;
	}

	public void setResult(ResultCnpjCgf result) {
		this.result = result;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/*
	 * public String[] getMessages() { return messages; } public void
	 * setMessages(String[] messages) { this.messages = messages; }
	 */

}
