package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;

import br.gov.ce.sefaz.ciofi.model.entity.AutoInfracao;
import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.MandadoSeguranca;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.RegimeTributacao;
import br.gov.ce.sefaz.ciofi.model.entity.TransitoLivre;

public class DossieGeralReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private AutoInfracao autoInfracaoLavrados;
	private EmpresaReturn empresa;
	private MandadoSeguranca mandadoSeguranca;
	private NotaFiscal nfsSemRegistroSitram;
	private EmpresaReturn participacaoOutrasEmpresas;
	private TransitoLivre pendenciaTransitoLivre;
	private RegimeTributacao regimetributacao;
	private EmpresaReturn socioOutrasEmpresas;
	private EmpresaReturn transportadoras;
	
	public AutoInfracao getAutoInfracaoLavrados() {
		return autoInfracaoLavrados;
	}
	public void setAutoInfracaoLavrados(AutoInfracao autoInfracaoLavrados) {
		this.autoInfracaoLavrados = autoInfracaoLavrados;
	}
	public EmpresaReturn getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaReturn empresa) {
		this.empresa = empresa;
	}
	public MandadoSeguranca getMandadoSeguranca() {
		return mandadoSeguranca;
	}
	public void setMandadoSeguranca(MandadoSeguranca mandadoSeguranca) {
		this.mandadoSeguranca = mandadoSeguranca;
	}
	public NotaFiscal getNfsSemRegistroSitram() {
		return nfsSemRegistroSitram;
	}
	public void setNfsSemRegistroSitram(NotaFiscal nfsSemRegistroSitram) {
		this.nfsSemRegistroSitram = nfsSemRegistroSitram;
	}
	public EmpresaReturn getParticipacaoOutrasEmpresas() {
		return participacaoOutrasEmpresas;
	}
	public void setParticipacaoOutrasEmpresas(EmpresaReturn participacaoOutrasEmpresas) {
		this.participacaoOutrasEmpresas = participacaoOutrasEmpresas;
	}
	public TransitoLivre getPendenciaTransitoLivre() {
		return pendenciaTransitoLivre;
	}
	public void setPendenciaTransitoLivre(TransitoLivre pendenciaTransitoLivre) {
		this.pendenciaTransitoLivre = pendenciaTransitoLivre;
	}
	public RegimeTributacao getRegimetributacao() {
		return regimetributacao;
	}
	public void setRegimetributacao(RegimeTributacao regimetributacao) {
		this.regimetributacao = regimetributacao;
	}
	public EmpresaReturn getSocioOutrasEmpresas() {
		return socioOutrasEmpresas;
	}
	public void setSocioOutrasEmpresas(EmpresaReturn socioOutrasEmpresas) {
		this.socioOutrasEmpresas = socioOutrasEmpresas;
	}
	public EmpresaReturn getTransportadoras() {
		return transportadoras;
	}
	public void setTransportadoras(EmpresaReturn transportadoras) {
		this.transportadoras = transportadoras;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
