package br.gov.ce.sefaz.ciofi.model.repository;

import br.gov.ce.sefaz.ciofi.model.entity.MunicipioSeta;
import br.gov.ce.sefaz.jee.business.model.Repository;

public interface MunicipioRepository extends Repository<MunicipioSeta>{

}
