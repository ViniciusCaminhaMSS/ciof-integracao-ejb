package br.gov.ce.sefaz.ciofi.model.entity;

import java.util.List;

public class NfUltimaAcaoFiscal {
	private Integer quantidadeNfs;
	private Double valorNfs;
	private List<NotaFiscalSitram> notasFiscais;
	private Integer codigo;
	private String mensagem;
	
	public Integer getQuantidadeNfs() {
		return quantidadeNfs;
	}
	public void setQuantidadeNfs(Integer quantidadeNfs) {
		this.quantidadeNfs = quantidadeNfs;
	}
	public Double getValorNfs() {
		return valorNfs;
	}
	public void setValorNfs(Double valorNfs) {
		this.valorNfs = valorNfs;
	}
	public List<NotaFiscalSitram> getNotasFiscais() {
		return notasFiscais;
	}
	public void setNotasFiscais(List<NotaFiscalSitram> notasFiscais) {
		this.notasFiscais = notasFiscais;
	}
	public Integer getCodigo() {
		return codigo;
	}
	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}
	public String getMensagem() {
		return mensagem;
	}
	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}
	
	

}
