package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;
import java.util.List;

public class NfeSemRegistroSitramReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<NotaFiscalSiget> nfsSemRegistroSitram;
	
	
	
	public List<NotaFiscalSiget> getNfsSemRegistroSitram() {
		return nfsSemRegistroSitram;
	}
	public void setNfsSemRegistroSitram(List<NotaFiscalSiget> nfsSemRegistroSitram) {
		this.nfsSemRegistroSitram = nfsSemRegistroSitram;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

	
}
