package br.gov.ce.sefaz.ciofi.model.repository;

import br.gov.ce.sefaz.ciofi.model.entity.Ncm;
import br.gov.ce.sefaz.jee.business.model.Repository;

public interface NcmRepository extends Repository<Ncm> {

}
