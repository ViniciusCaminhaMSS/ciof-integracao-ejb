package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.MandadoSeguranca;
import br.gov.ce.sefaz.ciofi.model.entity.RegimeTributacao;

public class DossieEmpresaWs2 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<MandadoSeguranca> mandadoSeguranca;
	private List<RegimeTributacao> regimeTributacao;
	
	public List<MandadoSeguranca> getMandadoSeguranca() {
		return mandadoSeguranca;
	}
	public void setMandadoSeguranca(List<MandadoSeguranca> mandadoSeguranca) {
		this.mandadoSeguranca = mandadoSeguranca;
	}
	public List<RegimeTributacao> getRegimeTributacao() {
		return regimeTributacao;
	}
	public void setRegimeTributacao(List<RegimeTributacao> regimeTributacao) {
		this.regimeTributacao = regimeTributacao;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
