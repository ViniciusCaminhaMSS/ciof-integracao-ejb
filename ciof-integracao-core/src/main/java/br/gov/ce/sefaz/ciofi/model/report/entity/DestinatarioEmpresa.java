package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class DestinatarioEmpresa implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String cnpj;
	
	private String cgf;
	
	private String razaoSocial;
	
	private String atividadeCNAE;
	
	private String situacaoCadastral;

	public DestinatarioEmpresa() {
		super();
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCgf() {
		return cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getAtividadeCNAE() {
		return atividadeCNAE;
	}

	public void setAtividadeCNAE(String atividadeCNAE) {
		this.atividadeCNAE = atividadeCNAE;
	}

	public String getSituacaoCadastral() {
		return situacaoCadastral;
	}

	public void setSituacaoCadastral(String situacaoCadastral) {
		this.situacaoCadastral = situacaoCadastral;
	}

}
