package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Cliente implements Serializable {

	private static final long serialVersionUID = -3017609978461820572L;

	private String customerId;

	private String nome;

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
