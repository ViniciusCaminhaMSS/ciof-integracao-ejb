package br.gov.ce.sefaz.ciofi.model.entity;

public class CnaePrincipal {
	private Integer codigoCNAE;
	private String descricaoCNAE;

	public Integer getCodigoCNAE() {
		return codigoCNAE;
	}

	public void setCodigoCNAE(Integer codigoCNAE) {
		this.codigoCNAE = codigoCNAE;
	}

	public String getDescricaoCNAE() {
		return descricaoCNAE;
	}

	public void setDescricaoCNAE(String descricaoCNAE) {
		this.descricaoCNAE = descricaoCNAE;
	}

}
