package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class NotaFiscalSiget implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String nfe;
	private String chaveAcesso;
	private String dataEmissao;
	private String valorTotal;
	private String situacao;
	private String cnpjEmitente;
	private String tipoDocumento;
	private String tipoOperacao;
	
	public String getNfe() {
		return nfe;
	}
	public void setNfe(String nfe) {
		this.nfe = nfe;
	}
	public String getChaveAcesso() {
		return chaveAcesso;
	}
	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}
	public String getSituacao() {
		return situacao;
	}
	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}
	public String getCnpjEmitente() {
		return cnpjEmitente;
	}
	public void setCnpjEmitente(String cnpjEmitente) {
		this.cnpjEmitente = cnpjEmitente;
	}
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getTipoOperacao() {
		return tipoOperacao;
	}
	public void setTipoOperacao(String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
