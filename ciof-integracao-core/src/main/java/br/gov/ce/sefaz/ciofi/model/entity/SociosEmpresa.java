package br.gov.ce.sefaz.ciofi.model.entity;

public class SociosEmpresa {
	private String nome;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
