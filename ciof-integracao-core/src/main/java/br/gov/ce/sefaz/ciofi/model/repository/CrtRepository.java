package br.gov.ce.sefaz.ciofi.model.repository;

import br.gov.ce.sefaz.ciofi.model.entity.Crt;
import br.gov.ce.sefaz.jee.business.model.Repository;

public interface CrtRepository extends Repository<Crt> {

}
