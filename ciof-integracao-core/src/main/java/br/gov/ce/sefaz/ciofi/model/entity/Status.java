package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Status implements Serializable {

	private static final long serialVersionUID = 5661433893993339826L;

	private String description;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
