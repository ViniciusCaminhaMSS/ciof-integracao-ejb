package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.ContadorBuscarEmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;

public class DossieEmpresaWs1 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private ContadorBuscarEmpresaReturn contador;
	private EmpresaReturn empresa;
	private List<EmpresaReturn> participacaoOutrasEmpresas;
	private List<EmpresaReturn> socioOutrasEmpresas;
	
	public ContadorBuscarEmpresaReturn getContador() {
		return contador;
	}
	public void setContador(ContadorBuscarEmpresaReturn contador) {
		this.contador = contador;
	}
	public EmpresaReturn getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaReturn empresa) {
		this.empresa = empresa;
	}
	public List<EmpresaReturn> getParticipacaoOutrasEmpresas() {
		return participacaoOutrasEmpresas;
	}
	public void setParticipacaoOutrasEmpresas(List<EmpresaReturn> participacaoOutrasEmpresas) {
		this.participacaoOutrasEmpresas = participacaoOutrasEmpresas;
	}
	public List<EmpresaReturn> getSocioOutrasEmpresas() {
		return socioOutrasEmpresas;
	}
	public void setSocioOutrasEmpresas(List<EmpresaReturn> socioOutrasEmpresas) {
		this.socioOutrasEmpresas = socioOutrasEmpresas;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
