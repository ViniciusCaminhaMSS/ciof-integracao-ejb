package br.gov.ce.sefaz.ciofi.model.bean;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.Collection;
import java.util.Map;

import br.gov.ce.sefaz.ciofi.enums.ReportEnum;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.oasis.JROdtExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

public class Report {
	
	private ByteArrayOutputStream byteArray;
	
	public byte[] reportPdf(Collection<?> list, Map<String,Object> parameters, String relatorio) throws JRException {

		byteArray = new ByteArrayOutputStream(); 
		JRPdfExporter exporterPdf = new JRPdfExporter();
		exporterPdf.setExporterInput(new SimpleExporterInput(jasperPrint(list, parameters, relatorio + ReportEnum.JASPER.getName())));
		exporterPdf.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArray));  
		exporterPdf.exportReport();   
		
		return byteArray.toByteArray();
	}
	
	public byte[] reportDocx(Collection<?> list, Map<String,Object> parameters, String relatorio) throws JRException {  

		byteArray = new ByteArrayOutputStream();   
		JROdtExporter exporterDocx = new JROdtExporter();
		exporterDocx.setExporterInput(new SimpleExporterInput(jasperPrint(list, parameters, relatorio + ReportEnum.JASPER.getName())));
		exporterDocx.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArray)); 
		exporterDocx.exportReport(); 
		
		return byteArray.toByteArray();
	}
	
	public byte[] reportXls(Collection<?> list, Map<String,Object> parameters, String relatorio) throws JRException {  
		  
		byteArray = new ByteArrayOutputStream(); 
		JRXlsxExporter exporterXls = new JRXlsxExporter(); 
		exporterXls.setExporterInput(new SimpleExporterInput(jasperPrint(list, parameters, relatorio + ReportEnum.JASPER.getName())));
		exporterXls.setExporterOutput(new SimpleOutputStreamExporterOutput(byteArray));
		exporterXls.exportReport();    
		
		return byteArray.toByteArray();
	}
	
	public JasperPrint jasperPrint(Collection<?> list, Map<String,Object> parameters, String relatorio) throws JRException {
		InputStream jasperStream = this.getClass().getResourceAsStream(relatorio);
		JasperReport jasperReport = (JasperReport) JRLoader.loadObject(jasperStream);
		JRDataSource jrds = new JRBeanCollectionDataSource(list);
		return JasperFillManager.fillReport(jasperReport, parameters, jrds);
	}
}
