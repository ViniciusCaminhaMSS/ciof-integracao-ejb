package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class ResultCnpjCgf implements Serializable {

	private static final long serialVersionUID = -7790634497243977008L;

	private String atividadeCNAE;
	private String bairro;
	private String cgf;
	private String cnpj;
	private Contador contador;
	private String numeroLogradouro;
	private String razaoSocial;
	private String situacaoCadastral;
	
	public String getAtividadeCNAE() {
		return atividadeCNAE;
	}
	public void setAtividadeCNAE(String atividadeCNAE) {
		this.atividadeCNAE = atividadeCNAE;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCgf() {
		return cgf;
	}
	public void setCgf(String cgf) {
		this.cgf = cgf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public Contador getContador() {
		return contador;
	}
	public void setContador(Contador contador) {
		this.contador = contador;
	}
	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}
	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getSituacaoCadastral() {
		return situacaoCadastral;
	}
	public void setSituacaoCadastral(String situacaoCadastral) {
		this.situacaoCadastral = situacaoCadastral;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	

}
