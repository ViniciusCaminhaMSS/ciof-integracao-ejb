package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Veiculo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String placa;
	private String uf;
	private Boolean roubado;
	private String proprietario;
	private Boolean debitoIPVA;
	private BigDecimal totalDebitos;
	
	public Veiculo() {
		super();
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public Boolean getRoubado() {
		return roubado;
	}

	public void setRoubado(Boolean roubado) {
		this.roubado = roubado;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public Boolean getDebitoIPVA() {
		return debitoIPVA;
	}

	public void setDebitoIPVA(Boolean debitoIPVA) {
		this.debitoIPVA = debitoIPVA;
	}

	public BigDecimal getTotalDebitos() {
		return totalDebitos;
	}

	public void setTotalDebitos(BigDecimal totalDebitos) {
		this.totalDebitos = totalDebitos;
	}
}
