package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Produto implements Serializable {

	private static final long serialVersionUID = -3449378789442796536L;

	private String productid;
	private String nomeServico;

	public String getProductid() {
		return productid;
	}

	public void setProductid(String productid) {
		this.productid = productid;
	}

	public String getNomeServico() {
		return nomeServico;
	}

	public void setNomeServico(String nomeServico) {
		this.nomeServico = nomeServico;
	}

}
