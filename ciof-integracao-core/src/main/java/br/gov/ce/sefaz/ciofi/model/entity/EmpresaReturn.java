package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;
import java.util.List;

public class EmpresaReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String atividadeCNAE;
	private String cgf;
	private String cnpj;
	private ContadorBuscarEmpresaReturn contador;
	private Endereco endereco;
	private String razaoSocial;
	private String situacaoCadastral;
	private List<SociosEmpresa> socios;
	private String nomeOrgaoAtuante;
	private String numero;
	
	public String getAtividadeCNAE() {
		return atividadeCNAE;
	}
	public void setAtividadeCNAE(String atividadeCNAE) {
		this.atividadeCNAE = atividadeCNAE;
	}
	public String getCgf() {
		return cgf;
	}
	public void setCgf(String cgf) {
		this.cgf = cgf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public ContadorBuscarEmpresaReturn getContador() {
		return contador;
	}
	public void setContador(ContadorBuscarEmpresaReturn contador) {
		this.contador = contador;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getSituacaoCadastral() {
		return situacaoCadastral;
	}
	public void setSituacaoCadastral(String situacaoCadastral) {
		this.situacaoCadastral = situacaoCadastral;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<SociosEmpresa> getSocios() {
		return socios;
	}
	public void setSocios(List<SociosEmpresa> socios) {
		this.socios = socios;
	}
	public String getNomeOrgaoAtuante() {
		return nomeOrgaoAtuante;
	}
	public void setNomeOrgaoAtuante(String nomeOrgaoAtuante) {
		this.nomeOrgaoAtuante = nomeOrgaoAtuante;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	
	
}
