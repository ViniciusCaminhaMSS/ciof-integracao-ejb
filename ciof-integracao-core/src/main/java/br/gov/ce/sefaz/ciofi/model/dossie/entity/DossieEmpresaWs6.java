package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;

public class DossieEmpresaWs6 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;
	
	private List<EmpresaReturn> transportadoras;

	public List<EmpresaReturn> getTransportadoras() {
		return transportadoras;
	}

	public void setTransportadoras(List<EmpresaReturn> transportadoras) {
		this.transportadoras = transportadoras;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
