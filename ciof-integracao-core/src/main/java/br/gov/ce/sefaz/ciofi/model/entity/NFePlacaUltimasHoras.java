package br.gov.ce.sefaz.ciofi.model.entity;

import java.util.List;

public class NFePlacaUltimasHoras {
	
	private String quantidadeNfs;
	private String valorNfs;
	private List<NotaFiscalSitram> notasFiscais;
	
	public String getQuantidadeNfs() {
		return quantidadeNfs;
	}
	public void setQuantidadeNfs(String quantidadeNfs) {
		this.quantidadeNfs = quantidadeNfs;
	}
	public String getValorNfs() {
		return valorNfs;
	}
	public void setValorNfs(String valorNfs) {
		this.valorNfs = valorNfs;
	}
	public List<NotaFiscalSitram> getNotasFiscais() {
		return notasFiscais;
	}
	public void setNotasFiscais(List<NotaFiscalSitram> notasFiscais) {
		this.notasFiscais = notasFiscais;
	}	
	
}
