package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

public class DossieCpfReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private BigDecimal valorTotalRemetente;
	private BigDecimal valorTotalDestinatario;
	private Contribuinte contribuinte;
	private List<ParticipacaoEmpresa> participacaoEmpresas;
	private List<ParticipouEmpresa> participouEmpresas;
	private List<DestinatarioEmpresa> destinatarioEmpresas;
	private List<RemetenteEmpresa> remetenteEmpresas;
	
	public DossieCpfReport() {
		super();
	}

	public BigDecimal getValorTotalRemetente() {
		return valorTotalRemetente;
	}

	public void setValorTotalRemetente(BigDecimal valorTotalRemetente) {
		this.valorTotalRemetente = valorTotalRemetente;
	}

	public BigDecimal getValorTotalDestinatario() {
		return valorTotalDestinatario;
	}

	public void setValorTotalDestinatario(BigDecimal valorTotalDestinatario) {
		this.valorTotalDestinatario = valorTotalDestinatario;
	}

	public Contribuinte getContribuinte() {
		return contribuinte;
	}

	public void setContribuinte(Contribuinte contribuinte) {
		this.contribuinte = contribuinte;
	}

	public List<ParticipacaoEmpresa> getParticipacaoEmpresas() {
		return participacaoEmpresas;
	}

	public void setParticipacaoEmpresas(List<ParticipacaoEmpresa> participacaoEmpresas) {
		this.participacaoEmpresas = participacaoEmpresas;
	}

	public List<ParticipouEmpresa> getParticipouEmpresas() {
		return participouEmpresas;
	}

	public void setParticipouEmpresas(List<ParticipouEmpresa> participouEmpresas) {
		this.participouEmpresas = participouEmpresas;
	}

	public List<DestinatarioEmpresa> getDestinatarioEmpresas() {
		return destinatarioEmpresas;
	}

	public void setDestinatarioEmpresas(List<DestinatarioEmpresa> destinatarioEmpresas) {
		this.destinatarioEmpresas = destinatarioEmpresas;
	}

	public List<RemetenteEmpresa> getRemetenteEmpresas() {
		return remetenteEmpresas;
	}

	public void setRemetenteEmpresas(List<RemetenteEmpresa> remetenteEmpresas) {
		this.remetenteEmpresas = remetenteEmpresas;
	}
	
}
