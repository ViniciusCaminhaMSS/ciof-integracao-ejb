package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Tipo implements Serializable {

	private static final long serialVersionUID = 1531318080144349573L;

	private String code;
	private String description;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
