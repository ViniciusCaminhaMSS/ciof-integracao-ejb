package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class DadosEmpresaDossiePlacaReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private ContribuinteSiget DadosEmpresa;
	private String regimeEspecial;
	
	public ContribuinteSiget getDadosEmpresa() {
		return DadosEmpresa;
	}
	public void setDadosEmpresa(ContribuinteSiget dadosEmpresa) {
		DadosEmpresa = dadosEmpresa;
	}
	public String getRegimeEspecial() {
		return regimeEspecial;
	}
	public void setRegimeEspecial(String regimeEspecial) {
		this.regimeEspecial = regimeEspecial;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
}
