package br.gov.ce.sefaz.ciofi.model.entity;

public class AutoInfracao {

	private Diligencia diligencia;
	private EmpresaReturn empresa;
	private String nomeOrgaoAtuante;
	private String numero;
	
	public Diligencia getDiligencia() {
		return diligencia;
	}
	public void setDiligencia(Diligencia diligencia) {
		this.diligencia = diligencia;
	}
	public EmpresaReturn getEmpresa() {
		return empresa;
	}
	public void setEmpresa(EmpresaReturn empresa) {
		this.empresa = empresa;
	}
	public String getNomeOrgaoAtuante() {
		return nomeOrgaoAtuante;
	}
	public void setNomeOrgaoAtuante(String nomeOrgaoAtuante) {
		this.nomeOrgaoAtuante = nomeOrgaoAtuante;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	
}
