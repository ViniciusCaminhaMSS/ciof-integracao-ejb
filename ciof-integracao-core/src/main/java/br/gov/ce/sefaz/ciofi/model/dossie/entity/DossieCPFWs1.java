package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.Contribuinte;
import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;

public class DossieCPFWs1 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private Contribuinte contribuinte;
	private List<EmpresaReturn> participacaoEmpresas;
	private List<EmpresaReturn> participouEmpresas;
	
	public Contribuinte getContribuinte() {
		return contribuinte;
	}
	public void setContribuinte(Contribuinte contribuinte) {
		this.contribuinte = contribuinte;
	}
	public List<EmpresaReturn> getParticipacaoEmpresas() {
		return participacaoEmpresas;
	}
	public void setParticipacaoEmpresas(List<EmpresaReturn> participacaoEmpresas) {
		this.participacaoEmpresas = participacaoEmpresas;
	}
	public List<EmpresaReturn> getParticipouEmpresas() {
		return participouEmpresas;
	}
	public void setParticipouEmpresas(List<EmpresaReturn> participouEmpresas) {
		this.participouEmpresas = participouEmpresas;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
		
}
