package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.util.List;

public class DossiePlacaReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String debitoIpva;
	private Integer quantidadeNfs;
	private String valorNfs;
	private Veiculo veiculo;
	private List<String> registroPassagemSitram;
	private List<NfsVinculadasMDFe> nfsVinculadasMDFe;
	private List<NfeVinculadasMDFe> nfeVinculadasMDFe;
	private List<PendenciaTransitoLivre> pendenciaTransitoLivre;
	private List<RegistroPlacaNF> registroPlacaNF;
	private List<PrincipaisCgfs> principaisCgfs;
	
	public DossiePlacaReport() {
		super();
	}

	public String getDebitoIpva() {
		return debitoIpva;
	}

	public void setDebitoIpva(String debitoIpva) {
		this.debitoIpva = debitoIpva;
	}

	public Integer getQuantidadeNfs() {
		return quantidadeNfs;
	}

	public void setQuantidadeNfs(Integer quantidadeNfs) {
		this.quantidadeNfs = quantidadeNfs;
	}

	public String getValorNfs() {
		return valorNfs;
	}

	public void setValorNfs(String valorNfs) {
		this.valorNfs = valorNfs;
	}

	public List<String> getRegistroPassagemSitram() {
		return registroPassagemSitram;
	}

	public void setRegistroPassagemSitram(List<String> registroPassagemSitram) {
		this.registroPassagemSitram = registroPassagemSitram;
	}

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public List<NfeVinculadasMDFe> getNfeVinculadasMDFe() {
		return nfeVinculadasMDFe;
	}

	public void setNfeVinculadasMDFe(List<NfeVinculadasMDFe> nfeVinculadasMDFe) {
		this.nfeVinculadasMDFe = nfeVinculadasMDFe;
	}

	public List<NfsVinculadasMDFe> getNfsVinculadasMDFe() {
		return nfsVinculadasMDFe;
	}

	public void setNfsVinculadasMDFe(List<NfsVinculadasMDFe> nfsVinculadasMDFe) {
		this.nfsVinculadasMDFe = nfsVinculadasMDFe;
	}

	public List<PendenciaTransitoLivre> getPendenciaTransitoLivre() {
		return pendenciaTransitoLivre;
	}

	public void setPendenciaTransitoLivre(List<PendenciaTransitoLivre> pendenciaTransitoLivre) {
		this.pendenciaTransitoLivre = pendenciaTransitoLivre;
	}

	public List<RegistroPlacaNF> getRegistroPlacaNF() {
		return registroPlacaNF;
	}

	public void setRegistroPlacaNF(List<RegistroPlacaNF> registroPlacaNF) {
		this.registroPlacaNF = registroPlacaNF;
	}

	public List<PrincipaisCgfs> getPrincipaisCgfs() {
		return principaisCgfs;
	}

	public void setPrincipaisCgfs(List<PrincipaisCgfs> principaisCgfs) {
		this.principaisCgfs = principaisCgfs;
	}
}
