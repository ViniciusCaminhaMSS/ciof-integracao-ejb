package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Empresa implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private CnaePrincipal cnaePrincipal;
	private String bairro;
	private String cgf;
	private String cnpj;
	private Contador contador;
	private String numeroLogradouro;
	private String razaoSocial;
	private String situacaoCadastral;
//	private String socios;
	
	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCgf() {
		return cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getSituacaoCadastral() {
		return situacaoCadastral;
	}

	public void setSituacaoCadastral(String situacaoCadastral) {
		this.situacaoCadastral = situacaoCadastral;
	}

	public CnaePrincipal getCnaePrincipal() {
		return cnaePrincipal;
	}

	public void setCnaePrincipal(CnaePrincipal cnaePrincipal) {
		this.cnaePrincipal = cnaePrincipal;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Contador getContador() {
		return contador;
	}

	public void setContador(Contador contador) {
		this.contador = contador;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	
}
