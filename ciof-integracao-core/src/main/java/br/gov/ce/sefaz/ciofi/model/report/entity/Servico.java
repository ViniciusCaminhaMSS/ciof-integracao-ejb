package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class Servico implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String codigo;
	private String descricao;
	private String nmcSH;
	private String oCst;
	private String cfop;
	private String unid;
	private String quant;
	private BigDecimal valorUnitario;
	private BigDecimal valorTotal;
	private BigDecimal calculo;
	private BigDecimal icms;
	private BigDecimal ipi;
	private BigDecimal icmsPorcent;
	private BigDecimal ipiPorcent;
	
	public Servico() {
		super();
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getNmcSH() {
		return nmcSH;
	}

	public void setNmcSH(String nmcSH) {
		this.nmcSH = nmcSH;
	}

	public String getoCst() {
		return oCst;
	}

	public void setoCst(String oCst) {
		this.oCst = oCst;
	}

	public String getCfop() {
		return cfop;
	}

	public void setCfop(String cfop) {
		this.cfop = cfop;
	}

	public String getUnid() {
		return unid;
	}

	public void setUnid(String unid) {
		this.unid = unid;
	}

	public String getQuant() {
		return quant;
	}

	public void setQuant(String quant) {
		this.quant = quant;
	}

	public BigDecimal getValorUnitario() {
		return valorUnitario;
	}

	public void setValorUnitario(BigDecimal valorUnitario) {
		this.valorUnitario = valorUnitario;
	}

	public BigDecimal getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(BigDecimal valorTotal) {
		this.valorTotal = valorTotal;
	}

	public BigDecimal getCalculo() {
		return calculo;
	}

	public void setCalculo(BigDecimal calculo) {
		this.calculo = calculo;
	}

	public BigDecimal getIcms() {
		return icms;
	}

	public void setIcms(BigDecimal icms) {
		this.icms = icms;
	}

	public BigDecimal getIpi() {
		return ipi;
	}

	public void setIpi(BigDecimal ipi) {
		this.ipi = ipi;
	}

	public BigDecimal getIcmsPorcent() {
		return icmsPorcent;
	}

	public void setIcmsPorcent(BigDecimal icmsPorcent) {
		this.icmsPorcent = icmsPorcent;
	}

	public BigDecimal getIpiPorcent() {
		return ipiPorcent;
	}

	public void setIpiPorcent(BigDecimal ipiPorcent) {
		this.ipiPorcent = ipiPorcent;
	}
	
}
