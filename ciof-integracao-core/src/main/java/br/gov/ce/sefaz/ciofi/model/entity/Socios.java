package br.gov.ce.sefaz.ciofi.model.entity;

import java.util.List;

public class Socios {
	private String cpfCnpj;
	private String nomeRazaoSocial;
	private String outrasEmpresas;
	private List<SocioParticipacoes> socioParticipacaos;
	
	public String getCpfCnpj() {
		return cpfCnpj;
	}
	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}
	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}
	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}
	public String getOutrasEmpresas() {
		return outrasEmpresas;
	}
	public void setOutrasEmpresas(String outrasEmpresas) {
		this.outrasEmpresas = outrasEmpresas;
	}
	public List<SocioParticipacoes> getSocioParticipacaos() {
		return socioParticipacaos;
	}
	public void setSocioParticipacaos(List<SocioParticipacoes> socioParticipacaos) {
		this.socioParticipacaos = socioParticipacaos;
	}
	
	
}
