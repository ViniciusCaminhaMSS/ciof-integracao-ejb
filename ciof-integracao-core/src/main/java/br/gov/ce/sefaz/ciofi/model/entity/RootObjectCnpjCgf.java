package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class RootObjectCnpjCgf implements Serializable {

	private static final long serialVersionUID = -2830469185486760212L;

	private RestResponseCnpjCgf restResponse;

	public RestResponseCnpjCgf getRestResponse() {
		return restResponse;
	}

	public void setRestResponse(RestResponseCnpjCgf restResponse) {
		this.restResponse = restResponse;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
