package br.gov.ce.sefaz.ciofi.model.entity;

import java.util.List;

public class SociosEmpresaPessoa {
	private List<Pessoa> pessoa;

	public List<Pessoa> getPessoa() {
		return pessoa;
	}

	public void setPessoa(List<Pessoa> pessoa) {
		this.pessoa = pessoa;
	}

	
}
