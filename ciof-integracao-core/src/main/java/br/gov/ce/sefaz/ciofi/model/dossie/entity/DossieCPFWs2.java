package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;

public class DossieCPFWs2 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<EmpresaReturn> destinatarioEmpresas;
	private List<EmpresaReturn> remetenteEmpresas;
	private Double valorTotalDestinatario;
	private Double valorTotalRemetente;
	
	public List<EmpresaReturn> getDestinatarioEmpresas() {
		return destinatarioEmpresas;
	}
	public void setDestinatarioEmpresas(List<EmpresaReturn> destinatarioEmpresas) {
		this.destinatarioEmpresas = destinatarioEmpresas;
	}
	public List<EmpresaReturn> getRemetenteEmpresas() {
		return remetenteEmpresas;
	}
	public void setRemetenteEmpresas(List<EmpresaReturn> remetenteEmpresas) {
		this.remetenteEmpresas = remetenteEmpresas;
	}
	public Double getValorTotalDestinatario() {
		return valorTotalDestinatario;
	}
	public void setValorTotalDestinatario(Double valorTotalDestinatario) {
		this.valorTotalDestinatario = valorTotalDestinatario;
	}
	public Double getValorTotalRemetente() {
		return valorTotalRemetente;
	}
	public void setValorTotalRemetente(Double valorTotalRemetente) {
		this.valorTotalRemetente = valorTotalRemetente;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	} 
 		
}
