package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;
import java.util.List;

public class Pessoa implements Serializable {

	private static final long serialVersionUID = -7790634497243977008L;

	private List<String> nome;

	public List<String> getNome() {
		return nome;
	}

	public void setNome(List<String> nome) {
		this.nome = nome;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	

}
