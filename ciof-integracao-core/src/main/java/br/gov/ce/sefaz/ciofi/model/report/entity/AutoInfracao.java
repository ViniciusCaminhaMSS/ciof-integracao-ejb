package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class AutoInfracao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String orgao;
	private String numero;
	
	public AutoInfracao() {
		super();
	}

	public String getOrgao() {
		return orgao;
	}

	public void setOrgao(String orgao) {
		this.orgao = orgao;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
}
