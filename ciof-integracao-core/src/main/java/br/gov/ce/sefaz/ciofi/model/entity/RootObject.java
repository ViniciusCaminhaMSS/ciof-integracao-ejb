package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class RootObject implements Serializable {

	private static final long serialVersionUID = -2830469185486760212L;

	private RestResponse restResponse;

	public RestResponse getRestResponse() {
		return restResponse;
	}

	public void setRestResponse(RestResponse restResponse) {
		this.restResponse = restResponse;
	}

}
