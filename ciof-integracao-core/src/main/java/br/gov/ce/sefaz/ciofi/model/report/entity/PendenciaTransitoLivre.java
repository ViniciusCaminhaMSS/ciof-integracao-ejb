package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class PendenciaTransitoLivre implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String numeroAcaoFiscal;
	
	private BigDecimal valorTotalAcao;

	public PendenciaTransitoLivre() {
		super();
	}

	public String getNumeroAcaoFiscal() {
		return numeroAcaoFiscal;
	}

	public void setNumeroAcaoFiscal(String numeroAcaoFiscal) {
		this.numeroAcaoFiscal = numeroAcaoFiscal;
	}

	public BigDecimal getValorTotalAcao() {
		return valorTotalAcao;
	}

	public void setValorTotalAcao(BigDecimal valorTotalAcao) {
		this.valorTotalAcao = valorTotalAcao;
	}

}
