package br.gov.ce.sefaz.ciofi.model.entity;

public class Veiculo {

	private boolean debitoIPVA;
	private String placa;
	private String proprietario;
	private boolean roubado;
	private double totalDebitos;
	private String uf;
	
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public boolean isDebitoIPVA() {
		return debitoIPVA;
	}
	public void setDebitoIPVA(boolean debitoIPVA) {
		this.debitoIPVA = debitoIPVA;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public boolean isRoubado() {
		return roubado;
	}
	public void setRoubado(boolean roubado) {
		this.roubado = roubado;
	}
	public double getTotalDebitos() {
		return totalDebitos;
	}
	public void setTotalDebitos(double totalDebitos) {
		this.totalDebitos = totalDebitos;
	}
	
	
}
