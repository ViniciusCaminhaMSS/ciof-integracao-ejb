package br.gov.ce.sefaz.ciofi.model.entity;

public class Diligencia {

	private Integer numero;

	public Integer getNumero() {
		return numero;
	}

	public void setNumero(Integer numero) {
		this.numero = numero;
	}
}
