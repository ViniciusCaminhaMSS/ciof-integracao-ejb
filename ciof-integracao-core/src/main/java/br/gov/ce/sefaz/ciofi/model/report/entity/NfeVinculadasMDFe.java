package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.math.BigDecimal;

public class NfeVinculadasMDFe implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String chave;
	
	private String numero;
	
	private String dataEmissao;
	
	private BigDecimal valor;
	

	public NfeVinculadasMDFe() {
		super();
	}

	public String getChave() {
		return chave;
	}

	public void setChave(String chave) {
		this.chave = chave;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String nummero) {
		this.numero = nummero;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}
}
