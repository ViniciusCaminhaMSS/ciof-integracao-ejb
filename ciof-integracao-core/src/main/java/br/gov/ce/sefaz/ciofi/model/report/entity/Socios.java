package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.util.List;

public class Socios implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String nomeRazaoSocial;
	
	private String cpfCnpj;
	
	private List<SocioParticipacao> socioParticipacaos;
	
	public Socios() {
		super();
	}

	public String getNomeRazaoSocial() {
		return nomeRazaoSocial;
	}

	public void setNomeRazaoSocial(String nomeRazaoSocial) {
		this.nomeRazaoSocial = nomeRazaoSocial;
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public List<SocioParticipacao> getSocioParticipacaos() {
		return socioParticipacaos;
	}

	public void setSocioParticipacaos(List<SocioParticipacao> socioParticipacaos) {
		this.socioParticipacaos = socioParticipacaos;
	}

}
