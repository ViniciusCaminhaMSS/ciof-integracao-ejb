package br.gov.ce.sefaz.ciofi.model.entity;

public class MandadoSeguranca {
	private Integer numeroProcesso;

	public Integer getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(Integer numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}
	
}
