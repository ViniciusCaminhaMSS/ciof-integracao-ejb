package br.gov.ce.sefaz.ciofi.model.entity;

public class NotaFiscalSitram {
	private String chave;
	private String numero;
	private String valor;
	private String dataEmissao;
	private String situacaoDebito;
	private String serie;
	
	
	public String getChave() {
		return chave;
	}
	public void setChave(String chave) {
		this.chave = chave;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getDataEmissao() {
		return dataEmissao;
	}
	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}
	public String getSituacaoDebito() {
		return situacaoDebito;
	}
	public void setSituacaoDebito(String situacaoDebito) {
		this.situacaoDebito = situacaoDebito;
	}
	public String getSerie() {
		return serie;
	}
	public void setSerie(String serie) {
		this.serie = serie;
	}
	
	
	
	
	
}
