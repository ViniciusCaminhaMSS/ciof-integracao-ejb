package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.TransitoLivre;

public class DossieEmpresaWs3 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<TransitoLivre> pendenciatransitoLivre;

	public List<TransitoLivre> getPendenciatransitoLivre() {
		return pendenciatransitoLivre;
	}

	public void setPendenciatransitoLivre(List<TransitoLivre> pendenciatransitoLivre) {
		this.pendenciatransitoLivre = pendenciatransitoLivre;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
