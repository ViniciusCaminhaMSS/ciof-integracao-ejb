package br.gov.ce.sefaz.ciofi.model.entity;

import br.gov.ce.sefaz.jee.business.model.entity.SefazEntity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "NCM", schema = "SETA")
public class Ncm extends SefazEntity {

  private static final long serialVersionUID = -3532322835587297115L;

  private Long id;

  private Integer codigo;

  private String descricao;

  private String descricaoOficial;

  private Boolean isentoIcms;

  private LocalDate validadeFim;

  @Override
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  public Long getId() {
    return id;
  }

  @Override
  public void setId(Long id) {
    this.id = id;
  }

  @Column(name = "COD_NCM")
  public Integer getCodigo() {
    return codigo;
  }

  public void setCodigo(Integer codigo) {
    this.codigo = codigo;
  }

  @Column(name = "DSC_NCM")
  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  @Column(name = "DSC_NCM_OFICIAL")
  public String getDescricaoOficial() {
    return descricaoOficial;
  }

  public void setDescricaoOficial(String descricaoOficial) {
    this.descricaoOficial = descricaoOficial;
  }

  @Column(name = "STA_ISENTO_ICMS")
  public Boolean getIsentoIcms() {
    return isentoIcms;
  }

  public void setIsentoIcms(Boolean isentoIcms) {
    this.isentoIcms = isentoIcms;
  }

  @Column(name = "DAT_VALIDADE_FIM")
  public LocalDate getValidadeFim() {
    return validadeFim;
  }

  public void setValidadeFim(LocalDate validadeFim) {
    this.validadeFim = validadeFim;
  }

  @Override
  public int hashCode() {
    return HashCodeBuilder.reflectionHashCode(this);
  }

  @Override
  public boolean equals(Object obj) {
    return EqualsBuilder.reflectionEquals(this, obj);
  }

  @Override
  public String toString() {
    return ToStringBuilder.reflectionToString(this);
  }
}
