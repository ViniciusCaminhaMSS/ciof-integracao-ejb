package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.AutoInfracao;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscalSitram;
import br.gov.ce.sefaz.ciofi.model.entity.TransitoLivre;
import br.gov.ce.sefaz.ciofi.model.entity.Veiculo;

public class DossiePlacaReturn implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<AutoInfracao> empresasVinculadasAutoInfracao;
	private List<NotaFiscalSitram> nfsVinculadasMDFe;
	private List<NotaFiscal> nfsVinculadasMDFePassagemCE;
	private List<TransitoLivre> pendenciasTransitoLivre;
	private String registroPassagemSitram;
	private List<NotaFiscal> registroPlacaNF;
	private Veiculo veiculo;
	
	
	public List<AutoInfracao> getEmpresasVinculadasAutoInfracao() {
		return empresasVinculadasAutoInfracao;
	}
	public void setEmpresasVinculadasAutoInfracao(List<AutoInfracao> empresasVinculadasAutoInfracao) {
		this.empresasVinculadasAutoInfracao = empresasVinculadasAutoInfracao;
	}
	public List<NotaFiscalSitram> getNfsVinculadasMDFe() {
		return nfsVinculadasMDFe;
	}
	public void setNfsVinculadasMDFe(List<NotaFiscalSitram> nfsVinculadasMDFe) {
		this.nfsVinculadasMDFe = nfsVinculadasMDFe;
	}
	
	public List<NotaFiscal> getNfsVinculadasMDFePassagemCE() {
		return nfsVinculadasMDFePassagemCE;
	}
	public void setNfsVinculadasMDFePassagemCE(List<NotaFiscal> nfsVinculadasMDFePassagemCE) {
		this.nfsVinculadasMDFePassagemCE = nfsVinculadasMDFePassagemCE;
	}
	public String getRegistroPassagemSitram() {
		return registroPassagemSitram;
	}
	public void setRegistroPassagemSitram(String registroPassagemSitram) {
		this.registroPassagemSitram = registroPassagemSitram;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public List<TransitoLivre> getPendenciasTransitoLivre() {
		return pendenciasTransitoLivre;
	}
	public void setPendenciasTransitoLivre(List<TransitoLivre> pendenciasTransitoLivre) {
		this.pendenciasTransitoLivre = pendenciasTransitoLivre;
	}
	public List<NotaFiscal> getRegistroPlacaNF() {
		return registroPlacaNF;
	}
	public void setRegistroPlacaNF(List<NotaFiscal> registroPlacaNF) {
		this.registroPlacaNF = registroPlacaNF;
	}
	
}
