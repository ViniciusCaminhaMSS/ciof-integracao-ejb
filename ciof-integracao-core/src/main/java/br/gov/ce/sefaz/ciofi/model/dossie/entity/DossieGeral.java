package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.Socios;

public class DossieGeral {
	private String cnpj;
	private String cgf;
	private String situacaoCnpj;
	private String regimeRecolhimento;
	private String cnae;
	private String razaoSocial;
	private String nomeFantasia;
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String uf;
	private String municipio;
	private String cep;
	private List<Socios> socios;
	private String orgaoMonitoramento;
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCgf() {
		return cgf;
	}
	public void setCgf(String cgf) {
		this.cgf = cgf;
	}
	public String getSituacaoCnpj() {
		return situacaoCnpj;
	}
	public void setSituacaoCnpj(String situacaoCnpj) {
		this.situacaoCnpj = situacaoCnpj;
	}
	public String getRegimeRecolhimento() {
		return regimeRecolhimento;
	}
	public void setRegimeRecolhimento(String regimeRecolhimento) {
		this.regimeRecolhimento = regimeRecolhimento;
	}
	public String getCnae() {
		return cnae;
	}
	public void setCnae(String cnae) {
		this.cnae = cnae;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComplemento() {
		return complemento;
	}
	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getUf() {
		return uf;
	}
	public void setUf(String uf) {
		this.uf = uf;
	}
	public String getMunicipio() {
		return municipio;
	}
	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public List<Socios> getSocios() {
		return socios;
	}
	public void setSocios(List<Socios> socios) {
		this.socios = socios;
	}
	public String getOrgaoMonitoramento() {
		return orgaoMonitoramento;
	}
	public void setOrgaoMonitoramento(String orgaoMonitoramento) {
		this.orgaoMonitoramento = orgaoMonitoramento;
	}
	
	
	//Aqui viriam tbm os dados do contador,mas no servico do Siget que tenho nao possuem essas informacoes
	
}
