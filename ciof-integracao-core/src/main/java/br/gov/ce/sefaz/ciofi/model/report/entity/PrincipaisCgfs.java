package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class PrincipaisCgfs implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cgfCnpjCpf;
	
	private String razaoSocial;
	
	private String numeroDiligencia;

	public PrincipaisCgfs() {
		super();
	}

	public String getCgfCnpjCpf() {
		return cgfCnpjCpf;
	}

	public void setCgfCnpjCpf(String cgfCnpjCpf) {
		this.cgfCnpjCpf = cgfCnpjCpf;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNumeroDiligencia() {
		return numeroDiligencia;
	}

	public void setNumeroDiligencia(String numeroDiligencia) {
		this.numeroDiligencia = numeroDiligencia;
	}

}
