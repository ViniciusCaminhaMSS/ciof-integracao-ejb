package br.gov.ce.sefaz.ciofi.model.entity;

public class AcaoFiscal {

	private String numero;
	private Double valorTotal;
	
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public Double getValorTotal() {
		return valorTotal;
	}
	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
	
}
