package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;

import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;

public class DossiePlacaWs2 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private EmpresaReturn empresa;

	public EmpresaReturn getEmpresa() {
		return empresa;
	}

	public void setEmpresa(EmpresaReturn empresa) {
		this.empresa = empresa;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
}
