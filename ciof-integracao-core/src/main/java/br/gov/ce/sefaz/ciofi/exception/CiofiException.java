package br.gov.ce.sefaz.ciofi.exception;

import br.gov.ce.sefaz.jee.exception.SefazException;

public class CiofiException extends SefazException {

	private static final long serialVersionUID = -2738190227535254489L;

	public CiofiException(Throwable e) {
		super(e);
	}

	public CiofiException(String mensagem) {
		super(mensagem);
	}

	public CiofiException(String mensagem, Object... parametros) {
		super(mensagem, parametros);
	}

	public CiofiException(String message, Throwable cause) {
		super(message, cause);
	}
}
