package br.gov.ce.sefaz.ciofi.model.entity;

public class TransitoLivre {
	private Integer numeroAcaoFiscal;
	private Integer numeroNFeChaveAcesso;
	
	public Integer getNumeroAcaoFiscal() {
		return numeroAcaoFiscal;
	}
	public void setNumeroAcaoFiscal(Integer numeroAcaoFiscal) {
		this.numeroAcaoFiscal = numeroAcaoFiscal;
	}
	public Integer getNumeroNFeChaveAcesso() {
		return numeroNFeChaveAcesso;
	}
	public void setNumeroNFeChaveAcesso(Integer numeroNFeChaveAcesso) {
		this.numeroNFeChaveAcesso = numeroNFeChaveAcesso;
	}
	
	
}
