package br.gov.ce.sefaz.ciofi.model.entity;

public class Aplicacao {

	private String nome;
	private String clienteId;
	private String clienteSenha;

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getClienteId() {
		return clienteId;
	}

	public void setClienteId(String clienteId) {
		this.clienteId = clienteId;
	}

	public String getClienteSenha() {
		return clienteSenha;
	}

	public void setClienteSenha(String clienteSenha) {
		this.clienteSenha = clienteSenha;
	}
}
