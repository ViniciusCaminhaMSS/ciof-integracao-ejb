package br.gov.ce.sefaz.ciofi.enums;

import java.io.Serializable;

public enum ReportEnum implements Serializable {
	REPORT("/report/"),
	CONTENT_DISPOSITION("Content-Disposition"),
	JASPER(".jasper"),
	JSON("{\"registros\":");

	private String name;

	ReportEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return this.name;
	}

}