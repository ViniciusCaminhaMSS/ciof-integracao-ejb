package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class RegistroSitram implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String chaveAcesso;
	private String nfe;
	private String dataEmissao;
	private String valorTotal;
	private String situacao;
	
	public RegistroSitram() {
		super();
	}

	public String getChaveAcesso() {
		return chaveAcesso;
	}

	public void setChaveAcesso(String chaveAcesso) {
		this.chaveAcesso = chaveAcesso;
	}

	public String getNfe() {
		return nfe;
	}

	public void setNfe(String nfe) {
		this.nfe = nfe;
	}

	public String getDataEmissao() {
		return dataEmissao;
	}

	public void setDataEmissao(String dataEmissao) {
		this.dataEmissao = dataEmissao;
	}

	public String getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(String valorTotal) {
		this.valorTotal = valorTotal;
	}

	public String getSituacao() {
		return situacao;
	}

	public void setSituacao(String situacao) {
		this.situacao = situacao;
	}

}
