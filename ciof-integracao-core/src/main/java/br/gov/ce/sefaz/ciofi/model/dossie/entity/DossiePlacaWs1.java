package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.AcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.AutoInfracao;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.Veiculo;

public class DossiePlacaWs1 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<AutoInfracao> empresasVinculadasAutoInfracao;
	private List<NotaFiscal> nfsVinculadasMDFe;
	private List<NotaFiscal> nsfVinculadasMDFePassagemCE;
	private List<AcaoFiscal> pendenciasTransitoLivre;
	private String registroPassagemSitram;
	private List<NotaFiscal> registroPlacaNF;
	private Veiculo veiculo;
	
	public List<AutoInfracao> getEmpresasVinculadasAutoInfracao() {
		return empresasVinculadasAutoInfracao;
	}
	public void setEmpresasVinculadasAutoInfracao(List<AutoInfracao> empresasVinculadasAutoInfracao) {
		this.empresasVinculadasAutoInfracao = empresasVinculadasAutoInfracao;
	}
	public List<NotaFiscal> getNfsVinculadasMDFe() {
		return nfsVinculadasMDFe;
	}
	public void setNfsVinculadasMDFe(List<NotaFiscal> nfsVinculadasMDFe) {
		this.nfsVinculadasMDFe = nfsVinculadasMDFe;
	}
	public List<NotaFiscal> getNsfVinculadasMDFePassagemCE() {
		return nsfVinculadasMDFePassagemCE;
	}
	public void setNsfVinculadasMDFePassagemCE(List<NotaFiscal> nsfVinculadasMDFePassagemCE) {
		this.nsfVinculadasMDFePassagemCE = nsfVinculadasMDFePassagemCE;
	}
	public List<AcaoFiscal> getPendenciasTransitoLivre() {
		return pendenciasTransitoLivre;
	}
	public void setPendenciasTransitoLivre(List<AcaoFiscal> pendenciasTransitoLivre) {
		this.pendenciasTransitoLivre = pendenciasTransitoLivre;
	}
	public String getRegistroPassagemSitram() {
		return registroPassagemSitram;
	}
	public void setRegistroPassagemSitram(String registroPassagemSitram) {
		this.registroPassagemSitram = registroPassagemSitram;
	}
	public List<NotaFiscal> getRegistroPlacaNF() {
		return registroPlacaNF;
	}
	public void setRegistroPlacaNF(List<NotaFiscal> registroPlacaNF) {
		this.registroPlacaNF = registroPlacaNF;
	}
	public Veiculo getVeiculo() {
		return veiculo;
	}
	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
	
	
}
