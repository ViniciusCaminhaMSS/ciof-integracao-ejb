package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Result implements Serializable {

	private static final long serialVersionUID = -7790634497243977008L;

	// private String abbr;
	// private String area;
	private String capital;
	private String country;
	private String largestCity;
	private String name;
//	private Integer numeroLogradouro;
//	private long cnpj;
//	private Integer cgf;
//	private String razaoSocial;
//	private String bairro;
	
	/*
	 * public String getAbbr() { return abbr; } public void setAbbr(String abbr)
	 * { this.abbr = abbr; } public String getArea() { return area; } public
	 * void setArea(String area) { this.area = area; }
	 */
	public String getCapital() {
		return capital;
	}

	public void setCapital(String capital) {
		this.capital = capital;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLargestCity() {
		return largestCity;
	}

	public void setLargestCity(String largestCity) {
		this.largestCity = largestCity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
