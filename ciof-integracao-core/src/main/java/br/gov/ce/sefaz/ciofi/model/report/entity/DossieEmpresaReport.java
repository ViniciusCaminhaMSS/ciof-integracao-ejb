package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaReport implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cnpj;
	private String cgf;
	private String situacaoCnpj;
	private String cnae;
	private String razaoSocial;
	private String nomeFantasia;
	private String cep;
	private String uf;
	private String municipio;
	private String bairro;
	private String logradouro;
	private String numero;
	private String complemento;
	private List<String> procuradores;
	private List<TransitoLivre> transitoLivre;
	private List<RegistroSitram> registros;
	private List<AutoInfracao> autoInfracao;
	private List<TransportadorasEmissor> transportadorasEmissor;
	private List<EmpresaParticipacoes> empresaParticipacoes;
	private List<Socios> socios;
	private List<String> nomeSocios;
	private List<SocioParticipacao> socioParticipacaos;
	private List<TermosDeAcordo> termosDeAcordo;
	private List<ProcessosJudiciais> processosJudiciais;
	
	public DossieEmpresaReport() {
		super();
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCgf() {
		return cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public String getSituacaoCnpj() {
		return situacaoCnpj;
	}

	public void setSituacaoCnpj(String situacaoCnpj) {
		this.situacaoCnpj = situacaoCnpj;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public List<String> getProcuradores() {
		return procuradores;
	}

	public void setProcuradores(List<String> procuradores) {
		this.procuradores = procuradores;
	}

	public List<TransitoLivre> getTransitoLivre() {
		return transitoLivre;
	}

	public void setTransitoLivre(List<TransitoLivre> transitoLivre) {
		this.transitoLivre = transitoLivre;
	}

	public List<RegistroSitram> getRegistros() {
		return registros;
	}

	public void setRegistros(List<RegistroSitram> registros) {
		this.registros = registros;
	}

	public List<AutoInfracao> getAutoInfracao() {
		return autoInfracao;
	}

	public void setAutoInfracao(List<AutoInfracao> autoInfracao) {
		this.autoInfracao = autoInfracao;
	}

	public List<TransportadorasEmissor> getTransportadorasEmissor() {
		return transportadorasEmissor;
	}

	public void setTransportadorasEmissor(List<TransportadorasEmissor> transportadorasEmissor) {
		this.transportadorasEmissor = transportadorasEmissor;
	}

	public List<EmpresaParticipacoes> getEmpresaParticipacoes() {
		return empresaParticipacoes;
	}

	public void setEmpresaParticipacoes(List<EmpresaParticipacoes> empresaParticipacoes) {
		this.empresaParticipacoes = empresaParticipacoes;
	}

	public List<Socios> getSocios() {
		return socios;
	}

	public void setSocios(List<Socios> socios) {
		this.socios = socios;
	}

	public List<String> getNomeSocios() {
		return nomeSocios;
	}

	public void setNomeSocios(List<String> nomeSocios) {
		this.nomeSocios = nomeSocios;
	}

	public List<SocioParticipacao> getSocioParticipacaos() {
		return socioParticipacaos;
	}

	public void setSocioParticipacaos(List<SocioParticipacao> socioParticipacaos) {
		this.socioParticipacaos = socioParticipacaos;
	}

	public List<TermosDeAcordo> getTermosDeAcordo() {
		return termosDeAcordo;
	}

	public void setTermosDeAcordo(List<TermosDeAcordo> termosDeAcordo) {
		this.termosDeAcordo = termosDeAcordo;
	}

	public List<ProcessosJudiciais> getProcessosJudiciais() {
		return processosJudiciais;
	}

	public void setProcessosJudiciais(List<ProcessosJudiciais> processosJudiciais) {
		this.processosJudiciais = processosJudiciais;
	}
}
