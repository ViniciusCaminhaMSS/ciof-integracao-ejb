package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;
import java.util.List;

import br.gov.ce.sefaz.ciofi.model.entity.AutoInfracao;

public class DossieEmpresaWs5 implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private List<AutoInfracao> autoInfracaoLavrados;

	public List<AutoInfracao> getAutoInfracaoLavrados() {
		return autoInfracaoLavrados;
	}

	public void setAutoInfracaoLavrados(List<AutoInfracao> autoInfracaoLavrados) {
		this.autoInfracaoLavrados = autoInfracaoLavrados;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
