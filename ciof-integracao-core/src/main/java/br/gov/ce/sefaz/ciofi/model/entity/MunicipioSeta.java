package br.gov.ce.sefaz.ciofi.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import br.gov.ce.sefaz.jee.business.model.entity.SefazEntity;

@Entity
@Table(name = "MUNICIPIO", schema = "SETA")
public class MunicipioSeta extends SefazEntity {
	
	private static final long serialVersionUID = -162614051036676150L;

	@Id
	private Long id;
	
	@Column(name = "COD_MUNICIPIO")
	private Long codigo;
	
	@Column(name = "DSC_MUNICIPIO")
	private String descricao;
	
	@Column(name = "NUM_CEP")
	private Integer cep;
	
	
	@Override
	public Long getVersion() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public void setVersion(Long version) {
		// TODO Auto-generated method stub
		super.setVersion(null);
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getCodigo() {
		return codigo;
	}
	
	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
    public Integer getCep() {
		return cep;
	}

	public void setCep(Integer cep) {
		this.cep = cep;
	}

	@Override
    public int hashCode() {
    	return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
    	return ToStringBuilder.reflectionToString(this);
    }

}
