package br.gov.ce.sefaz.ciofi.model.report.entity;

import java.io.Serializable;

public class SocioParticipacao implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String cpfCnpj;
	
	private String cgf;
	
	private String razaoSocial;
	
	private String cnae;
	
	public SocioParticipacao() {
		super();
	}

	public String getCpfCnpj() {
		return cpfCnpj;
	}

	public void setCpfCnpj(String cpfCnpj) {
		this.cpfCnpj = cpfCnpj;
	}

	public String getCgf() {
		return cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnae() {
		return cnae;
	}

	public void setCnae(String cnae) {
		this.cnae = cnae;
	}

}
