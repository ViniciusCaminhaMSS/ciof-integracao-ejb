package br.gov.ce.sefaz.ciofi.model.dossie.entity;

import java.io.Serializable;

import br.gov.ce.sefaz.ciofi.model.entity.CnaePrincipal;
import br.gov.ce.sefaz.ciofi.model.entity.Municipio;

public class DossieCPF implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String razaoSocial;
	private String logradouro;
	private String numeroLogradouro;
	private String complemento;
	private String bairro;
	private Municipio municipio;
	private String cnpj;
	private String cgf;
	private CnaePrincipal cnaePrimario;
	private String codSituacao;
	private String cep;
	private String uf;
	private String descricaoSituacao;
	
	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumeroLogradouro() {
		return numeroLogradouro;
	}

	public void setNumeroLogradouro(String numeroLogradouro) {
		this.numeroLogradouro = numeroLogradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Municipio getMunicipio() {
		return municipio;
	}

	public void setMunicipio(Municipio municipio) {
		this.municipio = municipio;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getCgf() {
		return cgf;
	}

	public void setCgf(String cgf) {
		this.cgf = cgf;
	}

	public CnaePrincipal getCnaePrimario() {
		return cnaePrimario;
	}

	public void setCnaePrimario(CnaePrincipal cnaePrimario) {
		this.cnaePrimario = cnaePrimario;
	}

	public String getCodSituacao() {
		return codSituacao;
	}

	public void setCodSituacao(String codSituacao) {
		this.codSituacao = codSituacao;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getDescricaoSituacao() {
		return descricaoSituacao;
	}

	public void setDescricaoSituacao(String descricaoSituacao) {
		this.descricaoSituacao = descricaoSituacao;
	}
	
	
	
}
