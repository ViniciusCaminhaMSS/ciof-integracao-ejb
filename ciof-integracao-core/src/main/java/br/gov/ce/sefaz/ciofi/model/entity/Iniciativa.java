package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class Iniciativa implements Serializable {

	private static final long serialVersionUID = 2481817960169852578L;

	private String nomeIniciativa;

	public String getNomeIniciativa() {
		return nomeIniciativa;
	}

	public void setNomeIniciativa(String nomeIniciativa) {
		this.nomeIniciativa = nomeIniciativa;
	}

}
