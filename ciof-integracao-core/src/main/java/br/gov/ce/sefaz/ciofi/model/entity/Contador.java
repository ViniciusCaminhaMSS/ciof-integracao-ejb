package br.gov.ce.sefaz.ciofi.model.entity;

public class Contador {
	private String codigoCpfCnpj;
	private String nome;

	public String getCodigoCpfCnpj() {
		return codigoCpfCnpj;
	}

	public void setCodigoCpfCnpj(String codigoCpfCnpj) {
		this.codigoCpfCnpj = codigoCpfCnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
