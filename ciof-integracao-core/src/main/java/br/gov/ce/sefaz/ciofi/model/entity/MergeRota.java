package br.gov.ce.sefaz.ciofi.model.entity;

import java.io.Serializable;

public class MergeRota implements Serializable {

	private static final long serialVersionUID = -7565330313595181161L;

	private String cep;
	private String name;
	private String country;

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}
}
