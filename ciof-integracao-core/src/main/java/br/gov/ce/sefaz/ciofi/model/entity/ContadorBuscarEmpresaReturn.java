package br.gov.ce.sefaz.ciofi.model.entity;

import java.util.List;

public class ContadorBuscarEmpresaReturn {
	private String cpf;
	private String nome;
	private List<String> telefones;

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<String> telefones) {
		this.telefones = telefones;
	}
	

}
