package br.gov.ce.sefaz.ciofi.web.router;

import org.apache.camel.CamelContext;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import br.gov.ce.sefaz.ciofi.web.router.gerarDossieCPF.BuscarContribuinteCPF;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossieCPF.BuscarIdentificadorNFe;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossieEmpresa.BuscarNfeEntradaInterestadual;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossieEmpresa.RetornaNotasFiscaisSemRegistro;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca.BuscarDadosEmpresaPlaca;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca.BuscarNFVinculadaMDFe;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca.BuscarNFe24PassagemSitram;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca.BuscarNFePlacaUltimasXHoras;
import br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca.BuscarPlaca;
import br.gov.ce.sefaz.ciofi.web.router.realizarConsulta.RealizarConsultaCnpjCgf;
import br.gov.ce.sefaz.ciofi.web.router.realizarConsulta.RealizarConsultaNFe;

@RunWith(MockitoJUnitRunner.class)
public class DiligenciaCentralRotasTest {

	private CamelContext context;
	private RealizarConsultaCnpjCgf realizarConsulta;
	private RealizarConsultaNFe realizarConsultaNFe;
	private BuscarContribuinteCPF buscarContribuinteCPF;
	private BuscarIdentificadorNFe buscarIdentificadorNFe;
	private BuscarNfeEntradaInterestadual buscarNfeEntradaInterestadual;
	private RetornaNotasFiscaisSemRegistro retornaNotasFiscaisSemRegistro;
	private BuscarDadosEmpresaPlaca buscarDadosEmpresaPlaca;
	private BuscarNFe24PassagemSitram buscarNfe24PassagemSitram;
	private BuscarNFePlacaUltimasXHoras buscarNFePlacaUltimasXHoras;
	private BuscarNFVinculadaMDFe buscarNFVinculadaMDFe;
	private BuscarPlaca buscarPlaca;
	
	
	@Before
	public void init() {
		// Realizar Consulta
		realizarConsulta = new RealizarConsultaCnpjCgf();
		realizarConsultaNFe = new RealizarConsultaNFe();
		// Dossie CPF
		buscarContribuinteCPF = new BuscarContribuinteCPF();
		buscarIdentificadorNFe = new BuscarIdentificadorNFe();
		// Dossie Empresa
		buscarNfeEntradaInterestadual = new BuscarNfeEntradaInterestadual();
		retornaNotasFiscaisSemRegistro = new RetornaNotasFiscaisSemRegistro();
		// Dossie Placa
		buscarDadosEmpresaPlaca = new BuscarDadosEmpresaPlaca();
		buscarNfe24PassagemSitram = new BuscarNFe24PassagemSitram();
		buscarNFePlacaUltimasXHoras = new BuscarNFePlacaUltimasXHoras();
		buscarNFVinculadaMDFe = new BuscarNFVinculadaMDFe();
		buscarPlaca = new BuscarPlaca();
	}

	@Test
	public void realizarConsulta() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(realizarConsulta);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void realizarConsultaNFe() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(realizarConsultaNFe);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarContribuinteCPF() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarContribuinteCPF);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarIdentificadorNFe() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarIdentificadorNFe);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarNfeEntradaInterestadual() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarNfeEntradaInterestadual);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void retornaNotasFiscaisSemRegistro() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(retornaNotasFiscaisSemRegistro);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarDadosEmpresaPlaca() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarDadosEmpresaPlaca);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarNfe24PassagemSitram() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarNfe24PassagemSitram);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarNFePlacaUltimasXHoras() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarNFePlacaUltimasXHoras);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarNFVinculadaMDFe() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarNFVinculadaMDFe);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test
	public void buscarPlaca() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(buscarPlaca);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
