package br.gov.ce.sefaz.ciofi.web.rest.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
//import org.apache.camel.CamelContext; 
//import org.apache.camel.component.mock.MockEndpoint; 
//import org.apache.camel.impl.DefaultCamelContext; 
import static org.mockito.Mockito.when;
import br.gov.ce.sefaz.ciofi.web.action.RotaAction;

@RunWith(MockitoJUnitRunner.class)
public class DiligenciaCentralServiceTest {

	@Mock
	private HttpServletRequest request;

	@Mock
	private RotaAction routerAction;

	@Mock
	private DiligenciaCentralService diligenciaService;

	@Before
	public void init() {
		diligenciaService = new DiligenciaCentralService(routerAction);
	}

	//Realizar Consulta - Buscar Empresa
	@Test
	public void buscarEmpresa() {
		String jsonEsperado = getBuscarEmpresaJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarEmpresa("cnpj=7319866000109").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarEmpresaRetornaNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.buscarEmpresa("cnpj=7319866000109").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarEmpresaStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarEmpresa("cnpj=7319866000109").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarEmpresaJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean msgErrorResposta = diligenciaService.buscarEmpresa("cnpj=7319866000109").getEntity() == null;
		assertFalse(msgErroEsperada, msgErrorResposta);
	}
	
	// Realizar Consulta - Buscar NFE-Chave de Acesso
	
	@Test
	public void buscarNfeOuChaveAcesso() {
		String jsonEsperado = getBuscarNfeChaveAcessoJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarContribuinteNfeOuChaveAcesso("221?dataAtual=01-01-2014").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarNfeOuChaveAcessoRetornaNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarContribuinteNfeOuChaveAcesso("221?dataAtual=01-01-2014").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNfeOuChaveAcessoStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarContribuinteNfeOuChaveAcesso("221?dataAtual=01-01-2014").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNfeOuChaveAcessoJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarEmpresa("221?dataAtual=01-01-2014").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	
	//Dossie CPF - Buscar Contribuinte
	@Test
	public void buscarContribuinteCPF() {
		String jsonEsperado = getBuscarContribuinteCPFJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarContribuinte("cpf=42659710325").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarContribuinteCPFNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarContribuinte("cpf=42659710325").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarContribuinteCPFStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarContribuinte("cpf=42659710325").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarContribuinteCPFJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarContribuinte("cpf=42659710325").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie CPF - Buscar Identificador NFe
	@Test
	public void buscarIdentificadorNFe() {
		String jsonEsperado = getBuscarIdentificadorNFeJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarIdentificadorNFe("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarIdentificadorNFeNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.buscarIdentificadorNFe("123").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarIdentificadorNFeStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarIdentificadorNFe("123").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarIdentificadorNFeJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean msgErrorResposta = diligenciaService.buscarIdentificadorNFe("123").getEntity() == null;
		assertFalse(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Placa - Buscar NFe 24 Passagem Sitram
	@Test
	public void buscarNFe24PassagemSitram() {
		String jsonEsperado = getBuscarNFe24PassagemSitramJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarNFe24PassagemSitram("placa=ASD2342").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarNFe24PassagemSitramNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarNFe24PassagemSitram("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFe24PassagemSitramStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarNFe24PassagemSitram("placa=ASD2342").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFe24PassagemSitramJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarNFe24PassagemSitram("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Placa - buscar Placa
	@Test
	public void buscarPlaca() {
		String jsonEsperado = getBuscarPlacaJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarPlaca("placa=ASD2342").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarPlacaNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarPlaca("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarPlacaStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarPlaca("placa=ASD2342").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarPlacaJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarPlaca("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Placa - Buscar NF Vinculada MDFe
	@Test
	public void buscarNFVinculadaMDFe() {
		String jsonEsperado = getBuscarNFVinculadaMDFeJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarNFVinculadaMDFe("placa=ASD2342").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarNFVinculadaMDFeNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarNFVinculadaMDFe("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFVinculadaMDFeStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarNFVinculadaMDFe("placa=ASD2342").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFVinculadaMDFeJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarNFVinculadaMDFe("HKE0575").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Placa - Buscar NFe Placa Ultimas X Horas
	@Test
	public void buscarNFePlacaUltimasXHoras() {
		String jsonEsperado = getbuscarNFePlacaUltimasXHorasJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarNFePlacaUltimasXHoras("HKE0575").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarNFePlacaUltimasXHorasNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarNFePlacaUltimasXHoras("HKE0575").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFePlacaUltimasXHorasStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarNFePlacaUltimasXHoras("HKE0575").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarNFePlacaUltimasXHorasJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarNFePlacaUltimasXHoras("placa=ASD2342").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}

	//Dossie Placa - Buscar Dados Empresa
	@Test
	public void buscarDadosEmpresaPlaca() {
		String jsonEsperado = getBuscarDadosEmpresaPlaca();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarDadosEmpresaPlaca("ie=61787477","cnpj=7278045000171").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarDadosEmpresaPlacaNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.buscarDadosEmpresaPlaca("ie=61787477","cnpj=7278045000171").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarDadosEmpresaPlacaStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarDadosEmpresaPlaca("ie=61787477","cnpj=7278045000171").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarDadosEmpresaPlacaJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.buscarDadosEmpresaPlaca("ie=61787477","cnpj=7278045000171").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Placa - Pendencia Transito Livre
	@Test
	public void pendenciaTransitoLivre() {
		String jsonEsperado = getPendenciaTransitoLivreJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.pendenciaTransitoLivre("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void pendenciaTransitoLivreNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.pendenciaTransitoLivre("123").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void pendenciaTransitoLivreStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.pendenciaTransitoLivre("123").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void pendenciaTransitoLivreJsonError() {
		String jsonEsperado = getPendenciaTransitoLivreJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.pendenciaTransitoLivre("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	// Dossie Placa - Tres Principais Contribuintes Auto Infracao
	@Test
	public void tresPrincipaisContribuintesAutoInfracao() {
		String jsonEsperado = getTresPrincipaisContribuintesAutoInfracaoJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.tresPrincipaisContribuintesAutoInfracao("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void tresPrincipaisContribuintesAutoInfracaoNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.tresPrincipaisContribuintesAutoInfracao("123").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void tresPrincipaisContribuintesAutoInfracaoStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.tresPrincipaisContribuintesAutoInfracao("123").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void tresPrincipaisContribuintesAutoInfracaoJsonError() {
		String jsonEsperado = getTresPrincipaisContribuintesAutoInfracaoJson();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.tresPrincipaisContribuintesAutoInfracao("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	//Dossie Empresa - Nfe Entrada Interestadual Sem Registro Sitram
	@Test
	public void nfeEntradaInterestadualSemRegistroSitram() {
		String jsonEsperado = getNfeEntradaInterestadualSemRegistroSitramJson();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.nfeEntradaInterestadualSemRegistroSitram("ie=69980438","cnpj=2045487000901").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void nfeEntradaInterestadualSemRegistroSitramNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) diligenciaService.nfeEntradaInterestadualSemRegistroSitram("ie=69980438","cnpj=2045487000901").getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void nfeEntradaInterestadualSemRegistroSitramStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.nfeEntradaInterestadualSemRegistroSitram("ie=69980438","cnpj=2045487000901").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void nfeEntradaInterestadualSemRegistroSitramJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) diligenciaService.nfeEntradaInterestadualSemRegistroSitram("ie=69980438","cnpj=2045487000901").getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Empresa - Historico Auto Infracao Por Cnpj
	@Test
	public void buscarAutoInfracao() {
		String jsonEsperado = getBuscarAutoInfracao();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarAutoInfracao("ie=69980438").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarAutoInfracaoNulo() {
		String jsonEsperado = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("ie=69980438").getEntity() == null;
		assertFalse(jsonEsperado, jsonResposta);
	}

	@Test
	public void buscarAutoInfracaoStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("ie=69980438").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarAutoInfracaoJsonError() {
		String jsonEsperado = getBuscarAutoInfracao();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarAutoInfracao("ie=69980438").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	//Dossie Empresa -  Transito Livre
	@Test
	public void buscarTransitoLivre() {
		String jsonEsperado = getBuscarTransitoLivre();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarTransitoLivre("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarTransitoLivreNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("123").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarTransitoLivreStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("123").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarTransitoLivreJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean msgErrorResposta = diligenciaService.buscarAutoInfracao("123").getEntity() == null;
		assertFalse(msgErroEsperada, msgErrorResposta);
	}
	
	//Dossie Empresa - Transportadoras Utilizadas
	@Test
	public void buscarTransportadorasUtilizadas() {
		String jsonEsperado = getBuscarTransportadorasUtilizadas();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarTransportadorasUtilizadas("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	@Test
	public void buscarTransportadorasUtilizadasNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("123").getEntity() == null;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarTransportadorasUtilizadasStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		Boolean jsonResposta = diligenciaService.buscarAutoInfracao("123").getLength() == 0;
		assertFalse(msgErroEsperada, jsonResposta);
	}

	@Test
	public void buscarTransportadorasUtilizadasJsonError() {
		String jsonEsperado = getBuscarTransportadorasUtilizadas();
		when(routerAction.getOutput()).thenReturn(jsonEsperado);
		String jsonResposta = (String) diligenciaService.buscarTransportadorasUtilizadas("123").getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}
	
	
	private String getBuscarEmpresaJson() {
		return "{ \"atividadeCNAE\": \"Beneficiamento de arroz\", \"cgf\": 60091088, \"cnpj\": 7319866000109, \"contador\": { \"cpf\": 4617053387, \"nome\": \"ANTONIO MARDONIO DE OLIVEIRA\" }, \"endereco\": { \"numero\": \"00535\", \"bairro\": \"ALTO DO JUCA\" }, \"razaoSocial\": \"F A LAVOR & CIA LTDA ME\", \"situacaoCadastral\": \"3 - BAIXADO A PEDIDO\" }";
	}
	
	private String getBuscarNfeChaveAcessoJson() {
		return "[{\"cnpjEmitente\":5109190000101,\"cnpjDestinatario\":63481683000166,\"chaveAcesso\":\"43131205109190000101550010000002211000001296\",\"ufEmitente\":\"RS\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"VS DIAS EQUIP. ESPORT. LTDA\"},{\"cnpjEmitente\":13759712000130,\"cnpjDestinatario\":11780302000146,\"chaveAcesso\":\"15131213759712000130550010000002211000002210\",\"ufEmitente\":\"PA\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"MADEIRA FORTE LTDA ME\"},{\"cnpjEmitente\":8736355000317,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23130708736355000317550010000002211000002216\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"F DE QUEIROZ NETA JALES\"},{\"cnpjEmitente\":14450776000118,\"cnpjDestinatario\":11440791000197,\"chaveAcesso\":\"26131214450776000118550010000002211000017689\",\"ufEmitente\":\"PE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"ALFA INTERNATIONAL RECIFE COM. E REP. LTDA\"},{\"cnpjEmitente\":12547267000182,\"cnpjDestinatario\":17558158000184,\"chaveAcesso\":\"29131212547267000182550010000002211004640327\",\"ufEmitente\":\"BA\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Comercial de Frutas Dias Lima ltda\"},{\"cnpjEmitente\":9598755000104,\"cnpjDestinatario\":15524451000103,\"chaveAcesso\":\"25131209598755000104550010000002211005000212\",\"ufEmitente\":\"PB\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"ALLUSE IND. E COM. DE S. E TELHAS PLASTICAS LTDA.\"},{\"cnpjEmitente\":41591397000110,\"cnpjDestinatario\":7349939000105,\"chaveAcesso\":\"23131041591397000110550010000002211005100060\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"ACQUACENTER COMÉRCIO E SERVIÇOS HIDRÁULICOS LTDA\"},{\"cnpjEmitente\":17756890000169,\"cnpjDestinatario\":0,\"chaveAcesso\":\"35131117756890000169550010000002211005700300\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"M. M. DISTRIBUICAO E SERVICOS LTDA-ME\"},{\"cnpjEmitente\":18966666000164,\"cnpjDestinatario\":0,\"chaveAcesso\":\"15131218966666000164550010000002211004640327\",\"ufEmitente\":\"PA\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"JOSE L. S. COSTA\"},{\"cnpjEmitente\":8541665000114,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23130608541665000114550010000002211043000221\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"EMILIO ALVES CORREIA ME\"},{\"cnpjEmitente\":17539168000172,\"cnpjDestinatario\":18836455000107,\"chaveAcesso\":\"35131217539168000172550010000002211070111571\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"RUBY COM. IMP. E EXP. EIRELI - EPP\"},{\"cnpjEmitente\":17326872000147,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23131217326872000147550010000002211082500600\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"H.P. DE VASCONCELOS ME\"},{\"cnpjEmitente\":899065000110,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23130100899065000110550010000002211109580345\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"CERÂMICA TAVARES LTDA\"},{\"cnpjEmitente\":83313189000108,\"cnpjDestinatario\":16921619000179,\"chaveAcesso\":\"15131283313189000108550010000002211000017685\",\"ufEmitente\":\"PA\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"R F BARBOSA COMERCIO\"},{\"cnpjEmitente\":6626253055134,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"31131206626253055134550110000002211221000007\",\"ufEmitente\":\"MG\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":18162632000117,\"cnpjDestinatario\":1098983000103,\"chaveAcesso\":\"26131218162632000117550020000002211019956455\",\"ufEmitente\":\"PE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"MDM XAVIER CALCADOS EIRELI - ME\"},{\"cnpjEmitente\":12187191000121,\"cnpjDestinatario\":35065028000191,\"chaveAcesso\":\"22131212187191000121550010000002211000002217\",\"ufEmitente\":\"PI\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"MACOL MATERIAL DE CONSTRUCAO LTDA-ME\"},{\"cnpjEmitente\":18131906000100,\"cnpjDestinatario\":7604556000640,\"chaveAcesso\":\"35131218131906000100550010000002211000053003\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"OliverSil Com. Atac. de Materiais de Manut. Eireli - Epp\"},{\"cnpjEmitente\":15488459000153,\"cnpjDestinatario\":18541368000122,\"chaveAcesso\":\"31131215488459000153550010000002211000262728\",\"ufEmitente\":\"MG\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"RONALDO ALVES DA SILVA\"},{\"cnpjEmitente\":9358108001016,\"cnpjDestinatario\":0,\"chaveAcesso\":\"33131209358108001016550460000002211057828727\",\"ufEmitente\":\"RJ\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"NOVA PONTOCOM COMERCIO ELETRONICO S.A.\"},{\"cnpjEmitente\":19900000001903,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23131219900000001903550030000002211063483900\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"EX\",\"nomeEmitente\":\"Cervejarias Kaiser Brasil S.A.\"},{\"cnpjEmitente\":11661116000198,\"cnpjDestinatario\":0,\"chaveAcesso\":\"25131211661116000198550010000002211100590073\",\"ufEmitente\":\"PB\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"ZAFIRO IND. E COM. EIRELE-ME\"},{\"cnpjEmitente\":6626253032789,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253032789550110000002211221000000\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Emprrendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":9080998000156,\"cnpjDestinatario\":5254600000108,\"chaveAcesso\":\"31131209080998000156550010000002211090180732\",\"ufEmitente\":\"MG\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"J.B. COMERCIO DE BIJOUTERIAS LTDA\"},{\"cnpjEmitente\":62356878003560,\"cnpjDestinatario\":7332190001084,\"chaveAcesso\":\"22131262356878003560550010000002211318102216\",\"ufEmitente\":\"PI\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"EISA Empresa Interagricola SA\"},{\"cnpjEmitente\":11137051018204,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23130111137051018204559000000002213456519204\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"INTERBELLE Comercio de Produtos de Beleza LTDA\"},{\"cnpjEmitente\":11200418000401,\"cnpjDestinatario\":0,\"chaveAcesso\":\"35131211200418000401550490000002211465651749\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"COMERCIO DIGITAL BF LTDA. CCP\"},{\"cnpjEmitente\":23448467000102,\"cnpjDestinatario\":6552566000102,\"chaveAcesso\":\"23130623448467000102550010000002211526000298\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"J. Adauto Alves-ME\"},{\"cnpjEmitente\":5325014000522,\"cnpjDestinatario\":5325014000107,\"chaveAcesso\":\"15131205325014000522550010000002211700004202\",\"ufEmitente\":\"PA\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"MAKRO ENGENHARIA LTDA\"},{\"cnpjEmitente\":17304535000159,\"cnpjDestinatario\":5355605000119,\"chaveAcesso\":\"35131217304535000159550010000002211802800000\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"FSC Fraldas Secas e Confortaveis\"},{\"cnpjEmitente\":15840693000106,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23130215840693000106550010000002211856692092\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"NATIVA MOTOS LTDA\"},{\"cnpjEmitente\":10797331000158,\"cnpjDestinatario\":9487141000155,\"chaveAcesso\":\"24131210797331000158550020000002211996505700\",\"ufEmitente\":\"RN\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"MS COMERCIO DE DERIVADOS DE PETROLEO LTDA\"},{\"cnpjEmitente\":35039007000100,\"cnpjDestinatario\":69369437000101,\"chaveAcesso\":\"23131035039007000100550010000002211997494694\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"ROCHA E IRMAO LTDA\"},{\"cnpjEmitente\":6626253027513,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253027513550110000002211221000003\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":2918654001449,\"cnpjDestinatario\":2918654001287,\"chaveAcesso\":\"35131202918654001449550500000002211056455633\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"CENTRO OESTE RACOES S/A\"},{\"cnpjEmitente\":10584744000154,\"cnpjDestinatario\":2748357000188,\"chaveAcesso\":\"24131210584744000154550010000002211080008007\",\"ufEmitente\":\"RN\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"NORTE SONO POTIGUAR COMÉRCIO DE COLCHÕES E ACESSÓRIOS LTDA\"},{\"cnpjEmitente\":15244677000142,\"cnpjDestinatario\":0,\"chaveAcesso\":\"23131215244677000142550010000002211116593025\",\"ufEmitente\":\"CE\",\"ufDestinatario\":\"EX\",\"nomeEmitente\":\"COMPANHIA SULAMERICANA DE CERAMICA\"},{\"cnpjEmitente\":14275263000118,\"cnpjDestinatario\":63310411000101,\"chaveAcesso\":\"35131214275263000118550010000002211211268753\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"NOVO MOLDE CONFECCOES LTDA EPP\"},{\"cnpjEmitente\":6626253014020,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"27131206626253014020550110000002211221000003\",\"ufEmitente\":\"AL\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253039104,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253039104550110000002211221000006\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A\"},{\"cnpjEmitente\":7241948000188,\"cnpjDestinatario\":1098983000103,\"chaveAcesso\":\"41131207241948000188550010000002211752721394\",\"ufEmitente\":\"PR\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"OLHO DA AGUIA COMERCIO DE CALCADOS LTDA\"},{\"cnpjEmitente\":6626253025065,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253025065550110000002211221000009\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253050337,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253050337550110000002211221000004\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253025731,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253025731550110000002211221000008\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253022392,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253022392550110000002211221000000\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253056297,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253056297550110000002211221000004\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":6626253033165,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253033165550110000002211221000009\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"},{\"cnpjEmitente\":70184064000175,\"cnpjDestinatario\":24996902000279,\"chaveAcesso\":\"26131270184064000175550010000002211495610101\",\"ufEmitente\":\"PE\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"SAM MELO CONSTRUCOES Ltda.\"},{\"cnpjEmitente\":7854774000129,\"cnpjDestinatario\":0,\"chaveAcesso\":\"35131207854774000129550010000002211930540400\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"R. ALMEIDA BRAZ AUTOMOTIVOS - ME\"},{\"cnpjEmitente\":10507378000482,\"cnpjDestinatario\":30260871001500,\"chaveAcesso\":\"13131210507378000482550010000002211504905044\",\"ufEmitente\":\"AM\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"E L REIS COMERCIO DE OTICAS LTDA\"},{\"cnpjEmitente\":3472246000669,\"cnpjDestinatario\":17881272000140,\"chaveAcesso\":\"32131203472246000669550080000002211923002719\",\"ufEmitente\":\"ES\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"AUDI DO BRASIL Ind.Com.Veic.Lt\"},{\"cnpjEmitente\":5917540000158,\"cnpjDestinatario\":394494010794,\"chaveAcesso\":\"53131205917540000158550010000002211944532221\",\"ufEmitente\":\"DF\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"DECOLANDO TURISMO E REPRESENTACOES LTDA - ME\"},{\"cnpjEmitente\":6626253034803,\"cnpjDestinatario\":6626253012400,\"chaveAcesso\":\"35131206626253034803550110000002211221000003\",\"ufEmitente\":\"SP\",\"ufDestinatario\":\"CE\",\"nomeEmitente\":\"Empreendimentos Pague Menos S.A.\"}]";
	}
	
	private String getBuscarContribuinteCPFJson() {
		return "{\"contribuinte\":{\"cpf\":7567548000167,\"endereco\":{\"cep\":60000,\"complemento\":\"A\",\"logradouro\":\"ARMANDO MONTEIRO\",\"municipio\":\"FORTALEZA\",\"numero\":\"00622\",\"bairro\":\"AEROPORTO\"},\"nome\":\"JOAO FURTADO PINHEIRO LANDIM MICROEMPRESA\"},\"participacaoEmpresas\":[{\"atividadeCNAE\":\"Comércio varejista de mercadorias em geral, com predominânci\",\"cgf\":69083177,\"cnpj\":41579293000190,\"razaoSocial\":\"BODYSHAPE COMERCIO IMPORTACAO E EXPORTACAO LTDA ME\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":66871760,\"cnpj\":4230183000354,\"razaoSocial\":\"LCM COMERCIO E SERVIÇOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63061880,\"cnpj\":4143698000589,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63059711,\"cnpj\":4143698000236,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio atacadista de outros equipamentos e artigos de uso \",\"cgf\":63059762,\"cnpj\":4143698000317,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":66942020,\"cnpj\":4230183000435,\"razaoSocial\":\"LCM COMERCIO E SERVIÇOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63081350,\"cnpj\":4230183000192,\"razaoSocial\":\"LCM COMERCIO E SERVIÇOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63105250,\"cnpj\":4143698000660,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63105624,\"cnpj\":4143698000821,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":63164914,\"cnpj\":4143698000740,\"razaoSocial\":\"PHOTO IMAGE COMERCIO E SERVICOS LTDA\",\"situacaoCadastral\":\"9\"},{\"atividadeCNAE\":\"Comércio varejista de artigos fotográficos e para filmagem \",\"cgf\":66871441,\"cnpj\":4230183000273,\"razaoSocial\":\"LCM COMERCIO E SERVIÇOS LTDA\",\"situacaoCadastral\":\"9\"}],\"participouEmpresas\":[]}";
	}
	
	private String getBuscarIdentificadorNFeJson() {
		return "{ \"destinatarioEmpresas\": [{ \"atividadeCNAE\": \"Fabricação de motores elétricos, peças e acessórios\", \"cgf\": \"58.171.836-1\", \"cnpj\": \"97.789.741/0001-35\", \"contador\": { \"cpf\": \"568.237.951-91\", \"nome\": \"Mauricio Bastter Hissa\", \"telefones\": [\"3012-4912\"] }, \"endereco\": { \"bairro\": \"Centro\", \"cep\": \"69271-922'\", \"complemento\": \"Apartamento 2912\", \"logradouro\": \"Avenida Santos Dumont\", \"municipio\": \"Fortaleza\", \"numero\": \"1321\", \"uf\": \"CE\" }, \"razaoSocial\": \"WEG S.A.\", \"situacaoCadastral\": \"ATIVO\", \"socios\": [{ \"nome\": \"Fabio Antonio\" }] }], \"remetenteEmpresas\": [{ \"atividadeCNAE\": \"Manutenção e reparação de máquinas, aparelhos e materiais elétricos não especificados anteriormente\", \"cgf\": \"69.401.560-2\", \"cnpj\": \"88.130.531/0001-40\", \"contador\": { \"cpf\": \"170.431.680-40\", \"nome\": \"Kauê Marcos da Mata\", \"telefones\": [\"(69) 2881-0998\"] }, \"endereco\": { \"bairro\": \"Castanheira\", \"cep\": \"76811-344\", \"complemento\": \"string\", \"logradouro\": \"Rua Abílio Nascimento\", \"municipio\": \"Porto Velho\", \"numero\": \"111\", \"uf\": \"RO\" }, \"razaoSocial\": \"Sérgio e Cauê Ferragens Ltda\", \"situacaoCadastral\": \"ATIVO\", \"socios\": [{ \"nome\": \"Lucas Lucca Benjamin Viana\" }] }], \"valorTotalDestinatario\": 3051.44, \"valorTotalRemetente\": 2958.33}";
	}
	
	private String getBuscarNFe24PassagemSitramJson() {
		return "{\"empresasVinculadasAutoInfracao\":[{\"diligencia\":{\"numero\":123123},\"empresa\":{\"atividadeCNAE\":\"Atividade Cnae 01\",\"cgf\":12312312,\"cnpj\":123123123333,\"contador\":{\"cpf\":123123123,\"nome\":\"Teste Contador 01\",\"telefones\":[\"88547687\"]},\"endereco\":{\"cep\":12312312,\"complemento\":\"Complemento 01\",\"logradouro\":\"Logradouro 01\",\"municipio\":\"Municipio 01\",\"numero\":\"12312\",\"uf\":\"CE\",\"bairro\":\"Bairro 01\"},\"razaoSocial\":\"Razao Social 01\",\"situacaoCadastral\":\"Situacao 01\",\"socios\":[{\"nome\":\"Socio 01\"}]},\"nomeOrgaoAtuante\":\"Orgao Atuante 01\",\"numero\":\"12312\"}],\"nfsVinculadasMDFe\":[{\"chave\":\"12312312312312312312412513616134123\",\"numero\":\"126553122\",\"valor\":\"145,30\",\"dataEmissao\":\"07/11/2014\",\"situacaoDebito\":\"Pendente\"}],\"nfsVinculadasMDFePassagemCE\":[{}],\"pendenciasTransitoLivre\":[{\"numeroAcaoFiscal\":12312,\"numeroNFeChaveAcesso\":213123123}],\"registroPassagemSitram\":\"2014-08-26 08:32:37\",\"registroPlacaNF\":[{\"destinatario\":\"Dest 01\",\"emitente\":\"Emi 01 \",\"numeroChaveAcesso\":\"12312312312312\",\"ufDestinatario\":\"UF 01\",\"ufEmitente\":\"UF 02\"}],\"veiculo\":{\"debitoIPVA\":true,\"placa\":\"ZZZ9999\",\"proprietario\":\"Proprietario 01\",\"roubado\":false,\"totalDebitos\":123.9}}";
	}
	
	private String getBuscarPlacaJson() {
		return "{\"registroPassagemSitram\":\"2014-08-26 08:32:37\",\"veiculo\":{\"debitoIPVA\":true,\"placa\":\"ASD3421\",\"proprietario\":\"José Da Silva Candido\",\"roubado\":false,\"totalDebitos\":3400.0,\"uf\":\"CE\"}}";
	}
	
	private String getBuscarNFVinculadaMDFeJson() {
		return "{\"quantidadeNfs\":\"1\",\"nfeVinculadasMDFe\":[{\"chave\":\"35140859275792008991550040030138921030138929\",\"numero\":\"3013892\",\"valor\":\"1682.28\",\"dataEmissao\":\"20082014\"}]}";
	}
	
	private String getbuscarNFePlacaUltimasXHorasJson() {
		return "{\"quantidadeNfs\":\"399\",\"valorNfs\":\"414.399,30\",\"registroPlacaNF\":[{\"chave\":\"35180407170938014078551150002416631321634393\",\"numero\":\"241663\",\"valor\":\"282.07\",\"dataEmissao\":\"23042018\",\"serie\":\"115\"},{\"chave\":\"35180407170938014078551830008355781892687077\",\"numero\":\"835578\",\"valor\":\"4348.0\",\"dataEmissao\":\"20042018\",\"serie\":\"183\"},{\"chave\":\"35180407170938014078551510001351711707750357\",\"numero\":\"135171\",\"valor\":\"139.44\",\"dataEmissao\":\"23042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551200003158571677843756\",\"numero\":\"315857\",\"valor\":\"275.88\",\"dataEmissao\":\"24042018\",\"serie\":\"120\"},{\"chave\":\"42180414055516000571550550002962721005615741\",\"numero\":\"296272\",\"valor\":\"167.27\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551530003392621949672201\",\"numero\":\"339262\",\"valor\":\"1038.9\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180413477066000895550220001713561003405146\",\"numero\":\"171356\",\"valor\":\"1196.4\",\"dataEmissao\":\"24042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938001766551410000634661826353348\",\"numero\":\"63466\",\"valor\":\"1240.58\",\"dataEmissao\":\"24042018\",\"serie\":\"141\"},{\"chave\":\"41180413986197000121550020003471021695707159\",\"numero\":\"347102\",\"valor\":\"2319.43\",\"dataEmissao\":\"24042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551250003324801965054341\",\"numero\":\"332480\",\"valor\":\"529.78\",\"dataEmissao\":\"23042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078551210002024761176441183\",\"numero\":\"202476\",\"valor\":\"326.1\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"35180407170938014078551830008370531798177675\",\"numero\":\"837053\",\"valor\":\"2392.02\",\"dataEmissao\":\"25042018\",\"serie\":\"183\"},{\"chave\":\"35180407170938001766551060002002291443465181\",\"numero\":\"200229\",\"valor\":\"305.12\",\"dataEmissao\":\"24042018\",\"serie\":\"106\"},{\"chave\":\"33180407170938001502551530003399021234480457\",\"numero\":\"339902\",\"valor\":\"1033.9\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"42180414055516000571550550002961041002418320\",\"numero\":\"296104\",\"valor\":\"976.87\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551510005969441229777138\",\"numero\":\"596944\",\"valor\":\"290.4\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551240003375851798188244\",\"numero\":\"337585\",\"valor\":\"431.68\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551530002866611923800005\",\"numero\":\"286661\",\"valor\":\"484.5\",\"dataEmissao\":\"23042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938050970551050005190271869738399\",\"numero\":\"519027\",\"valor\":\"649.9\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551540005226351574340725\",\"numero\":\"522635\",\"valor\":\"2194.0\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"35180407170938014078551100004539101803393841\",\"numero\":\"453910\",\"valor\":\"237.34\",\"dataEmissao\":\"24042018\",\"serie\":\"110\"},{\"chave\":\"35180407170938014078551170002573791080813969\",\"numero\":\"257379\",\"valor\":\"80.16\",\"dataEmissao\":\"23042018\",\"serie\":\"117\"},{\"chave\":\"33180407170938001502551540005226291428671405\",\"numero\":\"522629\",\"valor\":\"1546.55\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"33180407170938001502551530003395491997038961\",\"numero\":\"339549\",\"valor\":\"720.58\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938001766551410000591531499590287\",\"numero\":\"59153\",\"valor\":\"999.0\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938001766551410000591441432862723\",\"numero\":\"59144\",\"valor\":\"173.6\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551540005224131894870387\",\"numero\":\"522413\",\"valor\":\"35.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"33180407170938001502551510005978221376772477\",\"numero\":\"597822\",\"valor\":\"1038.99\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552080003786621373790317\",\"numero\":\"378662\",\"valor\":\"93.05\",\"dataEmissao\":\"25042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938050970551050005190371180796077\",\"numero\":\"519037\",\"valor\":\"394.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938050970551050005200131283482971\",\"numero\":\"520013\",\"valor\":\"584.96\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551120004048191805461800\",\"numero\":\"404819\",\"valor\":\"251.89\",\"dataEmissao\":\"23042018\",\"serie\":\"112\"},{\"chave\":\"35180407170938014078551530002864881144079893\",\"numero\":\"286488\",\"valor\":\"388.71\",\"dataEmissao\":\"23042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551170002573371985228734\",\"numero\":\"257337\",\"valor\":\"328.78\",\"dataEmissao\":\"23042018\",\"serie\":\"117\"},{\"chave\":\"35180407170938014078551230004349681591404049\",\"numero\":\"434968\",\"valor\":\"373.97\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078552060002842821014832322\",\"numero\":\"284282\",\"valor\":\"3319.45\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551250003333351370299031\",\"numero\":\"333335\",\"valor\":\"163.3\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078551230004349601398200170\",\"numero\":\"434960\",\"valor\":\"301.58\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"41180413986197000121550020003442211337611219\",\"numero\":\"344221\",\"valor\":\"1437.95\",\"dataEmissao\":\"23042018\",\"serie\":\"2\"},{\"chave\":\"41180413986197000121550020003424391681262048\",\"numero\":\"342439\",\"valor\":\"4812.85\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"42180414055516000571550550003012621005410130\",\"numero\":\"301262\",\"valor\":\"429.95\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551520004066681716926763\",\"numero\":\"406668\",\"valor\":\"814.8\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078552060002836151511238423\",\"numero\":\"283615\",\"valor\":\"193.23\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078552060002861511913544785\",\"numero\":\"286151\",\"valor\":\"1637.95\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"42180414055516000571550550002994521009215774\",\"numero\":\"299452\",\"valor\":\"218.49\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"42180414055516000571550550002976981005892845\",\"numero\":\"297698\",\"valor\":\"237.08\",\"dataEmissao\":\"21042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551240003342111481103195\",\"numero\":\"334211\",\"valor\":\"365.89\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551530003390321890352672\",\"numero\":\"339032\",\"valor\":\"1702.85\",\"dataEmissao\":\"20042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551530003392331187315473\",\"numero\":\"339233\",\"valor\":\"1718.9\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551510005978371751607294\",\"numero\":\"597837\",\"valor\":\"6018.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552060002834421167065493\",\"numero\":\"283442\",\"valor\":\"2154.99\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"33180407170938001502551550004137111255075828\",\"numero\":\"413711\",\"valor\":\"814.8\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551080002327331258710440\",\"numero\":\"232733\",\"valor\":\"384.89\",\"dataEmissao\":\"23042018\",\"serie\":\"108\"},{\"chave\":\"35180407170938014078552060002835711200830324\",\"numero\":\"283571\",\"valor\":\"809.7\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"33180407170938001502551540005228691961276213\",\"numero\":\"522869\",\"valor\":\"3538.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"41180413986197000121550020003424401681309053\",\"numero\":\"342440\",\"valor\":\"1867.78\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938001766551410000634741123234576\",\"numero\":\"63474\",\"valor\":\"1109.52\",\"dataEmissao\":\"24042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551100004538831436773744\",\"numero\":\"453883\",\"valor\":\"187.3\",\"dataEmissao\":\"24042018\",\"serie\":\"110\"},{\"chave\":\"35180407170938014078551220003276221193667541\",\"numero\":\"327622\",\"valor\":\"1822.9\",\"dataEmissao\":\"24042018\",\"serie\":\"122\"},{\"chave\":\"35180407170938014078552060002832411062975378\",\"numero\":\"283241\",\"valor\":\"469.78\",\"dataEmissao\":\"20042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551240003355991282504530\",\"numero\":\"335599\",\"valor\":\"1821.98\",\"dataEmissao\":\"23042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551530003390311007968702\",\"numero\":\"339031\",\"valor\":\"2463.93\",\"dataEmissao\":\"20042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551190003481561554442436\",\"numero\":\"348156\",\"valor\":\"273.83\",\"dataEmissao\":\"25042018\",\"serie\":\"119\"},{\"chave\":\"35180407170938014078551240003375841316954130\",\"numero\":\"337584\",\"valor\":\"101.93\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938001766551060001999381906486909\",\"numero\":\"199938\",\"valor\":\"212.13\",\"dataEmissao\":\"23042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938014078551510001351531882691781\",\"numero\":\"135153\",\"valor\":\"138.56\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551510005974691901621019\",\"numero\":\"597469\",\"valor\":\"1088.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552080003774481168987290\",\"numero\":\"377448\",\"valor\":\"129.9\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938014078551240003376531975946849\",\"numero\":\"337653\",\"valor\":\"470.94\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551160002017371262253241\",\"numero\":\"201737\",\"valor\":\"417.03\",\"dataEmissao\":\"24042018\",\"serie\":\"116\"},{\"chave\":\"33180407170938001502551520004065461969590019\",\"numero\":\"406546\",\"valor\":\"3438.9\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551510005988721783879900\",\"numero\":\"598872\",\"valor\":\"3533.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551170002583961020968600\",\"numero\":\"258396\",\"valor\":\"373.97\",\"dataEmissao\":\"25042018\",\"serie\":\"117\"},{\"chave\":\"33180407170938001502551520004063621532889830\",\"numero\":\"406362\",\"valor\":\"1397.86\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938001766551410000592501283122831\",\"numero\":\"59250\",\"valor\":\"558.56\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078552060002839911313937234\",\"numero\":\"283991\",\"valor\":\"1732.03\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551220003274581126987469\",\"numero\":\"327458\",\"valor\":\"1447.95\",\"dataEmissao\":\"24042018\",\"serie\":\"122\"},{\"chave\":\"35180407170938014078552060002867021948016163\",\"numero\":\"286702\",\"valor\":\"181.23\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551100004536471951227463\",\"numero\":\"453647\",\"valor\":\"311.87\",\"dataEmissao\":\"23042018\",\"serie\":\"110\"},{\"chave\":\"35180413477066000895550220001709701006386328\",\"numero\":\"170970\",\"valor\":\"1779.49\",\"dataEmissao\":\"23042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938014078551530002866971315916042\",\"numero\":\"286697\",\"valor\":\"263.81\",\"dataEmissao\":\"23042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938050970551020000605351791446330\",\"numero\":\"60535\",\"valor\":\"534.26\",\"dataEmissao\":\"23042018\",\"serie\":\"102\"},{\"chave\":\"35180407170938014078551170002572471822661189\",\"numero\":\"257247\",\"valor\":\"498.59\",\"dataEmissao\":\"23042018\",\"serie\":\"117\"},{\"chave\":\"33180407170938001502551540005224301651251844\",\"numero\":\"522430\",\"valor\":\"1713.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"35180407170938014078551250003330751616985067\",\"numero\":\"333075\",\"valor\":\"329.62\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938001766551410000592231397630219\",\"numero\":\"59223\",\"valor\":\"460.48\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551230004345541159087594\",\"numero\":\"434554\",\"valor\":\"206.05\",\"dataEmissao\":\"23042018\",\"serie\":\"123\"},{\"chave\":\"33180407170938001502551520004062121350199017\",\"numero\":\"406212\",\"valor\":\"499.12\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551200003163821210553325\",\"numero\":\"316382\",\"valor\":\"134.7\",\"dataEmissao\":\"25042018\",\"serie\":\"120\"},{\"chave\":\"33180407170938001502551510005980921141807519\",\"numero\":\"598092\",\"valor\":\"1151.74\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938001766551410000592561054987501\",\"numero\":\"59256\",\"valor\":\"270.46\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938001766551410000612231344429206\",\"numero\":\"61223\",\"valor\":\"540.86\",\"dataEmissao\":\"23042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551520004066571267338823\",\"numero\":\"406657\",\"valor\":\"2433.9\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551730001476151920297110\",\"numero\":\"147615\",\"valor\":\"570.82\",\"dataEmissao\":\"24042018\",\"serie\":\"173\"},{\"chave\":\"33180407170938001502551510005970621224140380\",\"numero\":\"597062\",\"valor\":\"1713.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551730001475561575182412\",\"numero\":\"147556\",\"valor\":\"1038.99\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938050970551050005200161821738304\",\"numero\":\"520016\",\"valor\":\"629.52\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078552080003772411077819040\",\"numero\":\"377241\",\"valor\":\"166.98\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"42180414055516000571550550002964891006174089\",\"numero\":\"296489\",\"valor\":\"201.44\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"41180413986197000121550020003424381681230700\",\"numero\":\"342438\",\"valor\":\"1735.05\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551530003403171424202949\",\"numero\":\"340317\",\"valor\":\"899.9\",\"dataEmissao\":\"25042018\",\"serie\":\"153\"},{\"chave\":\"42180414055516000571550550002983751009068458\",\"numero\":\"298375\",\"valor\":\"974.16\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"42180414055516000571550550002954671008325414\",\"numero\":\"295467\",\"valor\":\"492.0\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551230004346471520346333\",\"numero\":\"434647\",\"valor\":\"129.49\",\"dataEmissao\":\"23042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551830008367871542559716\",\"numero\":\"836787\",\"valor\":\"2491.0\",\"dataEmissao\":\"24042018\",\"serie\":\"183\"},{\"chave\":\"41180413986197000121550020003424421681418736\",\"numero\":\"342442\",\"valor\":\"1413.88\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551510005970981179817480\",\"numero\":\"597098\",\"valor\":\"2033.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552080003782131639365594\",\"numero\":\"378213\",\"valor\":\"110.14\",\"dataEmissao\":\"24042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938014078552060002839541383697778\",\"numero\":\"283954\",\"valor\":\"2872.09\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938050970551050005190311766358367\",\"numero\":\"519031\",\"valor\":\"1109.52\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551240003376301413227578\",\"numero\":\"337630\",\"valor\":\"303.87\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551240003348091664337319\",\"numero\":\"334809\",\"valor\":\"215.55\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551550004133301391294220\",\"numero\":\"413330\",\"valor\":\"1151.74\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551190003460031288550580\",\"numero\":\"346003\",\"valor\":\"893.01\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"33180407170938001502551510005971961216972643\",\"numero\":\"597196\",\"valor\":\"1713.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551730001475881114513628\",\"numero\":\"147588\",\"valor\":\"2134.89\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"33180407170938001502551550004128461193851885\",\"numero\":\"412846\",\"valor\":\"3433.9\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"33180407170938001502551530003389981220948431\",\"numero\":\"338998\",\"valor\":\"901.67\",\"dataEmissao\":\"20042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551510005984771968431463\",\"numero\":\"598477\",\"valor\":\"2603.95\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551510005979681773704774\",\"numero\":\"597968\",\"valor\":\"628.99\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551550004124911780916066\",\"numero\":\"412491\",\"valor\":\"474.27\",\"dataEmissao\":\"20042018\",\"serie\":\"155\"},{\"chave\":\"33180407170938001502551540005229791142436585\",\"numero\":\"522979\",\"valor\":\"1033.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"42180414055516000571550550002953141002417539\",\"numero\":\"295314\",\"valor\":\"291.3\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551060002640681432727395\",\"numero\":\"264068\",\"valor\":\"233.62\",\"dataEmissao\":\"24042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938001766551410000612291240206182\",\"numero\":\"61229\",\"valor\":\"3725.12\",\"dataEmissao\":\"23042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551240003343581427135183\",\"numero\":\"334358\",\"valor\":\"457.89\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938001766551410000612341401789451\",\"numero\":\"61234\",\"valor\":\"538.96\",\"dataEmissao\":\"23042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551540005230731074312857\",\"numero\":\"523073\",\"valor\":\"1333.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"35180413477066000895550220001714231002649488\",\"numero\":\"171423\",\"valor\":\"143.81\",\"dataEmissao\":\"25042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938001766551410000592141592467330\",\"numero\":\"59214\",\"valor\":\"117.44\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551240003375761292905426\",\"numero\":\"337576\",\"valor\":\"368.29\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551510005983731012634864\",\"numero\":\"598373\",\"valor\":\"1338.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938001766551410000612021160437869\",\"numero\":\"61202\",\"valor\":\"1240.58\",\"dataEmissao\":\"23042018\",\"serie\":\"141\"},{\"chave\":\"42180414055516000571550550002965891006174025\",\"numero\":\"296589\",\"valor\":\"370.76\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014230551020000356531144488646\",\"numero\":\"35653\",\"valor\":\"180.1\",\"dataEmissao\":\"23042018\",\"serie\":\"102\"},{\"chave\":\"33180407170938001502551510005974681580304807\",\"numero\":\"597468\",\"valor\":\"35.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551510005968821385830364\",\"numero\":\"596882\",\"valor\":\"1038.9\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552060002863391892557340\",\"numero\":\"286339\",\"valor\":\"340.38\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551200003145561242732480\",\"numero\":\"314556\",\"valor\":\"462.24\",\"dataEmissao\":\"20042018\",\"serie\":\"120\"},{\"chave\":\"42180414055516000571550550002998931001621131\",\"numero\":\"299893\",\"valor\":\"603.24\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551550004129521642413912\",\"numero\":\"412952\",\"valor\":\"819.8\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"33180407170938001502551510005967831133035979\",\"numero\":\"596783\",\"valor\":\"2284.0\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551230004348811288423233\",\"numero\":\"434881\",\"valor\":\"138.7\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551240003375931001347740\",\"numero\":\"337593\",\"valor\":\"629.54\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551510001355721728869255\",\"numero\":\"135572\",\"valor\":\"523.11\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551520004070871685971557\",\"numero\":\"407087\",\"valor\":\"1151.74\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551250003332461166734602\",\"numero\":\"333246\",\"valor\":\"348.28\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"33180407170938001413551100000561171293798235\",\"numero\":\"56117\",\"valor\":\"21604.0\",\"dataEmissao\":\"20042018\",\"serie\":\"110\"},{\"chave\":\"33180407170938001502551510005979291992496028\",\"numero\":\"597929\",\"valor\":\"3503.76\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552080003781571865364012\",\"numero\":\"378157\",\"valor\":\"493.74\",\"dataEmissao\":\"24042018\",\"serie\":\"208\"},{\"chave\":\"33180407170938001502551510005992111382610630\",\"numero\":\"599211\",\"valor\":\"1592.0\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551150002415851150697649\",\"numero\":\"241585\",\"valor\":\"772.99\",\"dataEmissao\":\"23042018\",\"serie\":\"115\"},{\"chave\":\"41180413986197000121550020003442221337658220\",\"numero\":\"344222\",\"valor\":\"2319.43\",\"dataEmissao\":\"23042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551740001753941567961880\",\"numero\":\"175394\",\"valor\":\"991.18\",\"dataEmissao\":\"20042018\",\"serie\":\"174\"},{\"chave\":\"35180407170938014078551240003343601006630020\",\"numero\":\"334360\",\"valor\":\"271.84\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938050970551050005190251797913233\",\"numero\":\"519025\",\"valor\":\"394.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551510005969831384752532\",\"numero\":\"596983\",\"valor\":\"458.89\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551240003354551684162820\",\"numero\":\"335455\",\"valor\":\"153.23\",\"dataEmissao\":\"23042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551550004126061897210201\",\"numero\":\"412606\",\"valor\":\"1827.76\",\"dataEmissao\":\"20042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551040002788741963357947\",\"numero\":\"278874\",\"valor\":\"244.89\",\"dataEmissao\":\"23042018\",\"serie\":\"104\"},{\"chave\":\"35180407170938014078551230004350411029701046\",\"numero\":\"435041\",\"valor\":\"171.2\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078552060002865841939620023\",\"numero\":\"286584\",\"valor\":\"1661.55\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"13180476487032005437550010000211691762605776\",\"numero\":\"21169\",\"valor\":\"13557.44\",\"dataEmissao\":\"23042018\",\"serie\":\"1\"},{\"chave\":\"42180414055516000571550550002977081004992844\",\"numero\":\"297708\",\"valor\":\"1759.72\",\"dataEmissao\":\"21042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551200003145511632851348\",\"numero\":\"314551\",\"valor\":\"274.89\",\"dataEmissao\":\"20042018\",\"serie\":\"120\"},{\"chave\":\"35180407170938014078551510001351301151400524\",\"numero\":\"135130\",\"valor\":\"367.39\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551220003274011932181642\",\"numero\":\"327401\",\"valor\":\"3146.0\",\"dataEmissao\":\"24042018\",\"serie\":\"122\"},{\"chave\":\"33180407170938001502551730001475591539786062\",\"numero\":\"147559\",\"valor\":\"1300.58\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551240003348121028540163\",\"numero\":\"334812\",\"valor\":\"272.56\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938050970551050005200191781516448\",\"numero\":\"520019\",\"valor\":\"676.4\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551520004064421345682414\",\"numero\":\"406442\",\"valor\":\"1351.8\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"42180414055516000571550550002986711000158699\",\"numero\":\"298671\",\"valor\":\"139.29\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938050970551050005190411037672486\",\"numero\":\"519041\",\"valor\":\"314.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551100004546261291949232\",\"numero\":\"454626\",\"valor\":\"158.24\",\"dataEmissao\":\"25042018\",\"serie\":\"110\"},{\"chave\":\"35180407170938001766551410000612071113004419\",\"numero\":\"61207\",\"valor\":\"518.17\",\"dataEmissao\":\"23042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078552060002862131134887894\",\"numero\":\"286213\",\"valor\":\"1199.0\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"41180413986197000121550020003471031695754162\",\"numero\":\"347103\",\"valor\":\"1973.99\",\"dataEmissao\":\"24042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551240003343631076271470\",\"numero\":\"334363\",\"valor\":\"295.47\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551190003464031134917931\",\"numero\":\"346403\",\"valor\":\"124.25\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"33180407170938001502551520004065341824876182\",\"numero\":\"406534\",\"valor\":\"1718.9\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551240003349651518542501\",\"numero\":\"334965\",\"valor\":\"174.01\",\"dataEmissao\":\"23042018\",\"serie\":\"124\"},{\"chave\":\"42180414055516000571550550002991591002174490\",\"numero\":\"299159\",\"valor\":\"744.66\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551160002018141819455956\",\"numero\":\"201814\",\"valor\":\"563.34\",\"dataEmissao\":\"25042018\",\"serie\":\"116\"},{\"chave\":\"35180407170938014078551190003466741329214681\",\"numero\":\"346674\",\"valor\":\"1989.45\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"33180407170938001502551520004073251538525300\",\"numero\":\"407325\",\"valor\":\"1354.85\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551530003400071618657825\",\"numero\":\"340007\",\"valor\":\"1399.09\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180413477066000895550240001630341002385251\",\"numero\":\"163034\",\"valor\":\"86.98\",\"dataEmissao\":\"20042018\",\"serie\":\"24\"},{\"chave\":\"35180413477066000895550220001709321007013613\",\"numero\":\"170932\",\"valor\":\"251.88\",\"dataEmissao\":\"23042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938014078551210002020691491504130\",\"numero\":\"202069\",\"valor\":\"836.72\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"35180407170938014078551240003377171932338200\",\"numero\":\"337717\",\"valor\":\"772.99\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"41180413986197000121550020003424411681356067\",\"numero\":\"342441\",\"valor\":\"1025.8\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551250003332631421672930\",\"numero\":\"333263\",\"valor\":\"244.89\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078551230004349571643618903\",\"numero\":\"434957\",\"valor\":\"200.23\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"33180407170938001502551550004133891422249970\",\"numero\":\"413389\",\"valor\":\"1119.0\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078552060002864501099038649\",\"numero\":\"286450\",\"valor\":\"297.07\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938001766551410000634701724958251\",\"numero\":\"63470\",\"valor\":\"980.4\",\"dataEmissao\":\"24042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551530003392811293256822\",\"numero\":\"339281\",\"valor\":\"939.0\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551520004068441412667503\",\"numero\":\"406844\",\"valor\":\"840.61\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551070002783351863720709\",\"numero\":\"278335\",\"valor\":\"195.23\",\"dataEmissao\":\"23042018\",\"serie\":\"107\"},{\"chave\":\"33180407170938001502551510005970321335496460\",\"numero\":\"597032\",\"valor\":\"148.48\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551240003376711251303757\",\"numero\":\"337671\",\"valor\":\"368.29\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551510005988641468125545\",\"numero\":\"598864\",\"valor\":\"2170.85\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938001766551410000592411869237378\",\"numero\":\"59241\",\"valor\":\"1092.06\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078552060002832681908742077\",\"numero\":\"283268\",\"valor\":\"2213.51\",\"dataEmissao\":\"20042018\",\"serie\":\"206\"},{\"chave\":\"42180414055516000571550550002986011000188690\",\"numero\":\"298601\",\"valor\":\"503.85\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078552060002836541265598013\",\"numero\":\"283654\",\"valor\":\"1373.09\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180413477066000895550240001631531007645040\",\"numero\":\"163153\",\"valor\":\"73.11\",\"dataEmissao\":\"20042018\",\"serie\":\"24\"},{\"chave\":\"35180407170938014078551200003158581717155232\",\"numero\":\"315858\",\"valor\":\"244.89\",\"dataEmissao\":\"24042018\",\"serie\":\"120\"},{\"chave\":\"33180407170938001502551520004064301521613806\",\"numero\":\"406430\",\"valor\":\"1489.88\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551510005976631421702071\",\"numero\":\"597663\",\"valor\":\"1048.88\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552080003778011318175690\",\"numero\":\"377801\",\"valor\":\"193.28\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"33180407170938001502551730001475251134098405\",\"numero\":\"147525\",\"valor\":\"1551.0\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551240003365161906127377\",\"numero\":\"336516\",\"valor\":\"218.35\",\"dataEmissao\":\"24042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551530002865001824885591\",\"numero\":\"286500\",\"valor\":\"324.18\",\"dataEmissao\":\"23042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551210002010601154695887\",\"numero\":\"201060\",\"valor\":\"926.62\",\"dataEmissao\":\"23042018\",\"serie\":\"121\"},{\"chave\":\"33180407170938001502551530003398781730813186\",\"numero\":\"339878\",\"valor\":\"1312.89\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551180002960081490600939\",\"numero\":\"296008\",\"valor\":\"260.66\",\"dataEmissao\":\"20042018\",\"serie\":\"118\"},{\"chave\":\"35180413477066000895550240001628781000491520\",\"numero\":\"162878\",\"valor\":\"83.23\",\"dataEmissao\":\"19042018\",\"serie\":\"24\"},{\"chave\":\"35180407170938014078551190003481671432698235\",\"numero\":\"348167\",\"valor\":\"187.3\",\"dataEmissao\":\"25042018\",\"serie\":\"119\"},{\"chave\":\"35180407170938014078552060002832981447912654\",\"numero\":\"283298\",\"valor\":\"157.6\",\"dataEmissao\":\"20042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078552060002854421970809590\",\"numero\":\"285442\",\"valor\":\"282.07\",\"dataEmissao\":\"24042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078552080003793421428584638\",\"numero\":\"379342\",\"valor\":\"301.58\",\"dataEmissao\":\"25042018\",\"serie\":\"208\"},{\"chave\":\"33180407170938001502551480002266521700537210\",\"numero\":\"226652\",\"valor\":\"302.38\",\"dataEmissao\":\"24042018\",\"serie\":\"148\"},{\"chave\":\"35180407170938014078552060002841951934321195\",\"numero\":\"284195\",\"valor\":\"1661.55\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"13180476487032005437550010000208661615692420\",\"numero\":\"20866\",\"valor\":\"11295.4\",\"dataEmissao\":\"19042018\",\"serie\":\"1\"},{\"chave\":\"35180407170938014078552080003772481338416271\",\"numero\":\"377248\",\"valor\":\"208.35\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938014078551250003332371208936560\",\"numero\":\"333237\",\"valor\":\"187.3\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078552080003792291723704609\",\"numero\":\"379229\",\"valor\":\"134.81\",\"dataEmissao\":\"25042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938050970551050005190331019092200\",\"numero\":\"519033\",\"valor\":\"699.71\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551250003327221872214953\",\"numero\":\"332722\",\"valor\":\"143.75\",\"dataEmissao\":\"24042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078551400000340071286352266\",\"numero\":\"34007\",\"valor\":\"181.65\",\"dataEmissao\":\"24042018\",\"serie\":\"140\"},{\"chave\":\"33180407170938001502551540005228801493330378\",\"numero\":\"522880\",\"valor\":\"1184.48\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"35180407170938014230551020000355591085561301\",\"numero\":\"35559\",\"valor\":\"1490.0\",\"dataEmissao\":\"20042018\",\"serie\":\"102\"},{\"chave\":\"33180407170938001502551750001150731144771261\",\"numero\":\"115073\",\"valor\":\"2167.85\",\"dataEmissao\":\"20042018\",\"serie\":\"175\"},{\"chave\":\"35180407170938014078551230004349701640290194\",\"numero\":\"434970\",\"valor\":\"257.35\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551170002583941020342502\",\"numero\":\"258394\",\"valor\":\"500.8\",\"dataEmissao\":\"25042018\",\"serie\":\"117\"},{\"chave\":\"33180407170938001502551730001475491239694360\",\"numero\":\"147549\",\"valor\":\"1386.88\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551100004538871833828072\",\"numero\":\"453887\",\"valor\":\"384.89\",\"dataEmissao\":\"24042018\",\"serie\":\"110\"},{\"chave\":\"35180407170938014078551250003324851355676930\",\"numero\":\"332485\",\"valor\":\"244.89\",\"dataEmissao\":\"23042018\",\"serie\":\"125\"},{\"chave\":\"42180414055516000571550550002954561009525170\",\"numero\":\"295456\",\"valor\":\"336.88\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180413477066000895550220001714181007191412\",\"numero\":\"171418\",\"valor\":\"257.09\",\"dataEmissao\":\"25042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938014078552080003787901971042187\",\"numero\":\"378790\",\"valor\":\"144.72\",\"dataEmissao\":\"25042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938001766551410000592001832584390\",\"numero\":\"59200\",\"valor\":\"312.21\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551230004349521218511927\",\"numero\":\"434952\",\"valor\":\"325.08\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551240003348871221209610\",\"numero\":\"334887\",\"valor\":\"342.36\",\"dataEmissao\":\"23042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551160002017671559761880\",\"numero\":\"201767\",\"valor\":\"371.77\",\"dataEmissao\":\"24042018\",\"serie\":\"116\"},{\"chave\":\"33180407170938001502551540005233151924703339\",\"numero\":\"523315\",\"valor\":\"1780.06\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"41180413986197000121550020003471011695628812\",\"numero\":\"347101\",\"valor\":\"1970.16\",\"dataEmissao\":\"24042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551510005982241399222798\",\"numero\":\"598224\",\"valor\":\"569.2\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"42180414055516000571550550003003311008228280\",\"numero\":\"300331\",\"valor\":\"189.04\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551170002584271224338298\",\"numero\":\"258427\",\"valor\":\"223.43\",\"dataEmissao\":\"25042018\",\"serie\":\"117\"},{\"chave\":\"33180407170938001502551520004069521432335511\",\"numero\":\"406952\",\"valor\":\"6018.9\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551520004070891188126403\",\"numero\":\"407089\",\"valor\":\"1184.48\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551120004047431392956952\",\"numero\":\"404743\",\"valor\":\"384.89\",\"dataEmissao\":\"23042018\",\"serie\":\"112\"},{\"chave\":\"33180407170938001502551530003390211125962066\",\"numero\":\"339021\",\"valor\":\"1511.85\",\"dataEmissao\":\"20042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551550004133241953502620\",\"numero\":\"413324\",\"valor\":\"1151.74\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"33180407170938001502551520004067351014951207\",\"numero\":\"406735\",\"valor\":\"35.9\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551190003471231766858002\",\"numero\":\"347123\",\"valor\":\"2141.54\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"35180407170938014078551240003343731654329597\",\"numero\":\"334373\",\"valor\":\"203.35\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078552060002837981535165114\",\"numero\":\"283798\",\"valor\":\"3527.4\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551040002787991916457586\",\"numero\":\"278799\",\"valor\":\"256.45\",\"dataEmissao\":\"23042018\",\"serie\":\"104\"},{\"chave\":\"35180407170938014078551510001351341774926778\",\"numero\":\"135134\",\"valor\":\"157.29\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"41180413986197000121550020003442231337736571\",\"numero\":\"344223\",\"valor\":\"2099.0\",\"dataEmissao\":\"23042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551830008368841233690269\",\"numero\":\"836884\",\"valor\":\"2991.4\",\"dataEmissao\":\"24042018\",\"serie\":\"183\"},{\"chave\":\"35180407170938014078552080003783341228584395\",\"numero\":\"378334\",\"valor\":\"204.3\",\"dataEmissao\":\"24042018\",\"serie\":\"208\"},{\"chave\":\"33180407170938001502551530003398281909898714\",\"numero\":\"339828\",\"valor\":\"1251.77\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078552060002854641443620227\",\"numero\":\"285464\",\"valor\":\"163.3\",\"dataEmissao\":\"24042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551240003361051562271094\",\"numero\":\"336105\",\"valor\":\"344.85\",\"dataEmissao\":\"23042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551100004536571188903260\",\"numero\":\"453657\",\"valor\":\"384.89\",\"dataEmissao\":\"23042018\",\"serie\":\"110\"},{\"chave\":\"35180407170938001766551060001999401857463376\",\"numero\":\"199940\",\"valor\":\"352.09\",\"dataEmissao\":\"23042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938014078551230004348471959234801\",\"numero\":\"434847\",\"valor\":\"206.05\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938001766551060002002271020419769\",\"numero\":\"200227\",\"valor\":\"1819.44\",\"dataEmissao\":\"24042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938014078551070002784471192573240\",\"numero\":\"278447\",\"valor\":\"267.86\",\"dataEmissao\":\"23042018\",\"serie\":\"107\"},{\"chave\":\"33180407170938001502551520004063711250604820\",\"numero\":\"406371\",\"valor\":\"1285.41\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551510005979631734529324\",\"numero\":\"597963\",\"valor\":\"434.89\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551080002327131967519537\",\"numero\":\"232713\",\"valor\":\"197.3\",\"dataEmissao\":\"23042018\",\"serie\":\"108\"},{\"chave\":\"35180407170938014078551240003375831285160105\",\"numero\":\"337583\",\"valor\":\"79.18\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551230004348641614555671\",\"numero\":\"434864\",\"valor\":\"566.56\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551510001355411197511979\",\"numero\":\"135541\",\"valor\":\"273.62\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938050970551050005200081843308521\",\"numero\":\"520008\",\"valor\":\"1141.92\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551510005973461484741328\",\"numero\":\"597346\",\"valor\":\"831.67\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938050970551050005190291798250121\",\"numero\":\"519029\",\"valor\":\"314.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551510001354001891575317\",\"numero\":\"135400\",\"valor\":\"222.44\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"41180413986197000121550020003419731739793010\",\"numero\":\"341973\",\"valor\":\"3629.41\",\"dataEmissao\":\"20042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551520004081561680151660\",\"numero\":\"408156\",\"valor\":\"1012.58\",\"dataEmissao\":\"25042018\",\"serie\":\"152\"},{\"chave\":\"35180407170938014078551250003324991176019418\",\"numero\":\"332499\",\"valor\":\"244.89\",\"dataEmissao\":\"23042018\",\"serie\":\"125\"},{\"chave\":\"33180407170938001502551530003399081698714310\",\"numero\":\"339908\",\"valor\":\"1479.0\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551960002812681375624298\",\"numero\":\"281268\",\"valor\":\"196.95\",\"dataEmissao\":\"25042018\",\"serie\":\"196\"},{\"chave\":\"33180407170938001502551550004130331552965266\",\"numero\":\"413033\",\"valor\":\"1153.9\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551510001353181804688966\",\"numero\":\"135318\",\"valor\":\"219.7\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"42180414055516000571550550002992331000621129\",\"numero\":\"299233\",\"valor\":\"267.17\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551100004536751257165186\",\"numero\":\"453675\",\"valor\":\"77.75\",\"dataEmissao\":\"23042018\",\"serie\":\"110\"},{\"chave\":\"35180413477066000895550240001630671007525621\",\"numero\":\"163067\",\"valor\":\"71.28\",\"dataEmissao\":\"20042018\",\"serie\":\"24\"},{\"chave\":\"42180414055516000571550550003002291007131174\",\"numero\":\"300229\",\"valor\":\"191.42\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551090003335281982664173\",\"numero\":\"333528\",\"valor\":\"368.29\",\"dataEmissao\":\"23042018\",\"serie\":\"109\"},{\"chave\":\"35180407170938050970551050005190241560326155\",\"numero\":\"519024\",\"valor\":\"422.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551130003692201224382975\",\"numero\":\"369220\",\"valor\":\"388.83\",\"dataEmissao\":\"24042018\",\"serie\":\"113\"},{\"chave\":\"35180407170938014078552060002838081855935199\",\"numero\":\"283808\",\"valor\":\"1752.99\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938050970551050005190351931050504\",\"numero\":\"519035\",\"valor\":\"279.42\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551730001475691320927291\",\"numero\":\"147569\",\"valor\":\"801.64\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551230004349491007329660\",\"numero\":\"434949\",\"valor\":\"549.78\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"33180407170938001502551510005964971088158786\",\"numero\":\"596497\",\"valor\":\"621.3\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078552060002851571539228319\",\"numero\":\"285157\",\"valor\":\"1843.15\",\"dataEmissao\":\"24042018\",\"serie\":\"206\"},{\"chave\":\"33180407170938001502551550004131781375925248\",\"numero\":\"413178\",\"valor\":\"1234.0\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551510001355371178867833\",\"numero\":\"135537\",\"valor\":\"856.6\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"42180414055516000571550550002955421002915720\",\"numero\":\"295542\",\"valor\":\"396.26\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938001766551410000591701637577737\",\"numero\":\"59170\",\"valor\":\"572.46\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551510005976591102999597\",\"numero\":\"597659\",\"valor\":\"809.45\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"41180413986197000121550020003424371681168020\",\"numero\":\"342437\",\"valor\":\"1819.88\",\"dataEmissao\":\"21042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551230004358111002426545\",\"numero\":\"435811\",\"valor\":\"296.89\",\"dataEmissao\":\"25042018\",\"serie\":\"123\"},{\"chave\":\"33180407170938001502551550004133251518891349\",\"numero\":\"413325\",\"valor\":\"1151.51\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938050970551050005190221053913543\",\"numero\":\"519022\",\"valor\":\"394.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078552060002836521343096560\",\"numero\":\"283652\",\"valor\":\"1447.95\",\"dataEmissao\":\"23042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551530002869731246582548\",\"numero\":\"286973\",\"valor\":\"424.12\",\"dataEmissao\":\"25042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551960002813061517409491\",\"numero\":\"281306\",\"valor\":\"155.35\",\"dataEmissao\":\"25042018\",\"serie\":\"196\"},{\"chave\":\"33180407170938001502551730001475831187648837\",\"numero\":\"147583\",\"valor\":\"2538.9\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551210002022001115879708\",\"numero\":\"202200\",\"valor\":\"186.73\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"41180413986197000121550020003471001695566135\",\"numero\":\"347100\",\"valor\":\"4762.85\",\"dataEmissao\":\"24042018\",\"serie\":\"2\"},{\"chave\":\"33180407170938001502551510005977751026506300\",\"numero\":\"597775\",\"valor\":\"684.33\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551210002020121790370842\",\"numero\":\"202012\",\"valor\":\"383.09\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"35180407170938014078551240003374681194987900\",\"numero\":\"337468\",\"valor\":\"203.55\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938001766551060001996011076360820\",\"numero\":\"199601\",\"valor\":\"1506.6\",\"dataEmissao\":\"22042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938014230551020000355681776194352\",\"numero\":\"35568\",\"valor\":\"1791.18\",\"dataEmissao\":\"23042018\",\"serie\":\"102\"},{\"chave\":\"35180407170938014078552060002865441161489031\",\"numero\":\"286544\",\"valor\":\"1648.4\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551170002579131708524674\",\"numero\":\"257913\",\"valor\":\"378.04\",\"dataEmissao\":\"24042018\",\"serie\":\"117\"},{\"chave\":\"35180407170938014078552060002833021629125784\",\"numero\":\"283302\",\"valor\":\"2070.97\",\"dataEmissao\":\"20042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078552060002864531587567383\",\"numero\":\"286453\",\"valor\":\"2213.6\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"33180407170938001502551550004134731441858337\",\"numero\":\"413473\",\"valor\":\"2014.0\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"42180414055516000571550550003003241002522549\",\"numero\":\"300324\",\"valor\":\"225.01\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551730001475931021298468\",\"numero\":\"147593\",\"valor\":\"2228.0\",\"dataEmissao\":\"24042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551210002022531863908921\",\"numero\":\"202253\",\"valor\":\"346.89\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"35180407170938001766551410000592091700197513\",\"numero\":\"59209\",\"valor\":\"1055.77\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"35180407170938014078551120004048541029164843\",\"numero\":\"404854\",\"valor\":\"175.23\",\"dataEmissao\":\"23042018\",\"serie\":\"112\"},{\"chave\":\"35180407170938014078552080003772741324782220\",\"numero\":\"377274\",\"valor\":\"263.72\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"33180407170938001502551510005999681322049380\",\"numero\":\"599968\",\"valor\":\"3330.89\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551530003395421332636064\",\"numero\":\"339542\",\"valor\":\"651.59\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938014078551230004350131990396632\",\"numero\":\"435013\",\"valor\":\"284.72\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551530002869761530939368\",\"numero\":\"286976\",\"valor\":\"363.02\",\"dataEmissao\":\"25042018\",\"serie\":\"153\"},{\"chave\":\"35180407170938050970551050005200221616570810\",\"numero\":\"520022\",\"valor\":\"1876.29\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"35180407170938014078551250003332641686684381\",\"numero\":\"333264\",\"valor\":\"388.8\",\"dataEmissao\":\"25042018\",\"serie\":\"125\"},{\"chave\":\"35180407170938014078552060002865701133127660\",\"numero\":\"286570\",\"valor\":\"1488.1\",\"dataEmissao\":\"25042018\",\"serie\":\"206\"},{\"chave\":\"35180407170938014078551240003348161576632305\",\"numero\":\"334816\",\"valor\":\"149.59\",\"dataEmissao\":\"20042018\",\"serie\":\"124\"},{\"chave\":\"33180407170938001502551510005975701038221413\",\"numero\":\"597570\",\"valor\":\"3433.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551520004069301647490476\",\"numero\":\"406930\",\"valor\":\"721.2\",\"dataEmissao\":\"24042018\",\"serie\":\"152\"},{\"chave\":\"33180407170938001502551530003390221337763366\",\"numero\":\"339022\",\"valor\":\"1614.61\",\"dataEmissao\":\"20042018\",\"serie\":\"153\"},{\"chave\":\"42180414055516000571550550002976951009042054\",\"numero\":\"297695\",\"valor\":\"349.54\",\"dataEmissao\":\"21042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551200003165991875490050\",\"numero\":\"316599\",\"valor\":\"142.13\",\"dataEmissao\":\"25042018\",\"serie\":\"120\"},{\"chave\":\"42180414055516000571550550002955461009552177\",\"numero\":\"295546\",\"valor\":\"351.71\",\"dataEmissao\":\"20042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078552080003774981688631344\",\"numero\":\"377498\",\"valor\":\"129.49\",\"dataEmissao\":\"23042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938050970551050005200251610575846\",\"numero\":\"520025\",\"valor\":\"784.36\",\"dataEmissao\":\"23042018\",\"serie\":\"105\"},{\"chave\":\"35180413477066000895550220001711321003013636\",\"numero\":\"171132\",\"valor\":\"99.16\",\"dataEmissao\":\"24042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938014078551210002022621106916682\",\"numero\":\"202262\",\"valor\":\"364.89\",\"dataEmissao\":\"24042018\",\"serie\":\"121\"},{\"chave\":\"35180413477066000895550220001706501006351320\",\"numero\":\"170650\",\"valor\":\"60.97\",\"dataEmissao\":\"20042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938014078551230004348781193168630\",\"numero\":\"434878\",\"valor\":\"244.89\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551110003425571979669416\",\"numero\":\"342557\",\"valor\":\"275.03\",\"dataEmissao\":\"23042018\",\"serie\":\"111\"},{\"chave\":\"35180407170938014078551190003474541751200092\",\"numero\":\"347454\",\"valor\":\"284.89\",\"dataEmissao\":\"24042018\",\"serie\":\"119\"},{\"chave\":\"35180407170938014078551230004348761938863070\",\"numero\":\"434876\",\"valor\":\"296.47\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551170002585781140090869\",\"numero\":\"258578\",\"valor\":\"445.91\",\"dataEmissao\":\"25042018\",\"serie\":\"117\"},{\"chave\":\"35180407170938014078551190003452441930533545\",\"numero\":\"345244\",\"valor\":\"407.76\",\"dataEmissao\":\"20042018\",\"serie\":\"119\"},{\"chave\":\"35180413477066000895550240001632341002385457\",\"numero\":\"163234\",\"valor\":\"361.24\",\"dataEmissao\":\"20042018\",\"serie\":\"24\"},{\"chave\":\"33180407170938001502551510005985061236218315\",\"numero\":\"598506\",\"valor\":\"1633.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551190003459941699156060\",\"numero\":\"345994\",\"valor\":\"910.99\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"35180407170938014078551060002639431730194684\",\"numero\":\"263943\",\"valor\":\"384.89\",\"dataEmissao\":\"23042018\",\"serie\":\"106\"},{\"chave\":\"33180407170938001502551550004132661848761420\",\"numero\":\"413266\",\"valor\":\"504.04\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078551170002585921669273455\",\"numero\":\"258592\",\"valor\":\"419.96\",\"dataEmissao\":\"25042018\",\"serie\":\"117\"},{\"chave\":\"35180407170938050970551050005190391444921584\",\"numero\":\"519039\",\"valor\":\"394.76\",\"dataEmissao\":\"22042018\",\"serie\":\"105\"},{\"chave\":\"33180407170938001502551550004129271262098370\",\"numero\":\"412927\",\"valor\":\"939.0\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"33180407170938001502551730001475581237186901\",\"numero\":\"147558\",\"valor\":\"929.89\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180407170938014078551170002572701278402232\",\"numero\":\"257270\",\"valor\":\"422.2\",\"dataEmissao\":\"23042018\",\"serie\":\"117\"},{\"chave\":\"42180414055516000571550550002983761009603172\",\"numero\":\"298376\",\"valor\":\"679.02\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"33180407170938001502551520004064241681265973\",\"numero\":\"406424\",\"valor\":\"333.72\",\"dataEmissao\":\"20042018\",\"serie\":\"152\"},{\"chave\":\"42180414055516000571550550003012641002523485\",\"numero\":\"301264\",\"valor\":\"786.98\",\"dataEmissao\":\"24042018\",\"serie\":\"55\"},{\"chave\":\"42180414055516000571550550002983611000218699\",\"numero\":\"298361\",\"valor\":\"316.81\",\"dataEmissao\":\"23042018\",\"serie\":\"55\"},{\"chave\":\"35180407170938014078551190003458081572455010\",\"numero\":\"345808\",\"valor\":\"769.8\",\"dataEmissao\":\"23042018\",\"serie\":\"119\"},{\"chave\":\"33180407170938001502551510005976521395160579\",\"numero\":\"597652\",\"valor\":\"5031.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551510005973801765339568\",\"numero\":\"597380\",\"valor\":\"2038.9\",\"dataEmissao\":\"24042018\",\"serie\":\"151\"},{\"chave\":\"35180407170938014078551230004358121941526460\",\"numero\":\"435812\",\"valor\":\"153.23\",\"dataEmissao\":\"25042018\",\"serie\":\"123\"},{\"chave\":\"33180407170938001502551540005230241025970159\",\"numero\":\"523024\",\"valor\":\"1524.9\",\"dataEmissao\":\"24042018\",\"serie\":\"154\"},{\"chave\":\"35180407170938014078551230004350111940695720\",\"numero\":\"435011\",\"valor\":\"119.25\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078551090003334601563975376\",\"numero\":\"333460\",\"valor\":\"241.38\",\"dataEmissao\":\"23042018\",\"serie\":\"109\"},{\"chave\":\"35180407170938014078551240003376181880087513\",\"numero\":\"337618\",\"valor\":\"294.83\",\"dataEmissao\":\"25042018\",\"serie\":\"124\"},{\"chave\":\"35180407170938014078551230004349561059460311\",\"numero\":\"434956\",\"valor\":\"289.9\",\"dataEmissao\":\"24042018\",\"serie\":\"123\"},{\"chave\":\"35180407170938014078552080003793371614489798\",\"numero\":\"379337\",\"valor\":\"305.07\",\"dataEmissao\":\"25042018\",\"serie\":\"208\"},{\"chave\":\"35180407170938014078551100004546281315165001\",\"numero\":\"454628\",\"valor\":\"289.35\",\"dataEmissao\":\"25042018\",\"serie\":\"110\"},{\"chave\":\"33180407170938001502551540005221771468848560\",\"numero\":\"522177\",\"valor\":\"2361.52\",\"dataEmissao\":\"20042018\",\"serie\":\"154\"},{\"chave\":\"33180407170938001502551730001475471239086315\",\"numero\":\"147547\",\"valor\":\"375.8\",\"dataEmissao\":\"20042018\",\"serie\":\"173\"},{\"chave\":\"35180413477066000895550220001709381008391496\",\"numero\":\"170938\",\"valor\":\"502.53\",\"dataEmissao\":\"23042018\",\"serie\":\"22\"},{\"chave\":\"35180407170938001766551410000592291494556498\",\"numero\":\"59229\",\"valor\":\"538.86\",\"dataEmissao\":\"22042018\",\"serie\":\"141\"},{\"chave\":\"33180407170938001502551510005999841235330653\",\"numero\":\"599984\",\"valor\":\"2280.29\",\"dataEmissao\":\"25042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551530003398421021627671\",\"numero\":\"339842\",\"valor\":\"3423.9\",\"dataEmissao\":\"24042018\",\"serie\":\"153\"},{\"chave\":\"33180407170938001502551730001476001611065239\",\"numero\":\"147600\",\"valor\":\"2134.89\",\"dataEmissao\":\"24042018\",\"serie\":\"173\"},{\"chave\":\"33180407170938001502551590002760211411522125\",\"numero\":\"276021\",\"valor\":\"850.78\",\"dataEmissao\":\"24042018\",\"serie\":\"159\"},{\"chave\":\"35180407170938014078551060002641161032723600\",\"numero\":\"264116\",\"valor\":\"244.89\",\"dataEmissao\":\"25042018\",\"serie\":\"106\"},{\"chave\":\"35180407170938014078551960002813081898044324\",\"numero\":\"281308\",\"valor\":\"86.18\",\"dataEmissao\":\"25042018\",\"serie\":\"196\"},{\"chave\":\"33180407170938001502551510005970331849556490\",\"numero\":\"597033\",\"valor\":\"261.28\",\"dataEmissao\":\"20042018\",\"serie\":\"151\"},{\"chave\":\"33180407170938001502551550004129171496757366\",\"numero\":\"412917\",\"valor\":\"1238.9\",\"dataEmissao\":\"24042018\",\"serie\":\"155\"},{\"chave\":\"35180407170938014078552060002849771500023777\",\"numero\":\"284977\",\"valor\":\"2213.51\",\"dataEmissao\":\"24042018\",\"serie\":\"206\"},{\"chave\":\"41180413986197000121550020003469081921089476\",\"numero\":\"346908\",\"valor\":\"3699.0\",\"dataEmissao\":\"23042018\",\"serie\":\"2\"},{\"chave\":\"35180407170938014078551110003423521955905368\",\"numero\":\"342352\",\"valor\":\"244.89\",\"dataEmissao\":\"23042018\",\"serie\":\"111\"}],\"registroPassagemSitram\":[\"25/03/2015 14:06:31\",\"25/03/2015 13:46:39\",\"25/03/2015 13:46:39\"]}";
	}
	
	private String getBuscarDadosEmpresaPlaca() {
		return "{\"DadosEmpresa\":{\"cnpj\":\"07.278.045/0001-71\",\"cgf\":\"06.178.747-7\",\"situacaoCnpj\":\"BAIX. DE OFICIO\",\"cnae\":\"1091101 - Fabricação de produtos de panificação Industrial\",\"razaoSocial\":\"PINHEIRO E SILVA COMERCIO VAREJISTA D\",\"nomeFantasia\":\"MERCANTIL COMPRE BEM\",\"ancora\":\"Não\",\"simplesNacional\":\"Sim\",\"dataSimplesNacional\":\"2010-10-12\",\"regimeRecolhimento\":\"MICROEMPRESA\",\"logradouro\":\"RUA JOSE GUIMARAES\",\"numero\":\"00312\",\"complemento\":\"PQ SANTA HELENA\",\"bairro\":\"JABOTI\",\"municipio\":\"EUSEBIO\",\"uf\":\"CE\",\"cep\":\"61700-0\"},\"regimeEspecial\":\"Não\"}";
	}
	
	private String getPendenciaTransitoLivreJson() {
		return "{ \"pendenciaTransitoLivre\":[ { \"numeroAcaoFiscal\":\"78448-63/4\", \"valorTotalAcao\":\"1253,00\" }, { \"numeroAcaoFiscal\":\"94625-11/2\", \"valorTotalAcao\":\"1783,00\" }, { \"numeroAcaoFiscal\":\"16547-71/9\", \"valorTotalAcao\":\"2185,00\" } ] }";
	}
	
	private String getTresPrincipaisContribuintesAutoInfracaoJson() {
		return " { \"valorTotalAutos\":\"16402,00\", \"principaisCgfs\":[ { \"cgfCnpjCpf\":\"07.135.453-6/ 50.081.644/0001-20/ 355.678.403-30\", \"razaoSocial\":\" GRENDENE S.A. \", \"numeroDiligencia\":\"571965/2018\" }, { \"cgfCnpjCpf\":\"32.562.212-3/ 52.947.082/0001-80/ 384.913.983-25\", \"razaoSocial\":\"WEG S.A.\", \"numeroDiligencia\":\"786221/2019\" }, { \"cgfCnpjCpf\":\"91.313.778-2/ 72.581.088/0001-93/ 195.305.213-40\", \"razaoSocial\":\"‎ENGIE BRASIL ENERGIA S.A.\", \"numeroDiligencia\":\"126978/2017\" } ] }";
	}
	
	private String getNfeEntradaInterestadualSemRegistroSitramJson() {
		return "{\"nfsSemRegistroSitram\":[{\"nfe\":\"1\",\"chaveAcesso\":\"41181114104691000188550020000000011635005704\",\"dataEmissao\":\"2018-11-19 10:09:00.000000\",\"valorTotal\":\"89.26\",\"situacao\":\"Autorizada\",\"cnpjEmitente\":\"14104691000188\",\"tipoDocumento\":\"Entrada\",\"tipoOperacao\":\"Interestadual\"},{\"nfe\":\"2\",\"chaveAcesso\":\"41181114104691000188550020000000021080020080\",\"dataEmissao\":\"2018-11-23 09:39:00.000000\",\"valorTotal\":\"309.71\",\"situacao\":\"Autorizada\",\"cnpjEmitente\":\"14104691000188\",\"tipoDocumento\":\"Entrada\",\"tipoOperacao\":\"Interestadual\"}]}";
	}
	
	private String getBuscarAutoInfracao() {
		return "{\"autoInfracao\":[{\"numero\":\"6001018000194\",\"orgao\":\"SECRETÁRIA DA FAZENDA\"}, {\"numero\":\"5346464376747\",\"orgao\":\"PROCURADORIA GERAL\"}]}";
	}
	
	private String getBuscarTransitoLivre() {
		return "{\"transitoLivre\":[{\"chave\":\"41181114104691000188550020000000011635005704\", \"numero\":\"16346423\", \"acaoFiscal\":\"787878789787\"}, {\"chave\":\"35181109619825000163550010000000061006006356\", \"numero\":\"12563465\", \"acaoFiscal\":\"78978787897\"}]}";
	}
	
	private String getBuscarTransportadorasUtilizadas() {
		return "{\"transportadorasEmissor\":[{\"cnpj\":\"56709562000163\",\"nome\":\"AVINE EMPRESA DE ALIMENTOS E LTDA\"}, {\"cnpj\":\"09563615000109\",\"nome\":\"SUPRIME TÉXTIL E LTDA\"}]}";
	}

	private String jsonVazio() {
		return "{\"codigo\":204,\"status\":\"sem resposta\"}";
	}
}