-- Remover esse arquivo quando iniciar o desenvolvimento.
-- Arquivo utilizado para popular o banco de dados em mem�ria na fase de demonstra��o do projeto.
--CREATE SCHEMA IF NOT EXISTS SETA;
--CREATE SCHEMA IF NOT EXISTS AUDITORIA_ENVERS;
SET SCHEMA SETA;

insert into seta.CRT (ID, VERSION,  COD_CRT, DSC_CRT) values (1, 1, 1, 'SIMPLES NACIONAL')
insert into seta.CRT (ID, VERSION,  COD_CRT, DSC_CRT) values (2, 1, 2, 'SIMPLES NACIONAL - EXCESSO DE SUBLIMITE DA RECEITA BRUTA')
insert into seta.CRT (ID, VERSION,  COD_CRT, DSC_CRT) values (3, 1, 3, 'REGIME NORMAL')

insert into SETA.indicador_financeiro (id, version, cod_indicador, dsc_indicador, nom_sigla_indicador, tip_indicador, tip_periodicidade) values (1, 1, 1, 'BTN FISCAL', 'BTNF', 'M', 'D')
insert into SETA.indicador_financeiro (id, version, cod_indicador, dsc_indicador, nom_sigla_indicador, tip_indicador, tip_periodicidade) values (2, 1, 2, 'TAXA REFERENCIA DIARIA', 'TRD', 'I', 'D')
insert into SETA.indicador_financeiro (id, version, cod_indicador, dsc_indicador, nom_sigla_indicador, tip_indicador, tip_periodicidade) values (3, 1, 3, 'UNIDADE FISCAL DO ESTADO DO CEARA', 'UFECE', 'M', 'M')
insert into SETA.indicador_financeiro (id, version, cod_indicador, dsc_indicador, nom_sigla_indicador, tip_indicador, tip_periodicidade) values (4, 1, 4, 'UNIDADE FISCAL DE REFERENCIA', 'UFIR', 'M', 'D')

insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (1, 1, 1011010, 'REPRODUTOR DE RACA PURA, CAVALOS', 0, 'Cavalos', PARSEDATETIME('19-05-2015', 'dd-MM-yyyy'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (2, 1, 1011090, 'OUTROS REPRODUTORES DE RACA PURA, DAS ESPECIES ASININA', 0, 'Outros', PARSEDATETIME('19-05-2015', 'dd-MM-yyyy'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (3, 1, 1012100, 'CAVALOS, REPRODUTORES DE RACA PURA', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (4, 1, 1012900, 'OUTROS, CAVALOS VIVOS', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (5, 1, 1013000, 'ASININOS', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (6, 1, 1019000, 'OUTROS MUARES VIVOS', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (7, 1, 1019010, 'OUTROS DA ESPECIE CAVALAR, VIVOS', 0, 'Cavalos', PARSEDATETIME('19-05-2015', 'dd-MM-yyyy'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (8, 1, 1019090, 'OUTROS ANIMAIS VIVOS, DAS ESPECIES ASININA E MUAR', 0, 'Outros', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (9, 1, 1021010, 'REPRODUTORES DE RACA PURA, PRENHE OU COM CRIA AO PE, D', 0, 'Prenhes ou com cria ao pé', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (10, 1, 1021090, 'OUTROS REPRODUTORES DE RACA PURA, DA ESPECIE BOVINA.', 0, 'Outros', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (11, 1, 1022110, 'BOVINOS DOMESTICOS, REPRODUTORES DE RACA PURA, PRENHE OU COM CRIA AO PE', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (12, 1, 1022190, 'OUTROS, BOVINOS DOMESTICOS, REPRODUTORES DE RACA PURA', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (13, 1, 1022911, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA, PARA REPRODUÇAO, PRENHE OU COM CRIA AO PE', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (14, 1, 1022919, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA, PARA REPRODUÇAO ', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (15, 1, 1022990, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA ', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (16, 1, 1023110, 'BUFALOS, REPRODUTORES DE RACA PURA, PRENHES OU COM CRIA AO PÉ', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (17, 1, 1023190, 'OUTROS, BUFALOS, REPRODUTORES DE RACA PURA', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (18, 1, 1023911, 'OUTROS BUFALOS PARA REPRODUCAO, PRENHE OU COM CRIA AO PE', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (19, 1, 1023919, 'OUTROS, BUFALOS, PARA REPRODUÇÃO', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (20, 1, 1023990, 'OUTROS, BUFALOS', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (21, 1, 1029000, 'OUTROS ANIMAIS DA ESPECIE BOVINA', 0, null, null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (22, 1, 1029011, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA, PARA REPRODUCA', 0, 'Prenhes ou com cria ao pé', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (23, 1, 1029019, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA, PARA REPRODUCA', 0, 'Outros', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (24, 1, 1029090, 'OUTROS ANIMAIS VIVOS DA ESPECIE BOVINA', 0, 'Outros', PARSEDATETIME('19/05/2015 10:05:03', 'dd/MM/yyyy hh:mm:ss'))
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (25, 1, 1031000, 'REPRODUTORES DE RACA PURA, DA ESPECIE SUINA', 0, '-Reprodutores de raça pura', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (26, 1, 1039100, 'OUTROS ANIMAIS VIVOS DA ESPECIE SUINA, DE PESO INFERIO', 0, '--De peso inferior a 50kg', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (27, 1, 1039200, 'OUTROS ANIMAIS VIVOS DA ESPECIE SUINA, DE PESO IGUAL OU', 0, '--De peso igual ou superior a 50kg', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (28, 1, 1041011, 'REPRODUTORES DE RACA PURA, PRENHE OU COM CRIA AO PE, D', 0, 'Prenhes ou com cria ao pé', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (29, 1, 1041019, 'OUTROS REPRODUTORES DE RACA PURA DA ESPECIE OVINA,', 0, 'Outros', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (30, 1, 1041090, 'OUTROS ANIMAIS VIVOS DA ESPECIE OVINA', 0, 'Outros', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (31, 1, 1042010, 'REPRODUTORES DE RACA PURA, DA ESPECIE CAPRINA', 0, 'Reprodutores de raça pura', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (32, 1, 1042090, 'OUTROS ANIMAIS VIVOS DA ESPECIE CAPRINA', 0, 'Outros', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (33, 1, 1051110, 'GALOS E GALINHAS DE LINHAS PURAS OU HIBRIDAS, PARA REPR', 0, 'De linhas puras ou h�bridas, para reprodu��o', null)
insert into seta.NCM (id, version, cod_ncm, dsc_ncm, sta_isento_icms, dsc_ncm_oficial, dat_validade_fim) values (34, 1, 1051190, 'OUTROS GALOS E GALINHAS COM PESO NAO SUPERIOR A 185G, V', 0, 'Outros', null)