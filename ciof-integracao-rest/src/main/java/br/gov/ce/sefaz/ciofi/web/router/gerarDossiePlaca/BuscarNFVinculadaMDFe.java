package br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.web.processor.BuscarContribuinteCpfProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarPlacaProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarNFeVinculadaMDFeProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;
import br.gov.ce.sefaz.ciofi.model.entity.NfUltimaAcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.UltimoRegistroAcaoFiscal;

@ContextName("ciofi-context")
public class BuscarNFVinculadaMDFe extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:BuscarNFVinculadaMDFe").routeId("BuscarNFVinculadaMDFe")
		.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param", simple("${body[param]}")).setBody(simple(""))
		.log("########################### Request 1 ##########################")
		.toD("http4://www3.sefaz.ce.gov.br/SITRAMWS/ws/ciofsitram/verificarDadosNfUltimaAcaoFiscal/?${header.param}")
		.unmarshal().json(JsonLibrary.Gson, NfUltimaAcaoFiscal.class).setProperty("nfUltimaAcaoFiscal", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarNFeVinculadaMDFeProcessor())
		.marshal()
		.json(JsonLibrary.Gson).log("${body}")
		.to("mock:result");
//		.setProperty("ultimoRegistroAcaoFiscal", simple("${body}")).setBody(simple("")).removeHeaders("CamelHttp*")
//		.log("########################### Request 2 ##########################")
//		.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).toD("http4://www3.sefaz.ce.gov.br/SITRAMWS/ws/ciofsitram/verificarDadosNfUltimaAcaoFiscal?${header.param}")
//		.unmarshal().json(JsonLibrary.Gson, NfUltimaAcaoFiscal.class)
//		.setProperty("nfUltimaAcaoFiscal", simple("${body}"))
//		.setBody(simple("")).removeHeaders("CamelHttp*").process(new DossiePlacaProcessor())
//		.marshal().json(JsonLibrary.Gson).log("${body}").to("mock:result");
	}
}
