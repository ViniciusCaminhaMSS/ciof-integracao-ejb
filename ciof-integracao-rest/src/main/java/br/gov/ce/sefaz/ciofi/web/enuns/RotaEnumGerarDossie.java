package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

/**
 * Classe enum responsável por registrar os nomes de cada rota
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
public enum RotaEnumGerarDossie implements Serializable {
	DADOS_DO_CONTRIBUINTE1("BuscarContribuinteCPF"),
	DADOS_DO_CONTRIBUINTE2("BuscarIdentificadorNFe"),
	DADOS_DA_EMPRESA1("BuscarNfeEntradaInterestadual"),
	DADOS_DA_EMPRESA2("RetornaNotasFiscaisSemRegistro"),
	DADOS_PLACA1("BuscarNFe24PassagemSitram"),
	DADOS_PLACA2("BuscarPlaca"),
	DADOS_PLACA3("BuscarNFVinculadaMDFe"),
	DADOS_PLACA4("BuscarNFePlacaUltimasXHoras"),
	DADOS_PLACA5("BuscarDadosEmpresaPlaca");

	private static final String DIRECT = "direct:";
	private String name;

	RotaEnumGerarDossie(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}

}
