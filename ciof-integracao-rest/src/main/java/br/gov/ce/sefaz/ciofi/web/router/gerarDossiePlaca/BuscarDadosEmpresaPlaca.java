package br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.ContribuinteSiget;
import br.gov.ce.sefaz.ciofi.model.entity.Empresa;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarDadosEmpresaPlacaProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;


@ContextName("ciofi-context")
public class BuscarDadosEmpresaPlaca extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");
		
		from("direct:BuscarDadosEmpresaPlaca").routeId("BuscarDadosEmpresaPlaca")
		.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param1", simple("${body[param1]}")).setHeader("param2", simple("${body[param2]}")).setBody(simple(""))
		.log("## Requesting http4://dese2.sefaz.ce.gov.br/SigetWebService/ciof/diligencia-central-cadine?${header.param1}&${header.param2}  ## ")
		.toD("http4://dese2.sefaz.ce.gov.br/SigetWebService/ciof/diligencia-central-cadine?${header.param1}&${header.param2}")
		.unmarshal().json(JsonLibrary.Gson, ContribuinteSiget.class).setProperty("contribuinteSigetWs1", simple("${body}"))
		.setBody(simple(""))
//		substituir parametro da consulta abaixo pelo param2
		.log("## Requesting http://dese2.sefaz.ce.gov.br/cadastro-webservice/services/ContribuinteService/consultarGeral?cnpj=7319866000109")
		.toD("http4://dese2.sefaz.ce.gov.br/cadastro-webservice/services/ContribuinteService/consultarGeral?cnpj=7319866000109")
		.unmarshal().json(JsonLibrary.Gson, Empresa.class).setProperty("empresa", simple("${body}"))
		.setBody(simple(""))
		.log("## http://dese2.sefaz.ce.gov.br/SigetWebService/ciof/diligencia-central-dossie?${header.param1}&${header.param2}  ## ")
		.toD("http4://dese2.sefaz.ce.gov.br/SigetWebService/ciof/diligencia-central-dossie?${header.param1}&${header.param2}")
		.unmarshal().json(JsonLibrary.Gson, ContribuinteSiget.class).setProperty("contribuinteSigetWs2", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarDadosEmpresaPlacaProcessor())
		.marshal().json(JsonLibrary.Gson).log("${body}")
		.to("mock:result");
	}
}


