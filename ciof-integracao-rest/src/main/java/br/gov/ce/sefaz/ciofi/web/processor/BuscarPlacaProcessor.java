package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.dossie.entity.DossiePlacaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.UltimoRegistroAcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.Veiculo;

public class BuscarPlacaProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		UltimoRegistroAcaoFiscal rota = exchange.getProperty("ultimoRegistroAcaoFiscal",UltimoRegistroAcaoFiscal.class);
//		NfUltimaAcaoFiscal rota2 = exchange.getProperty("nfUltimaAcaoFiscal",NfUltimaAcaoFiscal.class);
		DossiePlacaReturn dossiePlaca = new DossiePlacaReturn();
		Veiculo veiculo = new Veiculo();
//		NotaFiscal notaFiscalPassagemCe = new NotaFiscal();
//		List<NotaFiscalSitram> notaFiscaisSitram = new ArrayList<NotaFiscalSitram>();
//		
//		notaFiscaisSitram = rota2.getNotasFiscais();
//		
//		notaFiscalPassagemCe.setNumeroChaveAcesso(notaFiscaisSitram.get(0).getChave());
		
//		dossiePlaca.setNfsVinculadasMDFePassagemCE(notaFiscalPassagemCe);
//		dossiePlaca.setRegistroPassagemSitram(rota.getData());
		
		veiculo.setPlaca("ASD3421");
		veiculo.setUf("CE");
		veiculo.setProprietario("José Da Silva Candido");
		veiculo.setDebitoIPVA(true);
		veiculo.setTotalDebitos(3400);
		
		dossiePlaca.setVeiculo(veiculo);
		dossiePlaca.setRegistroPassagemSitram(rota.getData());
		
		exchange.getOut().setBody(dossiePlaca);
	}
}