package br.gov.ce.sefaz.ciofi.web.rest.service;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumGerarDossie;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumRealizarConsulta;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

@Path("/diligencia-central")
@RequestScoped
public class DiligenciaCentralService extends ResponseBase {

	private RotaAction routerAction;

	/**
	 * Contrutor default.
	 */
	public DiligenciaCentralService() {
		super();
	}

	@Inject
	public DiligenciaCentralService(RotaAction routerAction) {
		this.routerAction = routerAction;
	}
	
	@GET
	@Path("/buscarEndereco/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarEndereco(@PathParam("param") String param) {

		return Response
				.ok("{\"cnpj\":\"12222333000123\",\"cgf\":\"12222333000123\",\"situacaoCadastral\":\"Pendente\",\"endereco\":{\"numero\":\"12343\",\"bairro\":\"Aldeota\"},\"cnaePrincipal\":{\"descricaoCNAE\":\"produto 3234\"}}")
				.build();
	}
	
	@GET
	@Path("/buscarEmpresa/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarEmpresa(@PathParam("param") String param) {

		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumRealizarConsulta.REALIZAR_CONSULTA.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/buscarNfeOuChaveAcesso/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarContribuinteNfeOuChaveAcesso(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumRealizarConsulta.REALIZAR_CONSULTA_NFE.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossieCPF/buscarContribuinteCPF/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarContribuinte(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumGerarDossie.DADOS_DO_CONTRIBUINTE1.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossieCPF/buscarIdentificadorNFe/{param}")
	// @Produces(MediaType.APPLICATION_JSON)
	public Response buscarIdentificadorNFe(@PathParam("param") String param) {
		// Map<String, String> parametros = new HashMap<>();
		// parametros.put("param", param);
		// routerAction.executar(RotaEnumGerarDossie.DADOS_DO_CONTRIBUINTE2.getName(),
		// parametros);
		// String json = routerAction.getOutput();

		return Response
				.ok("{ \"destinatarioEmpresas\": [{ \"atividadeCNAE\": \"Fabricação de motores elétricos, peças e acessórios\", \"cgf\": \"58.171.836-1\", \"cnpj\": \"97.789.741/0001-35\", \"contador\": { \"cpf\": \"568.237.951-91\", \"nome\": \"Mauricio Bastter Hissa\", \"telefones\": [\"3012-4912\"] }, \"endereco\": { \"bairro\": \"Centro\", \"cep\": \"69271-922'\", \"complemento\": \"Apartamento 2912\", \"logradouro\": \"Avenida Santos Dumont\", \"municipio\": \"Fortaleza\", \"numero\": \"1321\", \"uf\": \"CE\" }, \"razaoSocial\": \"WEG S.A.\", \"situacaoCadastral\": \"ATIVO\", \"socios\": [{ \"nome\": \"Fabio Antonio\" }] }], \"remetenteEmpresas\": [{ \"atividadeCNAE\": \"Manutenção e reparação de máquinas, aparelhos e materiais elétricos não especificados anteriormente\", \"cgf\": \"69.401.560-2\", \"cnpj\": \"88.130.531/0001-40\", \"contador\": { \"cpf\": \"170.431.680-40\", \"nome\": \"Kauê Marcos da Mata\", \"telefones\": [\"(69) 2881-0998\"] }, \"endereco\": { \"bairro\": \"Castanheira\", \"cep\": \"76811-344\", \"complemento\": \"string\", \"logradouro\": \"Rua Abílio Nascimento\", \"municipio\": \"Porto Velho\", \"numero\": \"111\", \"uf\": \"RO\" }, \"razaoSocial\": \"Sérgio e Cauê Ferragens Ltda\", \"situacaoCadastral\": \"ATIVO\", \"socios\": [{ \"nome\": \"Lucas Lucca Benjamin Viana\" }] }], \"valorTotalDestinatario\": 3051.44, \"valorTotalRemetente\": 2958.33}",
				MediaType.TEXT_HTML).build();
	}

	@GET
	@Path("/dossiePlaca/buscarNFe24PassagemSitram/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarNFe24PassagemSitram(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumGerarDossie.DADOS_PLACA1.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossiePlaca/buscarPlaca/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarPlaca(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumGerarDossie.DADOS_PLACA2.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossiePlaca/buscarNFVinculadaMDFe/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarNFVinculadaMDFe(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumGerarDossie.DADOS_PLACA3.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossiePlaca/buscarNFePlacaUltimasXHoras/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarNFePlacaUltimasXHoras(@PathParam("param") String param) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param", param);
		routerAction.executar(RotaEnumGerarDossie.DADOS_PLACA4.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	@GET
	@Path("/dossiePlaca/buscarDadosEmpresa/{param1}/{param2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarDadosEmpresaPlaca(@PathParam("param1") String param1, @PathParam("param2") String param2) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param1", param1);
		parametros.put("param2", param2);
		routerAction.executar(RotaEnumGerarDossie.DADOS_PLACA5.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}
	
	@GET
	@Path("/dossiePlaca/pendenciaTransitoLivre/{param}")
	// @Produces(MediaType.APPLICATION_JSON)
	public Response pendenciaTransitoLivre(@PathParam("param") String param) {

		return Response
				.ok("{ \"pendenciaTransitoLivre\":[ { \"numeroAcaoFiscal\":\"78448-63/4\", \"valorTotalAcao\":\"1253,00\" }, { \"numeroAcaoFiscal\":\"94625-11/2\", \"valorTotalAcao\":\"1783,00\" }, { \"numeroAcaoFiscal\":\"16547-71/9\", \"valorTotalAcao\":\"2185,00\" } ] }",
				MediaType.TEXT_HTML).build();
	}
	
	@GET
	@Path("/dossiePlaca/tresPrincipaisContribuintesAutoInfracao/{param}")
	// @Produces(MediaType.APPLICATION_JSON)
	public Response tresPrincipaisContribuintesAutoInfracao(@PathParam("param") String param) {

		return Response
				.ok(" { \"valorTotalAutos\":\"16402,00\", \"principaisCgfs\":[ { \"cgfCnpjCpf\":\"07.135.453-6/ 50.081.644/0001-20/ 355.678.403-30\", \"razaoSocial\":\" GRENDENE S.A. \", \"numeroDiligencia\":\"571965/2018\" }, { \"cgfCnpjCpf\":\"32.562.212-3/ 52.947.082/0001-80/ 384.913.983-25\", \"razaoSocial\":\"WEG S.A.\", \"numeroDiligencia\":\"786221/2019\" }, { \"cgfCnpjCpf\":\"91.313.778-2/ 72.581.088/0001-93/ 195.305.213-40\", \"razaoSocial\":\"‎ENGIE BRASIL ENERGIA S.A.\", \"numeroDiligencia\":\"126978/2017\" } ] }",
				MediaType.TEXT_HTML).build();
	}

	@GET
	@Path("/dossieEmpresa/nfeEntradaInterestadualSemRegistroSitram/{param1}/{param2}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response nfeEntradaInterestadualSemRegistroSitram(@PathParam("param1") String param1,
			@PathParam("param2") String param2) {
		Map<String, String> parametros = new HashMap<>();
		parametros.put("param1", param1);
		parametros.put("param2", param2);
		routerAction.executar(RotaEnumGerarDossie.DADOS_DA_EMPRESA1.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}
	
	@GET
	@Path("/dossieEmpresa/historicoAutoInfracaoPorCnpj/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarAutoInfracao(@PathParam("param") String param) {

		return Response
				.ok("{\"autoInfracao\":[{\"numero\":\"6001018000194\",\"orgao\":\"SECRETÁRIA DA FAZENDA\"}, {\"numero\":\"5346464376747\",\"orgao\":\"PROCURADORIA GERAL\"}]}").build();
	}
	
	@GET
	@Path("/dossieEmpresa/transitoLivre/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarTransitoLivre(@PathParam("param") String param) {

		return Response
				.ok("{\"transitoLivre\":[{\"chave\":\"41181114104691000188550020000000011635005704\", \"numero\":\"16346423\", \"acaoFiscal\":\"787878789787\"}, {\"chave\":\"35181109619825000163550010000000061006006356\", \"numero\":\"12563465\", \"acaoFiscal\":\"78978787897\"}]}")
				.build();
	}
	
	@GET
	@Path("/dossieEmpresa/transportadorasUtilizadas/{param}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response buscarTransportadorasUtilizadas(@PathParam("param") String param) {

		return Response
				.ok("{\"transportadorasEmissor\":[{\"cnpj\":\"56709562000163\",\"nome\":\"AVINE EMPRESA DE ALIMENTOS E LTDA\"}, {\"cnpj\":\"09563615000109\",\"nome\":\"SUPRIME TÉXTIL E LTDA\"}]}")
				.build();
	}
	
}