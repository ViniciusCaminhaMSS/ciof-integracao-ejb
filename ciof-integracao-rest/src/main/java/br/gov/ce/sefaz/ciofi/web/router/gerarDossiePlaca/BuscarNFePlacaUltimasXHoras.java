package br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarNFePlacaUltimasXHorasProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;
import br.gov.ce.sefaz.ciofi.model.entity.NFePlacaUltimasHoras;


@ContextName("ciofi-context")
public class BuscarNFePlacaUltimasXHoras extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");
		
		from("direct:BuscarNFePlacaUltimasXHoras").routeId("BuscarNFePlacaUltimasXHoras")
		.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param", simple("${body[param]}")).setBody(simple(""))
		.log("########################### Request 1 ##########################")
		.toD("http4://www3.sefaz.ce.gov.br/SITRAMWS/ws/ciofsitram/listarDadosAcoesFiscaisPorDataUltimasHoras?placa=${header.param}&horas=8000")
		.unmarshal().json(JsonLibrary.Gson, NFePlacaUltimasHoras.class).setProperty("NFePlacaUltimasHoras", simple("${body}"))
		.setBody(simple(""))
		.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).log("########################### Request 2 ##########################")
		.toD("http4://dese2.sefaz.ce.gov.br/Nfecorp-WebServices/rs/consultaregistropassagem/ultimas-passagens/MMD0017")
		.unmarshal().json(JsonLibrary.Gson, String[].class).setProperty("RegistroPassagemSitram", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarNFePlacaUltimasXHorasProcessor())
		.marshal()
		.json(JsonLibrary.Gson).log("${body}")
		.to("mock:result");
	}
}


