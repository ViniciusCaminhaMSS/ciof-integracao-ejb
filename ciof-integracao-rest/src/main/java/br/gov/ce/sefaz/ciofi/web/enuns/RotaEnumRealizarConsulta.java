package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

/**
 * Classe enum responsável por registrar os nomes de cada rota
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
public enum RotaEnumRealizarConsulta implements Serializable {
	REALIZAR_CONSULTA("RealizarConsultaCnpjCgf"),
	REALIZAR_CONSULTA_NFE("RealizarConsultaNFe");

	private static final String DIRECT = "direct:";
	private String name;

	RotaEnumRealizarConsulta(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}

}
