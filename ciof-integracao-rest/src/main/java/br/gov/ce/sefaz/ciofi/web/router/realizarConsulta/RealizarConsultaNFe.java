package br.gov.ce.sefaz.ciofi.web.router.realizarConsulta;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

@ContextName("ciofi-context")
public class RealizarConsultaNFe extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:RealizarConsultaNFe").routeId("Realizar consulta Empresa por NFe ou Chave de Acesso")
				.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
				.setHeader("param", simple("${body[param]}")).setBody(simple(""))
				.toD("http4://www3.sefaz.ce.gov.br/Nfecorp-WebServices/rs/nfe/listaCnpjsPorNumeroNF/${header.param}?dataAtual=01-01-2014")
				.to("mock:result");
	}
}
