package br.gov.ce.sefaz.ciofi.web.rest.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONObject;

import com.google.gson.Gson;

import br.gov.ce.sefaz.ciofi.enums.ReportEnum;
import br.gov.ce.sefaz.ciofi.model.bean.Report;
import br.gov.ce.sefaz.ciofi.model.report.entity.DadosNotaReport;
import br.gov.ce.sefaz.ciofi.model.report.entity.DossieCpfReport;
import br.gov.ce.sefaz.ciofi.model.report.entity.DossieEmpresaReport;
import br.gov.ce.sefaz.ciofi.model.report.entity.DossiePlacaReport;
import br.gov.ce.sefaz.ciofi.model.report.entity.Servico;
import br.gov.ce.sefaz.ciofi.model.report.entity.Socios;
import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumGerarDossie;
import br.gov.ce.sefaz.ciofi.web.util.JsonMerged;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;
import net.sf.jasperreports.engine.JRException;

@Path("/diligencia-central/relatorioDossie")
public class RelatorioDiligenciaCentralService extends ResponseBase {
	
	private ResponseBuilder responseBuilder;
	private Report report = new Report();
	
	private RotaAction rotaAction;

	private String relatorioDossieNFe = "RelatorioNFe";
	private String relatorioDossiePlaca = "RelatorioDossiePlaca";
	private String relatorioDossieCpf = "RelatorioDossieCpf";
	private String relatorioDossieEmpresa = "RelatorioDossieEmpresa";
	
	public RelatorioDiligenciaCentralService() {
		super();
	}

	@Inject
		public RelatorioDiligenciaCentralService(RotaAction rotaAction) {
		this.rotaAction = rotaAction;
	}

		@GET
		@Path("/NFe/xls")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getNFeExcel() throws JRException{
			Map<String, Object> parameters = new HashMap<>();
		 
			byte[] bytes = report.reportXls(populaDadosNota(), parameters, ReportEnum.REPORT.getName() + relatorioDossieNFe);
			
			responseBuilder = Response.ok(bytes);
			responseBuilder.header(ReportEnum.CONTENT_DISPOSITION.getName(), "attachment;filename=RelatorioNFe.xls");
			return responseBuilder.build();
		}
		
		@GET
		@Path("/NFe/pdf")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getNFePdf() throws JRException{
			Map<String, Object> parameters = new HashMap<>();
		 
			byte[] bytes = report.reportPdf(populaDadosNota(), parameters, ReportEnum.REPORT.getName() + relatorioDossieNFe);
			
			responseBuilder = Response.ok(bytes);
			responseBuilder.header(ReportEnum.CONTENT_DISPOSITION.getName(), "attachment;filename=RelatorioNFe.pdf");
			return responseBuilder.build();
		}
		
		@GET
		@Path("/NFe/doc")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getNFeDoc() throws JRException{
			Map<String, Object> parameters = new HashMap<>();
		 
			byte[] bytes = report.reportDocx(populaDadosNota(), parameters, ReportEnum.REPORT.getName() + relatorioDossieNFe);
			
			responseBuilder = Response.ok(bytes);
			responseBuilder.header(ReportEnum.CONTENT_DISPOSITION.getName(), "attachment;filename=RelatorioNFe.odt");
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Placa/xls")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossiePlacaExcel(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") BigDecimal param6, @HeaderParam("param7") String param7) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("valorTotalNF", param6);
			parameters.put("login", param7);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2, param3, param4, param5 };
			DossiePlacaReport dossiePlaca = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossiePlacaReport.class);
			
			List<DossiePlacaReport> dossiesPlaca = new ArrayList<>();
			dossiesPlaca.add(dossiePlaca);
		 
			byte[] bytes = report.reportXls(dossiesPlaca, parameters, ReportEnum.REPORT.getName() + relatorioDossiePlaca);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Placa/pdf")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossiePlacaPdf(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") BigDecimal param6, @HeaderParam("param7") String param7) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("valorTotalNF", param6);
			parameters.put("login", param7);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2, param3, param4, param5 };
			DossiePlacaReport dossiePlaca = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossiePlacaReport.class);
			
			List<DossiePlacaReport> dossiesPlaca = new ArrayList<>();
			dossiesPlaca.add(dossiePlaca);
		 
			byte[] bytes = report.reportPdf(dossiesPlaca, parameters, ReportEnum.REPORT.getName() + relatorioDossiePlaca);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Placa/doc")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossiePlacaDoc(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") BigDecimal param6, @HeaderParam("param7") String param7) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("valorTotalNF", param6);
			parameters.put("login", param7);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] {  param2, param3, param4, param5 };
			DossiePlacaReport dossiePlaca = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossiePlacaReport.class);
			
			List<DossiePlacaReport> dossiesPlaca = new ArrayList<>();
			dossiesPlaca.add(dossiePlaca);
		 
			byte[] bytes = report.reportDocx(dossiesPlaca, parameters, ReportEnum.REPORT.getName() + relatorioDossiePlaca);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/CPF/xls")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieCpfExcel(@Context HttpServletResponse response, @HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") String param3) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("login", param3);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2 };
			DossieCpfReport dossieCpf = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieCpfReport.class);
			
			List<DossieCpfReport> dossiesCpf = new ArrayList<>();
			dossiesCpf.add(dossieCpf);
		 
			byte[] bytes = report.reportXls(dossiesCpf, parameters, ReportEnum.REPORT.getName() + relatorioDossieCpf);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/CPF/pdf")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieCpfPdf(@Context HttpServletResponse response, @HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") String param3) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("login", param3);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2 };
			DossieCpfReport dossieCpf = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieCpfReport.class);
			
			List<DossieCpfReport> dossiesCpf = new ArrayList<>();
			dossiesCpf.add(dossieCpf);
		 
			byte[] bytes = report.reportPdf(dossiesCpf, parameters, ReportEnum.REPORT.getName() + relatorioDossieCpf);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/CPF/doc")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieCpfDoc(@Context HttpServletResponse response, @HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") String param3) throws JRException{
			
			Map<String, Object> parameters = new HashMap<>();
			parameters.put("login", param3);
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2 };
			DossieCpfReport dossieCpf = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieCpfReport.class);
			
			List<DossieCpfReport> dossiesCpf = new ArrayList<>();
			dossiesCpf.add(dossieCpf);
		 
			byte[] bytes = report.reportDocx(dossiesCpf, parameters, ReportEnum.REPORT.getName() + relatorioDossieCpf);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Empresa/xls")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieEmpresaXls(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") String param6, @HeaderParam("param7") String param7, @HeaderParam("param8") String param8) throws JRException{
			
			Map<String, Object> parametersReport = new HashMap<>();
			parametersReport.put("login", param8);
			
			Map<String, String> parameters = new HashMap<>();
			parameters.put("param1", "ie=69980438");
			parameters.put("param2", "cnpj=2045487000901");
			rotaAction.executar(RotaEnumGerarDossie.DADOS_DA_EMPRESA2.getName(), parameters);
			String json = rotaAction.getOutput();
			JSONObject jsonConcatenate = new JSONObject(ReportEnum.JSON.getName() + json + "}"); 
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2, param3, param4, param5, jsonConcatenate };
			DossieEmpresaReport dossieEmpresa = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieEmpresaReport.class);
		 
			byte[] bytes = report.reportXls(dossiesempresa(dossieEmpresa), parametersReport, ReportEnum.REPORT.getName() + relatorioDossieEmpresa);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Empresa/pdf")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieEmpresaPdf(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") String param6, @HeaderParam("param7") String param7, @HeaderParam("param8") String param8) throws JRException{
			
			Map<String, Object> parametersReport = new HashMap<>();
			parametersReport.put("login", param8);
			
			Map<String, String> parameters = new HashMap<>();
			parameters.put("param1", "ie=69980438");
			parameters.put("param2", "cnpj=2045487000901");
			rotaAction.executar(RotaEnumGerarDossie.DADOS_DA_EMPRESA2.getName(), parameters);
			String json = rotaAction.getOutput();
			JSONObject jsonConcatenate = new JSONObject(ReportEnum.JSON.getName() + json + "}"); 
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2, param3, param4, param5, jsonConcatenate };
			DossieEmpresaReport dossieEmpresa = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieEmpresaReport.class);
		 
			byte[] bytes = report.reportPdf(dossiesempresa(dossieEmpresa), parametersReport, ReportEnum.REPORT.getName() + relatorioDossieEmpresa);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		@POST
		@Path("/Empresa/doc")
		@Produces(MediaType.APPLICATION_JSON)
		public Response getDossieEmpresaDoc(@HeaderParam("param") JSONObject param, @HeaderParam("param2") JSONObject param2, @HeaderParam("param3") JSONObject param3, @HeaderParam("param4") JSONObject param4, @HeaderParam("param5") JSONObject param5, @HeaderParam("param6") String param6, @HeaderParam("param7") String param7, @HeaderParam("param8") String param8) throws JRException{
		
			Map<String, Object> parametersReport = new HashMap<>();
			parametersReport.put("login", param8);
			
			Map<String, String> parameters = new HashMap<>();
			parameters.put("param1", "ie=69980438");
			parameters.put("param2", "cnpj=2045487000901");
			rotaAction.executar(RotaEnumGerarDossie.DADOS_DA_EMPRESA2.getName(), parameters);
			String json = rotaAction.getOutput();
			JSONObject jsonConcatenate = new JSONObject(ReportEnum.JSON.getName() + json + "}"); 
			
			Gson g = new Gson();
			JSONObject[] objs = new JSONObject[] { param, param2, param3, param4, param5, jsonConcatenate };
			DossieEmpresaReport dossieEmpresa = g.fromJson(JsonMerged.mergedJson(objs).toString(), DossieEmpresaReport.class);
			
			byte[] bytes = report.reportDocx(dossiesempresa(dossieEmpresa), parametersReport, ReportEnum.REPORT.getName() + relatorioDossieEmpresa);
			responseBuilder = Response.ok(bytes);
			
			return responseBuilder.build();
		}
		
		public List<DossieEmpresaReport> dossiesempresa(DossieEmpresaReport dossieEmpresa){
			List<String> nomesSocios = new ArrayList<>();
			
			for(Socios socio : dossieEmpresa.getSocios()){
				nomesSocios.add(socio.getNomeRazaoSocial());
				dossieEmpresa.setSocioParticipacaos(socio.getSocioParticipacaos());
			}
			
			List<DossieEmpresaReport> dossiesEmpresa = new ArrayList<>();
			dossieEmpresa.setNomeSocios(nomesSocios);
			dossiesEmpresa.add(dossieEmpresa);
			return dossiesEmpresa;
		}

		public List<DadosNotaReport> populaDadosNota(){
		List<DadosNotaReport> dadosNotas = new ArrayList<>();
		DadosNotaReport dadosNota = new DadosNotaReport();
		dadosNota.setDataRecebimento(new Date());
		dadosNota.setIdentificadorAssinatura("Identificação");
		dadosNota.setNaturezaOperacao("Transf merc ad ou rec terc.-ST / Transf.merc.adq.rec.terc.o");
		dadosNota.setRazaoSocial("TELEFONICA BRASIL S/A");
		dadosNota.setValorBaseCalc(BigDecimal.valueOf(206.00));
		dadosNota.setNumeroNota("32423523525");
		dadosNota.setEndereco("Avenida Beira Mar");
		dadosNota.setCep("60.434.377");
		dadosNota.setCnpjCpf("123.456.768-23");
		dadosNota.setMunicipio("Fortaleza");
		dadosNota.setBairro("Praia de Iracema");
		
		List<Servico> servicos = new ArrayList<>();
		Servico servico = new Servico();
		servico.setCodigo("3124125");
		servico.setDescricao("09C80faFy");
		
		Servico servico2 = new Servico();
		servico2.setCodigo("312412");
		servico2.setDescricao("09C80faF7");
		
		Servico servico3 = new Servico();
		servico3.setCodigo("3124126");
		servico3.setDescricao("09C80faF8");
		
		Servico servico4 = new Servico();
		servico4.setCodigo("3124172");
		servico4.setDescricao("09C80faF9");
		
		servicos.add(servico);
		servicos.add(servico2);
		servicos.add(servico3);
		servicos.add(servico4);
		
		dadosNota.setServicos(servicos);
		dadosNotas.add(dadosNota);
		
		return dadosNotas;
	}
}
