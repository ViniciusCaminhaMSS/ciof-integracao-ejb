package br.gov.ce.sefaz.ciofi.web.processor;

import java.text.DecimalFormat;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.NFePlacaUltimasHoras;
import br.gov.ce.sefaz.ciofi.model.entity.RegistroPlacaNFeUltimasXHorasReturn;

public class BuscarNFePlacaUltimasXHorasProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		RegistroPlacaNFeUltimasXHorasReturn registroReturn = new RegistroPlacaNFeUltimasXHorasReturn();
		NFePlacaUltimasHoras rota1 = exchange.getProperty("NFePlacaUltimasHoras",NFePlacaUltimasHoras.class);
		String[] rota2 = exchange.getProperty("RegistroPassagemSitram",String[].class);
		
		DecimalFormat df = new DecimalFormat("#,###.00");
	
		registroReturn.setQuantidadeNfs(rota1.getQuantidadeNfs());
		registroReturn.setValorNfs(df.format(Double.valueOf(rota1.getValorNfs())));
		registroReturn.setRegistroPlacaNF(rota1.getNotasFiscais());
		registroReturn.setRegistroPassagemSitram(rota2);
		
		exchange.getOut().setBody(registroReturn);
	
	}
}