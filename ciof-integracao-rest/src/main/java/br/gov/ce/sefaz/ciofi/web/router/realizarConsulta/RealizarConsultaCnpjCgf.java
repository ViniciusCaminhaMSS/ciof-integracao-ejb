package br.gov.ce.sefaz.ciofi.web.router.realizarConsulta;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.Empresa;
import br.gov.ce.sefaz.ciofi.web.processor.ConsultaContribuinteProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

@ContextName("ciofi-context")
public class RealizarConsultaCnpjCgf extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:RealizarConsultaCnpjCgf").routeId("Realizar consulta Empresa por CNPJ ou CGF")
				.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
				.setHeader("param", simple("${body[param]}")).setBody(simple(""))
				.toD("http4://dese2.sefaz.ce.gov.br/cadastro-webservice/services/ContribuinteService/consultarGeral?${header.param}")
				.unmarshal().json(JsonLibrary.Gson, Empresa.class).setProperty("empresa", simple("${body}"))
				.setBody(simple("")).removeHeaders("CamelHttp*").process(new ConsultaContribuinteProcessor()).marshal()
				.json(JsonLibrary.Gson).log("${body}")
				.to("mock:result");
	}
}
