package br.gov.ce.sefaz.ciofi.web.router.gerarDossieEmpresa;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscalSiget;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarNfeEntradaInterestadualProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;
//Referente ao CDTR.S05
//Dados do contribuinte - Tem participacao em empresas - Teve participação em empresas

@ContextName("ciofi-context")
public class BuscarNfeEntradaInterestadual  extends RouteBuilder {
	
	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");
		
		from("direct:BuscarNfeEntradaInterestadual").routeId("Busca Notas Fiscais interestaduais que não tenham passagem no SITRAM")
		.log("Consulta iniciada ").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param1", simple("${body[param1]}")).setHeader("param2", simple("${body[param2]}")).setBody(simple(""))
		.toD("http4://www3.sefaz.ce.gov.br/SigetWebService/ciof/nfe-sem-registro-sitram?${header.param1}&${header.param2}&ano=2018")
		.unmarshal().json(JsonLibrary.Gson, NotaFiscalSiget[].class).setProperty("notasFiscaisSiget", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarNfeEntradaInterestadualProcessor())
		.marshal()
		.json(JsonLibrary.Gson).log("${body}")
		.to("mock:result");
	}	
}
