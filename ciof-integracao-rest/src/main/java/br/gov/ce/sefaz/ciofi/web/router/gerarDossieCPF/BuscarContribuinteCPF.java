package br.gov.ce.sefaz.ciofi.web.router.gerarDossieCPF;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.dossie.entity.DossieCPF;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarContribuinteCpfProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;
//Referente ao CDTR.S05
//Dados do contribuinte - Tem participacao em empresas - Teve participação em empresas

@ContextName("ciofi-context")
public class BuscarContribuinteCPF  extends RouteBuilder {
	
	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");
		
		//CDTR.S05
		from("direct:BuscarContribuinteCPF").routeId("Busca dados do Contribuinte, Participação em empresas.")
		.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param", simple("${body[param]}")).setBody(simple(""))
		.toD("http4://dese2.sefaz.ce.gov.br/cadastro-webservice/services/ContribuinteService/consultarCPFSocio?${header.param}")
		.unmarshal().json(JsonLibrary.Gson, DossieCPF[].class).setProperty("dossie", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarContribuinteCpfProcessor())
		.marshal()
		.json(JsonLibrary.Gson).log("${body}")
		.to("mock:result");
	}	
}
