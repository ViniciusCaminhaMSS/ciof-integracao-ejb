package br.gov.ce.sefaz.ciofi.web.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.NfeSemRegistroSitramReturn;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscalSiget;

public class BuscarNfeEntradaInterestadualProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		
		NotaFiscalSiget[] rota = exchange.getProperty("notasFiscaisSiget",NotaFiscalSiget[].class);
		NfeSemRegistroSitramReturn retornoNfeSemRegistro = new NfeSemRegistroSitramReturn();
		List<NotaFiscalSiget> notasFiscais = new ArrayList<NotaFiscalSiget>();
		
		
		for(int i=0;i < rota.length ; i++) {
			notasFiscais.add(rota[i]);
		}
		
		retornoNfeSemRegistro.setNfsSemRegistroSitram(notasFiscais);
		
		exchange.getOut().setBody(retornoNfeSemRegistro);
	}
} 