package br.gov.ce.sefaz.ciofi.web.router.gerarDossiePlaca;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.NfUltimaAcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.UltimoRegistroAcaoFiscal;
import br.gov.ce.sefaz.ciofi.web.processor.BuscarNFe24PassagemSitramProcessor;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

@ContextName("ciofi-context")
public class BuscarNFe24PassagemSitram extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:BuscarNFe24PassagemSitram").routeId("BuscarNFe24PassagemSitram")
		.log("Consulta iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
		.setHeader("param", simple("${body[param]}")).setBody(simple(""))
		.log("########################### Request 1 ##########################")
		.toD("http4://www3.sefaz.ce.gov.br/SITRAMWS/ws/ciofsitram/verificarUltimoRegistroAcaoFiscal?${header.param}")
		.unmarshal().json(JsonLibrary.Gson, UltimoRegistroAcaoFiscal.class)
		.setProperty("ultimoRegistroAcaoFiscal", simple("${body}")).setBody(simple("")).removeHeaders("CamelHttp*")
		.log("########################### Request 2 ##########################")
		.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).toD("http4://www3.sefaz.ce.gov.br/SITRAMWS/ws/ciofsitram/verificarDadosNfUltimaAcaoFiscal?${header.param}")
		.unmarshal().json(JsonLibrary.Gson, NfUltimaAcaoFiscal.class)
		.setProperty("nfUltimaAcaoFiscal", simple("${body}"))
		.setBody(simple("")).removeHeaders("CamelHttp*").process(new BuscarNFe24PassagemSitramProcessor())
		.marshal().json(JsonLibrary.Gson).log("${body}").to("mock:result");
	}
}
