package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.Endereco;
import br.gov.ce.sefaz.ciofi.model.entity.ContadorBuscarEmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.Empresa;
import br.gov.ce.sefaz.ciofi.model.entity.MergeRota;
import br.gov.ce.sefaz.ciofi.model.entity.RootObject;
import br.gov.ce.sefaz.ciofi.model.entity.RootObjectCnpjCgf;

public class ConsultaContribuinteProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		Empresa rota = exchange.getProperty("empresa",Empresa.class);
		EmpresaReturn retornoBuscarEmpresa = new EmpresaReturn();
		ContadorBuscarEmpresaReturn retornoContadorBuscarEmpresa = new ContadorBuscarEmpresaReturn();
		Endereco endereco = new Endereco();
		
		endereco.setBairro(rota.getBairro());
		endereco.setNumero(rota.getNumeroLogradouro());
//		retornoBuscarEmpresa.setBairro(rota.getBairro());
		retornoBuscarEmpresa.setCgf(rota.getCgf());
		retornoBuscarEmpresa.setAtividadeCNAE(rota.getCnaePrincipal().getDescricaoCNAE());
		retornoBuscarEmpresa.setCnpj(rota.getCnpj());
		retornoContadorBuscarEmpresa.setCpf(rota.getContador().getCodigoCpfCnpj());
		retornoContadorBuscarEmpresa.setNome(rota.getContador().getNome());
		retornoBuscarEmpresa.setContador(retornoContadorBuscarEmpresa);
		retornoBuscarEmpresa.getContador().setNome(rota.getContador().getNome());
//		retornoBuscarEmpresa.setNumeroLogradouro(Integer.parseInt(rota.getNumeroLogradouro()));
		retornoBuscarEmpresa.setRazaoSocial(rota.getRazaoSocial());
		retornoBuscarEmpresa.setSituacaoCadastral(rota.getSituacaoCadastral());
		retornoBuscarEmpresa.setEndereco(endereco);
		
		exchange.getOut().setBody(retornoBuscarEmpresa);
	}
}