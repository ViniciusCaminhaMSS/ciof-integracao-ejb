package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.ContribuinteSiget;
import br.gov.ce.sefaz.ciofi.model.entity.DadosEmpresaDossiePlacaReturn;

public class BuscarDadosEmpresaPlacaProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		ContribuinteSiget rota1 = exchange.getProperty("contribuinteSigetWs1", ContribuinteSiget.class);
		ContribuinteSiget rota2 = exchange.getProperty("contribuinteSigetWs2", ContribuinteSiget.class);
		
		DadosEmpresaDossiePlacaReturn dadosEmpresaReturn = new DadosEmpresaDossiePlacaReturn();
		dadosEmpresaReturn.setDadosEmpresa(rota2);
		dadosEmpresaReturn.setRegimeEspecial(rota1.getRegimeEspecial());

		exchange.getOut().setBody(dadosEmpresaReturn);
	
	}
}