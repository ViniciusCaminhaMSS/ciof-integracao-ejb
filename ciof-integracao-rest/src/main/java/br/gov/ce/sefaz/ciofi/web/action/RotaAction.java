package br.gov.ce.sefaz.ciofi.web.action;

import java.util.Map;

import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.cdi.ContextName;

import br.gov.ce.sefaz.ciofi.web.util.qualifier.Rota;

/**
 * Classe responsável em fornecer as rotas de integração.
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 */
public class RotaAction {

	@ContextName("ciofi-context")
	private CamelContext camelContext;
	private ProducerTemplate producer;
	private String output;

	/**
	 * Construcor default
	 */
	public RotaAction() {
	}

	/**
	 * Construtor com Injeção de Dependências (DI).
	 *
	 * @param CamelContext
	 */
	@Inject
	public RotaAction(@Rota CamelContext camelContext) {
		this.camelContext = camelContext;
	}

	/**
	 * Método responsávem em executar a rota.
	 *
	 * @param nomeRota
	 * @param parametro
	 */
	public void executar(String nomeRota, String parametro) {
		producer = camelContext.createProducerTemplate();
		output = producer.requestBody(nomeRota, parametro, String.class);
	}

	/**
	 * Método responsávem em executar a rota.
	 *
	 * @param nomeRota
	 * @param mapParametros
	 */
	public void executar(String nomeRota, Map<String, String> mapParametros) {
		producer = camelContext.createProducerTemplate();
		output = producer.requestBody(nomeRota, mapParametros, String.class);
	}

	/**
	 * Retorna a resposta apôs executar a rota.
	 *
	 * @return String
	 */
	public String getOutput() {
		return output;
	}
}
