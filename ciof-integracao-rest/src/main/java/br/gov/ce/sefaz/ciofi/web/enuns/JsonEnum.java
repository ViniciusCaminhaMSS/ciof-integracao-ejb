package br.gov.ce.sefaz.ciofi.web.enuns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.camel.Exchange;

import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;
import net.sf.json.JSONObject;

public enum JsonEnum {

	JSON_CONEXAO_INVALIDA_SERVIDOR_AMQ(1), JSON_ACESSO_NAO_AUTORIZADO(2), JSON_ERRO_AO_EXECUTAR_UMA_ROTA(3);

	private JSONObject json;
	private int ordinal;

	JsonEnum(int ordinal) {
		this.ordinal = ordinal;
	}

	public String getJson(Object... parametros) {

		switch (ordinal) {
		case 1:
			getConexaoInvalidaServidorAMQ();
			break;
		case 2:
			getAcessoNaoAutorizado(parametros);
			break;
		case 3:
			getErroAoExecutarUmaRota(parametros);
			break;
		default:
			break;
		}

		return json.toString();
	}

	/**
	 * Cria um json informado um erro de conexao com o servidor AMQ.
	 */
	private void getConexaoInvalidaServidorAMQ() {
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO),
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(new Date()));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ERRO),
				MensagemResource.getMensagensProperties(MensagemResource.CONEXAO_INVALIDA_SEVIDOR_AMQ));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM),
				MensagemResource.getMensagensProperties(MensagemResource.CONEXAO_SERVIDOR_AMQ_ERRO));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}

	/**
	 * Cria um json informando que o acesso não tem autorização.
	 *
	 * @param parametros
	 */
	private void getAcessoNaoAutorizado(Object... parametros) {
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO), HttpServletResponse.SC_UNAUTHORIZED);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(new Date()));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ERRO), parametros[0]);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM), parametros[1]);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}

	/**
	 * Cria um json informando o erro ao executar uma determinada rota.
	 *
	 * @param parametros
	 */
	private void getErroAoExecutarUmaRota(Object... parametros) {
		Map<String, Object> msg = ((Exchange) parametros[0]).getProperties();
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO),
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.END_POINT),
				msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_TO_END_POINT)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(
						msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_CREATED_TIMESTAMP))));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ROOT_CASE),
				msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_EXCEPTION_CAUGHT)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM),
				MensagemResource.getMensagensProperties(MensagemResource.NAO_FOI_POSSSIVEL_EXECUTAR_ROTA) + " "
						+ msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_FAILURE_ROUTE_ID)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}
}
