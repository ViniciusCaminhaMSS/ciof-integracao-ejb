package br.gov.ce.sefaz.ciofi.web.context;

import javax.enterprise.context.ApplicationScoped;

import org.apache.camel.cdi.ContextName;
import org.apache.camel.impl.DefaultCamelContext;

import br.gov.ce.sefaz.ciofi.web.util.qualifier.Rota;

@Rota
@ApplicationScoped
@ContextName("ciofi-context")
public class RotaCamelContext extends DefaultCamelContext {

}
