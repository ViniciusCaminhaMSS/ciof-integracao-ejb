package br.gov.ce.sefaz.ciofi.web.util;

import java.util.Locale;
import java.util.ResourceBundle;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

@Startup
@Singleton
@ApplicationScoped
public class MensagemResource {

	// mensagens_pt_BR.properties
	public static final String DATA = "data";
	public static final String ERRO = "erro";
	public static final String STATUS = "status";
	public static final String CODIGO = "codigo";
	public static final String MENSAGEM = "mensagem";
	public static final String ROOT_CASE = "root.case";
	public static final String END_POINT = "end.point";
	public static final String CONTENT_TYPE = "content.type";
	public static final String SEM_RESPOSTA = "sem.resposta";
	public static final String CHARSET_UTF_8 = "charset.utf.8";
	public static final String INVALIDO_TEMPO = "invalido.tempo";
	public static final String SENHA_INVALIDA = "senha.invalida";
	public static final String INVALIDO_TOKENL = "invalido.tokenl";
	public static final String RESOURCE_BUNDLE = "resource.bundle";
	public static final String APPLICATION_JSON = "application.json";
	public static final String CAMEL_TO_END_POINT = "camel.to.end.point";
	public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
	public static final String CAMEL_EXCEPTION_CAUGHT = "camel.exception.caught";
	public static final String CAMEL_FAILURE_ROUTE_ID = "camel.failure.route.id";
	public static final String CAMEL_CREATED_TIMESTAMP = "camel.created.timestamp";
	public static final String CONEXAO_INVALIDA_SEVIDOR_AMQ = "conexao.invalida.sevidor.amq";
	public static final String NAO_FOI_POSSSIVEL_EXECUTAR_ROTA = "nao.foi.posssivel.executar.rota";
	public static final String CONEXAO_SERVIDOR_AMQ_ERRO = "conexao.servidor.amq.erro";
	public static final String AUTORIZACAO_CLIENTE_INVALIDO = "autorizacao.cliente.invalido";
	public static final String INVALIDO_CLIENTE_SENHA = "invalido.cliente.senha";
	public static final String INVALIDO_CLIENTE_CREDENCIAL = "invalido.cliente.credencial";

	// aplicacao.properties
	public static final String USUARIO_AMQ = "user.amq";
	public static final String SENHA_AMQ = "pwd.amq";
	public static final String HOST_AMQ = "host.amq";
	public static final String SARVER_ERRO_AMQ = "server.erro.amq";

	public static final String URL_BRMS = "url.brms";
	public static final String URL_PARAMETROS_BRMS = "url.parametros.brms";

	private static final String PT_BR = "pt_BR";
	private static final String PORTAIS = "portais";
	private static final String APLICACAO = "application";
	private static final String MENSAGENS_PT_BR = "mensagens_pt_BR";

	private static ResourceBundle mensagensProperties = ResourceBundle.getBundle(MENSAGENS_PT_BR, new Locale(PT_BR));
	private static ResourceBundle aplicacaoProperties = ResourceBundle.getBundle(APLICACAO, new Locale(PT_BR));
	private static ResourceBundle protaisProperties = ResourceBundle.getBundle(PORTAIS, new Locale(PT_BR));

	public static String getMensagensProperties(String bundle) {
		return mensagensProperties.getString(bundle);
	}

	public static String getAplicacaoProperties(String bundle) {
		return aplicacaoProperties.getString(bundle);
	}

	public static String getProtaisProperties(String bundle) {
		return protaisProperties.getString(bundle);
	}

	public static ResourceBundle getProtaisProperties() {
		return protaisProperties;
	}

}
