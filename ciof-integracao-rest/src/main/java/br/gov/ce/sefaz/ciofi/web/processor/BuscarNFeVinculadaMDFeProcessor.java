package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.NfUltimaAcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.NfVinculadaMDFeReturn;

public class BuscarNFeVinculadaMDFeProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		NfUltimaAcaoFiscal rota = exchange.getProperty("nfUltimaAcaoFiscal",NfUltimaAcaoFiscal.class);
		NfVinculadaMDFeReturn nfVinculadaMDFe = new NfVinculadaMDFeReturn();
		
		nfVinculadaMDFe.setQuantidadeNfs(rota.getQuantidadeNfs().toString());
		nfVinculadaMDFe.setNfeVinculadasMDFe(rota.getNotasFiscais());
		
		exchange.getOut().setBody(nfVinculadaMDFe);
	
	}
}