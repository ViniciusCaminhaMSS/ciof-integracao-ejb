package br.gov.ce.sefaz.ciofi.web.rest.service;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.model.bean.MunicipioBO;
import br.gov.ce.sefaz.ciofi.model.entity.MunicipioSeta;

@Path("/municipios")
@RequestScoped
public class MunicipioSetaService {
	
	@Inject
	private MunicipioBO municipioBO;
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response findAll(){
		try {
			List<MunicipioSeta> municipios = municipioBO.findAll();
			return Response.ok(municipios).build();
		} catch (Exception e) {
			return Response.status(Response.Status.BAD_REQUEST).entity("error: " +  e.getMessage()).build();
		}
	}
	
}
