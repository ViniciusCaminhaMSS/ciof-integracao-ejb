package br.gov.ce.sefaz.ciofi.web.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.dossie.entity.DossieCPF;
import br.gov.ce.sefaz.ciofi.model.dossie.entity.DossieCPFWs1;
import br.gov.ce.sefaz.ciofi.model.entity.Contribuinte;
import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.Endereco;

public class BuscarContribuinteCpfProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		DossieCPF[] rota = exchange.getProperty("dossie",DossieCPF[].class);
		DossieCPFWs1 retornoDossieCpf = new DossieCPFWs1();
		Contribuinte contribuinte = new Contribuinte();
		List<EmpresaReturn> participacaoEmpresas = new ArrayList<EmpresaReturn>();
		List<EmpresaReturn> participouEmpresas = new ArrayList<EmpresaReturn>();
		
		Endereco endereco = new Endereco();
		
		contribuinte.setCpf(Long.parseLong(rota[0].getCnpj().replaceAll("[^0-9]*", "")));
			endereco.setBairro(rota[0].getBairro());
			endereco.setCep(Integer.parseInt(rota[0].getCep()));
			endereco.setComplemento(rota[0].getComplemento());
			endereco.setLogradouro(rota[0].getLogradouro());
			endereco.setMunicipio(rota[0].getMunicipio().getNome());
			endereco.setNumero(rota[0].getNumeroLogradouro());
			endereco.setUf(rota[0].getUf());
		contribuinte.setEndereco(endereco);
		contribuinte.setNome(rota[0].getRazaoSocial());
		
		
		
		
		for (int i=1; i<rota.length ;i++ ){
			EmpresaReturn empresaParticipacao = new EmpresaReturn();
			empresaParticipacao.setCnpj(rota[i].getCnpj());
			empresaParticipacao.setCgf(rota[i].getCgf());
			empresaParticipacao.setRazaoSocial(rota[i].getRazaoSocial());
			empresaParticipacao.setAtividadeCNAE(rota[i].getCnaePrimario().getDescricaoCNAE());
			empresaParticipacao.setSituacaoCadastral(rota[i].getDescricaoSituacao());
			participacaoEmpresas.add(empresaParticipacao);
		}

		
		
		retornoDossieCpf.setContribuinte(contribuinte);
		retornoDossieCpf.setParticipacaoEmpresas(participacaoEmpresas);
		retornoDossieCpf.setParticipouEmpresas(participouEmpresas);
		
		exchange.getOut().setBody(retornoDossieCpf);
	}
}