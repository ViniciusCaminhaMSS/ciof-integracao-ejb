package br.gov.ce.sefaz.ciofi.web.util;

import java.util.Iterator;

import org.json.JSONObject;

public class JsonMerged {
	
	private JsonMerged() {
		super();
	}

	public static JSONObject mergedJson(JSONObject[] objs){
		JSONObject merged = new JSONObject();
		for (JSONObject obj : objs) {
		    Iterator<?> it = obj.keys();
		    while (it.hasNext()) {
		        String key = (String)it.next();
		        merged.put(key, obj.get(key));
		    }
		}
		return merged;
	}
}
