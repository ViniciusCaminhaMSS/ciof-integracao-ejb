package br.gov.ce.sefaz.ciofi.web.rest.service;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Classe utilizada para indicar que o projeto vai ter suporte a serviços rest.
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {

}
