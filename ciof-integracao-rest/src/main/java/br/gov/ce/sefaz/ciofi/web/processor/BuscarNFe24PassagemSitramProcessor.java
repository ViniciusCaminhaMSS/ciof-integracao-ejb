package br.gov.ce.sefaz.ciofi.web.processor;

import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.dossie.entity.DossiePlacaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.AutoInfracao;
import br.gov.ce.sefaz.ciofi.model.entity.ContadorBuscarEmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.Diligencia;
import br.gov.ce.sefaz.ciofi.model.entity.EmpresaReturn;
import br.gov.ce.sefaz.ciofi.model.entity.Endereco;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.NotaFiscalSitram;
import br.gov.ce.sefaz.ciofi.model.entity.SociosEmpresa;
import br.gov.ce.sefaz.ciofi.model.entity.TransitoLivre;
import br.gov.ce.sefaz.ciofi.model.entity.UltimoRegistroAcaoFiscal;
import br.gov.ce.sefaz.ciofi.model.entity.Veiculo;

public class BuscarNFe24PassagemSitramProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		UltimoRegistroAcaoFiscal rota = exchange.getProperty("ultimoRegistroAcaoFiscal",UltimoRegistroAcaoFiscal.class);
		DossiePlacaReturn dossiePlaca = new DossiePlacaReturn();
		NotaFiscal notaFiscalPassagemCe = new NotaFiscal();
		List<NotaFiscal> listanotaFiscalPassagemCe = new ArrayList<NotaFiscal>();
		NotaFiscalSitram nfsVinculadaMDFe = new NotaFiscalSitram();
		List<NotaFiscalSitram> listaNfsVinculadaMDFe = new ArrayList<NotaFiscalSitram>();
		
		
		
//		notaFiscalPassagemCe.setNumeroChaveAcesso(notaFiscaisSitram.get(0).getChave());
		listanotaFiscalPassagemCe.add(notaFiscalPassagemCe);
		dossiePlaca.setNfsVinculadasMDFePassagemCE(listanotaFiscalPassagemCe);
		dossiePlaca.setRegistroPassagemSitram(rota.getData());
		
		//DadosMockados
		
		List<AutoInfracao> listaAutoInfracao = new ArrayList<AutoInfracao>();
		AutoInfracao autoInfracao = new AutoInfracao();
		Diligencia diligencia = new Diligencia();
		EmpresaReturn empresa = new EmpresaReturn();
		ContadorBuscarEmpresaReturn contador = new ContadorBuscarEmpresaReturn();
		List<String> telefones = new ArrayList<String>();
		Endereco endereco = new Endereco();
		SociosEmpresa socios = new SociosEmpresa();
		List<SociosEmpresa> listaSocios = new ArrayList<SociosEmpresa>();
		
		diligencia.setNumero(123123);
		autoInfracao.setDiligencia(diligencia);
		
		empresa.setAtividadeCNAE("Fabricação de pilhas, baterias e acumuladores elétricos, exceto para veículos automotores");
		empresa.setCgf("35962027-2");
		empresa.setCnpj("04569799000192");
		contador.setCpf("266.231.953-43");
		contador.setNome("Ruan Alberto Gomes");
		telefones.add("3021-1234");
		contador.setTelefones(telefones);
		endereco.setBairro("Centro");
		endereco.setCep(12312312);
		endereco.setComplemento("Apartamento 2013");
		endereco.setLogradouro("Avenida Barbosa de Sousa");
		endereco.setMunicipio("Iguatu");
		endereco.setNumero("1234");
		endereco.setUf("CE");
		empresa.setEndereco(endereco);
		empresa.setRazaoSocial("LOJAS RENNER S.A.");
		empresa.setSituacaoCadastral("Ativo");
		socios.setNome("Francisco Gomes de Matos");
		listaSocios.add(socios);
		empresa.setSocios(listaSocios);
		
		empresa.setContador(contador);
		
		autoInfracao.setEmpresa(empresa);
		autoInfracao.setNomeOrgaoAtuante("Célula de Informações e Operações Fiscais");
		autoInfracao.setNumero("4401250");
		autoInfracao.setDiligencia(diligencia);
		
		listaAutoInfracao.add(autoInfracao);
		dossiePlaca.setEmpresasVinculadasAutoInfracao(listaAutoInfracao);
		
		nfsVinculadaMDFe.setChave("16570352914227882359696587548219107568266148");
		nfsVinculadaMDFe.setDataEmissao("07/11/2014");
		nfsVinculadaMDFe.setNumero("65991478");
		nfsVinculadaMDFe.setSituacaoDebito("Pendente");
		nfsVinculadaMDFe.setValor("12.486,90");
		
		listaNfsVinculadaMDFe.add(nfsVinculadaMDFe);
		
		dossiePlaca.setNfsVinculadasMDFe(listaNfsVinculadaMDFe);
		
		TransitoLivre pendenciaTransitoLivre = new TransitoLivre();
		List<TransitoLivre> listaPendenciaTransitoLivre = new ArrayList<TransitoLivre>();
		
		pendenciaTransitoLivre.setNumeroAcaoFiscal(12312);
		pendenciaTransitoLivre.setNumeroNFeChaveAcesso(213123123);
		
		listaPendenciaTransitoLivre.add(pendenciaTransitoLivre);
		
		dossiePlaca.setPendenciasTransitoLivre(listaPendenciaTransitoLivre);
		
		NotaFiscal registroPlacaNf = new NotaFiscal();
		List<NotaFiscal> listaRegistroPlacaNf = new ArrayList<NotaFiscal>();
		
		registroPlacaNf.setDestinatario("Lojas Pague Menos S.A");
		registroPlacaNf.setEmitente("Alberto Freitas Souza");
		registroPlacaNf.setNumeroChaveAcesso("16570352914227882359696587548219107568266148");
		registroPlacaNf.setUfDestinatario("CE");
		registroPlacaNf.setUfEmitente("SC");
		
		listaRegistroPlacaNf.add(registroPlacaNf);
		
		dossiePlaca.setRegistroPlacaNF(listaRegistroPlacaNf);
		
		Veiculo veiculo = new Veiculo();
		veiculo.setDebitoIPVA(true);
		veiculo.setPlaca("AOP-1251");
		veiculo.setProprietario("Clovis Bezerra Oliveira");
		veiculo.setRoubado(false);
		veiculo.setTotalDebitos(1200.00);
		
		dossiePlaca.setVeiculo(veiculo);
		
		
		exchange.getOut().setBody(dossiePlaca);
	}
}