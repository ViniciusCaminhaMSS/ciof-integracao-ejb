package br.gov.ce.sefaz.ciofi.web.util;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Strings;

import net.sf.json.JSONObject;

/**
 * Classe responsável em fonecer métodos que trata as respostas das classes de
 * serviços.
 *
 * @author carlos.lima
 *
 */
public abstract class ResponseBase {

	private Logger log = LoggerFactory.getLogger(ResponseBase.class);

	/**
	 * Método que trata a resposta retornada pelo Camel e retorna um objeto
	 * Response.
	 *
	 * @param json
	 * @return Response
	 */
	public Response createResponse(String json) {
		if (!Strings.isNullOrEmpty(json)) {
			return json.contains(MensagemResource.getMensagensProperties(MensagemResource.ERRO)) != Boolean.TRUE
					? getResponseOk(json) : getResponseError(json);
		} else {
			return getJsonVazio();
		}
	}

	private Response getResponseError(String json) {
		log.error("Error ao executar a rota: " + json);
		return Response.status(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode()).entity(json)
				.type(MediaType.APPLICATION_JSON)
				.header(MensagemResource.getMensagensProperties(MensagemResource.CONTENT_TYPE),
						MensagemResource.getMensagensProperties(MensagemResource.CHARSET_UTF_8))
				.build();
	}

	private Response getResponseOk(String json) {
		return Response.ok(json, MediaType.APPLICATION_JSON)
				.header(MensagemResource.getMensagensProperties(MensagemResource.CONTENT_TYPE),
						MensagemResource.getMensagensProperties(MensagemResource.CHARSET_UTF_8))
				.build();
	}

	/**
	 * Retorna um json do tipo vazio.
	 *
	 * @param msgErro
	 * @return
	 */
	public Response getJsonVazio() {
		JSONObject json = new JSONObject();
		json.put(MensagemResource.CODIGO, HttpServletResponse.SC_NO_CONTENT);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.SEM_RESPOSTA));
		return Response.status(Response.Status.NO_CONTENT).entity(json.toString()).build();
	}
}
