package br.gov.ce.sefaz.ciofi.web.security;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;

import br.gov.ce.sefaz.ciofi.model.entity.Aplicacao;
import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;

/**
 * Classe que guarda os tokes válidos gerados pela classe {@link TokenEndpoint}
 * e as aplicações importadas do arquivo resourcebundle aplicacoes.properties
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 */
@Startup
@Singleton
@DependsOn("MensagemResource")
@ApplicationScoped
public class RepositorioAplicacoesOAuthCIOF {

	private static final String SECRET = ".secret";
	private static final String ID = ".id";
	private static final String NOME = ".nome";
	private static final String CLIENT = "client";
	private static final String APP = "app";
	private static final String MINUTO = "minuto";
	private static final String HORA = "hora";

	private Map<String, Long> tokens = new HashMap<String, Long>();
	private Map<String, Aplicacao> aplicacoes = new HashMap<String, Aplicacao>();

	public void adicionaToken(String accessToken, Long timeExpiration) {
		this.tokens.put(accessToken, timeExpiration);
	}

	public void removeTokenExpirado(String accessToken) {
		tokens.remove(accessToken);
	}

	public boolean isTokenValido(String accessToken) {
		return tokens.containsKey(accessToken);
	}

	public boolean isAplicacaoClienteValido(String clienteId) {
		return aplicacoes.containsKey(clienteId);
	}

	public boolean isAplicacaoSenhaValida(String clienteId, String senha) {
		return aplicacoes.get(clienteId).getClienteSenha().equals(senha);
	}

	public boolean isTempoValido(String accessToken) {
		return Calendar.getInstance().getTimeInMillis() < tokens.get(accessToken);
	}

	/**
	 * Carrega todas as aplicações cadastradas no resourcebundle 'portais' e
	 *
	 */
	@PostConstruct
	private void carregaPortais() {
		Set<String> keySet = MensagemResource.getProtaisProperties().keySet();
		int totalAplicacoes = keySet.size() / 3;
		for (int i = 1; i <= totalAplicacoes; i++) {
			Aplicacao portal = new Aplicacao();
			portal.setNome(MensagemResource.getProtaisProperties(APP + i + NOME));
			portal.setClienteId(MensagemResource.getProtaisProperties(CLIENT + i + ID));
			portal.setClienteSenha(MensagemResource.getProtaisProperties(CLIENT + i + SECRET));
			aplicacoes.put(portal.getClienteId(), portal);
		}
	}

	/**
	 * Gera o tempo que o token deve experiar.
	 *
	 * @return
	 */
	public Long getTempoParaExpirarToken() {
		GregorianCalendar dataAtual = new GregorianCalendar();
		dataAtual.setTime(Calendar.getInstance().getTime());
		dataAtual.add(Calendar.HOUR, Integer.parseInt(MensagemResource.getAplicacaoProperties(HORA)));
		dataAtual.add(Calendar.MINUTE, Integer.parseInt(MensagemResource.getAplicacaoProperties(MINUTO)));
		return dataAtual.getTimeInMillis();
	}
}
