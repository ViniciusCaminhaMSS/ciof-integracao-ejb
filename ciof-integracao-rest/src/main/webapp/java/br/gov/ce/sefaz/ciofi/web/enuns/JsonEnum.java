package br.gov.ce.sefaz.ciofi.web.enuns;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.camel.Exchange;
import org.json.JSONObject;

import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;

public enum JsonEnum {

	JSON_CONNECTIONFACTORY_AMQ_ERROR(1), JSON_OAUTHAUTORIZATION_ERROR(2), JSON_ROTA_CAMEL_EXCHANGE_ERROR(3);

	private JSONObject json;
	private int ordinal;

	JsonEnum(int ordinal) {
		this.ordinal = ordinal;
	}

	public String getJson(Object... parametros) {

		switch (ordinal) {
		case 1:
			getConexaoInvalidaServidorAMQ();
			break;
		case 2:
			getAcessoNaoAutorizado(parametros);
			break;
		case 3:
			getErroAoExecutarUmaRota(parametros);
			break;
		default:
			break;
		}

		return json.toString();
	}

	/**
	 * Cria um json informando o erro ao executar uma determinada rota.
	 *
	 * @param parametros
	 */
	private void getErroAoExecutarUmaRota(Object... parametros) {
		Map<String, Object> msg = ((Exchange) parametros[0]).getProperties();
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO),
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.END_POINT),
				msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_TO_END_POINT)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(
						msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_CREATED_TIMESTAMP))));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ROOT_CASE),
				msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_EXCEPTION_CAUGHT)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM),
				MensagemResource.getMensagensProperties(MensagemResource.NAO_FOI_POSSSIVEL_EXECUTAR_ROTA)
						+ " " + msg.get(MensagemResource.getMensagensProperties(MensagemResource.CAMEL_FAILURE_ROUTE_ID)));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}

	/**
	 * Cria um json informando que o acesso não tem autorização.
	 *
	 * @param parametros
	 */
	private void getAcessoNaoAutorizado(Object... parametros) {
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO), HttpServletResponse.SC_UNAUTHORIZED);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(new Date()));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ERRO), parametros[0]);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM), parametros[1]);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}

	/**
	 * Cria um json informado um erro de conexao com o servidor AMQ.
	 */
	private void getConexaoInvalidaServidorAMQ() {
		json = new JSONObject();
		json.put(MensagemResource.getMensagensProperties(MensagemResource.CODIGO),
				HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
		json.put(MensagemResource.getMensagensProperties(MensagemResource.DATA),
				new SimpleDateFormat(MensagemResource.DD_MM_YYYY_HH_MM_SS).format(new Date()));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.ERRO),
				MensagemResource.getMensagensProperties(MensagemResource.CONEXAO_INVALIDA_SEVIDOR_AMQ));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.MENSAGEM),
				MensagemResource.getMensagensProperties(MensagemResource.CONEXAO_INVALIDA_SEVIDOR_AMQ));
		json.put(MensagemResource.getMensagensProperties(MensagemResource.STATUS),
				MensagemResource.getMensagensProperties(MensagemResource.ERRO));
	}
}
