package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.web.enuns.JsonEnum;

/**
 * Classe responsável em processar erros decorrente de uma rota. Quando for
 * lançada uma exceção é realizado um tratamento para retornar um erro no
 * formato json.
 *
 *
 * @author carlos.lima
 *
 */
public class RotaCamelError implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		exchange.getOut().setBody(JsonEnum.JSON_ROTA_CAMEL_EXCHANGE_ERROR.getJson(exchange));
	}
}
