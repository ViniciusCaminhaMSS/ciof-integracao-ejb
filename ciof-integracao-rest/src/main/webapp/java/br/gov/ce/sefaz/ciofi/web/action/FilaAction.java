package br.gov.ce.sefaz.ciofi.web.action;

import javax.inject.Inject;

import org.apache.camel.ProducerTemplate;

import br.gov.ce.sefaz.ciofi.web.context.FilaCamelContext;

/**
 * Classe responsável em enviar uma mensagem para sua fila através de uma rota
 * do camel.
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 */
public class FilaAction {

	@Inject
	private FilaCamelContext filaCamelContext;
	private String output;
	private ProducerTemplate producer;

	public void enviar(String nomeRota, String message) throws Throwable {
		producer = filaCamelContext.getProducerTemplate();
		output = producer.requestBody(nomeRota, message, String.class);
	}

	public String getOutput() {
		return output;
	}

}
