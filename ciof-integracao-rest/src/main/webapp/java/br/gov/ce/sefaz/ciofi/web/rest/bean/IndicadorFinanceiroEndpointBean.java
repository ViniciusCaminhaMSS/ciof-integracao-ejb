package br.gov.ce.sefaz.ciofi.web.rest.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.data.domain.PageRequest;

import br.gov.ce.sefaz.ciofi.model.bean.IndicadorFinanceiroBo;
import br.gov.ce.sefaz.ciofi.model.entity.IndicadorFinanceiro;
import br.gov.ce.sefaz.ciofi.web.rest.IndicadorFinanceiroEndpoint;

@Stateless
public class IndicadorFinanceiroEndpointBean implements IndicadorFinanceiroEndpoint {

  @Inject
  private IndicadorFinanceiroBo bean;


  @Override
  public Response findAll(Integer page, Integer size) {
    try {
      return Response.ok(bean.findAll(new PageRequest(page, size))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findAll(IndicadorFinanceiro indicadorFinanceiro, Integer page, Integer size) {
    try {
      ExampleMatcher matcher =
          ExampleMatcher.matching().withIgnoreCase().withStringMatcher(StringMatcher.CONTAINING);
      return Response
          .ok(bean.findAll(Example.of(indicadorFinanceiro, matcher), new PageRequest(page, size)))
          .build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findOne(Long id) {
    try {
      return Response.ok(bean.findOne(id)).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response delete(Long id) {
    try {
      bean.delete(id);
      return Response.ok().build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response save(IndicadorFinanceiro indicadorFinanceiro) {
    try {
      bean.save(indicadorFinanceiro);
      return Response.created(null).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }

  }

  @Override
  public Response save(IndicadorFinanceiro indicadorFinanceiro, int id) {
    try {
      indicadorFinanceiro.setCodigo(id);
      bean.save(indicadorFinanceiro);
      return Response.ok(indicadorFinanceiro).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }

  }

  private Map<String, String> error(Exception e) {
    Map<String, String> responseObj = new HashMap<>();
    responseObj.put("error", e.getMessage());
    return responseObj;
  }
}
