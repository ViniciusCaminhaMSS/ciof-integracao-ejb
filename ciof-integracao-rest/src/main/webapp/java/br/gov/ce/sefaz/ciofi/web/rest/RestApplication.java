package br.gov.ce.sefaz.ciofi.web.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Classe utilizada para indicar que o projeto vai ter suporte a serviço rest.
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {

}
