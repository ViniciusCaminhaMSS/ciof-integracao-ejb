package br.gov.ce.sefaz.ciofi.web.router.exemplo;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.MergeRota;
import br.gov.ce.sefaz.ciofi.model.entity.RootObject;
import br.gov.ce.sefaz.ciofi.web.processor.MergeTresEndPoint;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

/**
 * Rota que executa 3 endpoints agregando o resultado em um unico json de
 * resposta.
 *
 * @author carlos.lima
 *
 */
@ContextName("ciofi-context")
public class ExemploComTresEndPointRota extends RouteBuilder {

	// private static final String URI_JSON_USA =
	// "http4://services.groupkt.com/country/get/iso3code/USA";
	private static final String URI_JSON_USA = "http4://localhost:8081/restciof/usa/";
	// private static final String URI_JSON_IND =
	// "http4://services.groupkt.com/state/get/IND/UP/";
	private static final String URI_JSON_IND = "http4://localhost:8081/restciof/country/";
	// private static final String URI_XML_CEP =
	// "http4://viacep.com.br/ws/01001000/xml/";
	private static final String URI_XML_CEP = "http4://localhost:8081/restciof/cep/";
	private static final String ROUTER3 = "direct:ExemploComTresEndPointRota";

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from(ROUTER3).routeId("Rota3").setProperty("name", simple("${body[name]}"))
				.setProperty("key", simple("${body[key]}")).setProperty("pwd", simple("${body[pwd]}"))
				.setBody(simple("")).log("########################### Request 1 ##########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).to(URI_XML_CEP)
				// .convertBodyTo(String.class, "ISO-8859-1")
				.marshal().xmljson().unmarshal().json(JsonLibrary.Gson, MergeRota.class)
				.setProperty("uriCEP", simple("${body}")).setBody(simple("")).removeHeaders("CamelHttp*")
				.log("########################### Request 2 ##########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).to(URI_JSON_IND)
				// .convertBodyTo(String.class, "ISO-8859-1")
				.unmarshal().json(JsonLibrary.Gson, RootObject.class).setProperty("uriIND", simple("${body}"))
				.setBody(simple("")).removeHeaders("CamelHttp*")
				.log("########################### Request 3 ##########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).to(URI_JSON_USA)
				// .convertBodyTo(String.class, "ISO-8859-1")
				.unmarshal().json(JsonLibrary.Gson, RootObject.class).setProperty("uriUSA", simple("${body}"))
				.setBody(simple("")).removeHeaders("CamelHttp*").process(new MergeTresEndPoint()).marshal()
				.json(JsonLibrary.Gson).log("${body}").to("mock:result");

	}

}
