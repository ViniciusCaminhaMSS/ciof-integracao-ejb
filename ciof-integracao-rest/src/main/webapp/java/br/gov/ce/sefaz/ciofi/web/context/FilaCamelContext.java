package br.gov.ce.sefaz.ciofi.web.context;

import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;

import br.gov.ce.sefaz.ciofi.web.helper.ConnectionFactoryResourceHelper;
import br.gov.ce.sefaz.ciofi.web.router.fila.bpms.EnviaBPMS;
import br.gov.ce.sefaz.ciofi.web.router.fila.bpms.RecebeBPMS;
import br.gov.ce.sefaz.ciofi.web.router.fila.fiscal.EnviaFilaFiscal;
import br.gov.ce.sefaz.ciofi.web.router.fila.fiscal.RecebeFilaFiscal;

@Startup
@Singleton
@ApplicationScoped
public class FilaCamelContext {

	@Inject
	private ConnectionFactoryResourceHelper connectionFactoryResourceHelper;
	private CamelContext camelContext;
	private ProducerTemplate producerTemplate;

	public void createCamelContext() throws Throwable {
		camelContext = connectionFactoryResourceHelper.getCameleContext();
		adicionaFilasToCamelcontext();
		camelContext.start();
		producerTemplate = camelContext.createProducerTemplate();
	}

	public ProducerTemplate getProducerTemplate() {
		return producerTemplate;
	}

	/**
	 * Adiciona as rotas de intragração com o servidor de filas.
	 *
	 * @throws Throwable
	 */
	private void adicionaFilasToCamelcontext() throws Throwable {
		camelContext.addRoutes(new EnviaFilaFiscal());
		camelContext.addRoutes(new RecebeFilaFiscal());
		camelContext.addRoutes(new RecebeBPMS());
		camelContext.addRoutes(new EnviaBPMS());
	}

}
