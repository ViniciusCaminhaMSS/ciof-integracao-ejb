package br.gov.ce.sefaz.ciofi.web.rest.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.web.action.FilaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.FilaEnum;
import br.gov.ce.sefaz.ciofi.web.interceptor.ConnectionFactoryResourceInterceptor;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

@Path("/fila")
@RequestScoped
@Interceptors({ ConnectionFactoryResourceInterceptor.class })
public class FilaService extends ResponseBase {

	private FilaAction filaAction;

	/**
	 * Contrutor default.
	 */
	public FilaService() {

	}

	/**
	 * Construtor com Injeção de Dependências (DI).
	 *
	 * @param FilaAction
	 */
	@Inject
	public FilaService(FilaAction filaAction) {
		this.filaAction = filaAction;
	}

	@POST
	@Path("/alerta/fiscal")
	@Consumes("Application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response enviarParaFilaPainelFiscal(@Context HttpServletRequest request, String json) throws Throwable {
		filaAction.enviar(FilaEnum.ENVIAR_ALERTA_FISCAL.getName(), json);
		String jsonResponse = filaAction.getOutput();
		return createResponse(jsonResponse);
	}

	@POST
	@Path("/alerta/bpms")
	@Consumes("Application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response enviarAlertaParaBPMS(@Context HttpServletRequest request, String json) throws Throwable {
		filaAction.enviar(FilaEnum.ENVIAR_ALERTA_BPMS.getName(), json);
		String jsonResponse = filaAction.getOutput();
		return createResponse(jsonResponse);
	}

}
