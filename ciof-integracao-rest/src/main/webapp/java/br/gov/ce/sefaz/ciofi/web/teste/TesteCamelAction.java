package br.gov.ce.sefaz.ciofi.web.teste;

import org.apache.camel.CamelContext;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.impl.DefaultCamelContext;

public class TesteCamelAction {

	private static final String TARGET = "http4://localhost:8080/kie-server/services/rest/server/containers/instances/myContainer?authMethod=Basic"
			+ "&authUsername=kieserver&authPassword=kieserver1!";

	public static void main(String[] args) {
		CamelContext context = new DefaultCamelContext();

		try {
			context.addRoutes(new RouteBuilder() {
				@Override
				public void configure() throws Exception {
					onException(Exception.class).to("log:GeneralError?level=ERROR").end();

					from("direct:start").setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
							.setHeader(Exchange.CONTENT_TYPE, constant("application/json")).log("${body}").to(TARGET)
							.process(new Processor() {
								@Override
								public void process(Exchange exchange) throws Exception {
									System.out.println(exchange.getIn().getBody(String.class));
								}
							}).log("${body}").end();
				}
			});

			ProducerTemplate templete = context.createProducerTemplate();
			context.start();
			templete.sendBody("direct:start",
					"{\"lookup\": \"mySession\",\"commands\":[{\"insert\":"
							+ "{\"object\":{\"org.brms.myproject.Person\":{\"firstName\":\"john\", \"lastName\":"
							+ "\"Carther\",\"wage\":9,\"hourlyRate\":7}},\"disconnected\":false,"
							+ "\"out-identifier\":\"person\",\"return-object\":true,\"entry-point\":"
							+ "\"DEFAULT\"}},{\"fire-all-rules\":\"\"}]}");
			System.out.println("Start Apache ");

			Thread.sleep(3 * 1000);
			context.stop();
			System.out.println("Stop Apache ");

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

}