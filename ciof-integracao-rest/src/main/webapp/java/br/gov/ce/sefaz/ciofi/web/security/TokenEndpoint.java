package br.gov.ce.sefaz.ciofi.web.security;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;
import org.apache.oltu.oauth2.as.request.OAuthTokenRequest;
import org.apache.oltu.oauth2.as.response.OAuthASResponse;
import org.apache.oltu.oauth2.common.OAuth;
import org.apache.oltu.oauth2.common.error.OAuthError;
import org.apache.oltu.oauth2.common.exception.OAuthProblemException;
import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.apache.oltu.oauth2.common.message.OAuthResponse;
import org.apache.oltu.oauth2.common.message.types.GrantType;

import br.gov.ce.sefaz.ciofi.web.enuns.JsonEnum;
import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em gerar novos tokens e verificar se o token gerado é um
 * token valido basedo no padrão open source de autorização Oauth 2.0
 *
 * @author carlos.lima
 *
 * @version 1.0.1
 *
 */
@Path("/token")
public class TokenEndpoint extends ResponseBase {

	private RepositorioAplicacoesOAuthCIOF repositorioAplicacao;

	/**
	 * Default Constructor
	 */
	public TokenEndpoint() {

	}

	/**
	 * Injeção de dependência via construtor.
	 *
	 * @param repositorioAplicacao
	 * @param resource
	 */
	@Inject
	public TokenEndpoint(RepositorioAplicacoesOAuthCIOF repositorioAplicacao) {
		this.repositorioAplicacao = repositorioAplicacao;
	}

	/**
	 * Método que cria um token no padrão OAuth 2.0
	 *
	 * @param request
	 * @return Response
	 * @throws OAuthSystemException
	 */
	@POST
	@Consumes("application/x-www-form-urlencoded")
	@Produces("application/json")
	public Response authorize(@Context HttpServletRequest request) throws OAuthSystemException {
		try {

			OAuthResponse response = null;
			OAuthTokenRequest oauthRequest = new OAuthTokenRequest(request);
			OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

			if (!isClienteValido(oauthRequest.getClientId())) {
				return invalidoClienteIdResponse();
			}

			if (!isSenhaValida(oauthRequest.getClientId(), oauthRequest.getClientSecret())) {
				return invalidoClienteSenhaResponse();
			}

			if (oauthRequest.getParam(OAuth.OAUTH_GRANT_TYPE).equals(GrantType.CLIENT_CREDENTIALS.toString())) {

				final String accessToken = oauthIssuerImpl.accessToken();
				final Long timeExpiration = repositorioAplicacao.getTempoParaExpirarToken();
				repositorioAplicacao.adicionaToken(accessToken, timeExpiration);

				response = OAuthASResponse.tokenResponse(HttpServletResponse.SC_OK).setAccessToken(accessToken)
						.setExpiresIn(timeExpiration.toString()).buildJSONMessage();
			} else {
				return invalidoOAuthGrantTypeResponse();
			}

			return Response.status(response.getResponseStatus()).entity(response.getBody()).build();

		} catch (OAuthProblemException e) {
			return createResponse(JsonEnum.JSON_OAUTHAUTORIZATION_ERROR.getJson(
					MensagemResource.getMensagensProperties(MensagemResource.INVALIDO_CLIENTE_SENHA), e.getMessage()));
		}
	}

	private Response invalidoClienteIdResponse() {
		return createResponse(JsonEnum.JSON_OAUTHAUTORIZATION_ERROR.getJson(OAuthError.TokenResponse.INVALID_CLIENT,
				MensagemResource.getMensagensProperties(MensagemResource.INVALIDO_CLIENTE_SENHA)));
	}

	private Response invalidoClienteSenhaResponse() {
		return createResponse(JsonEnum.JSON_OAUTHAUTORIZATION_ERROR.getJson(OAuthError.TokenResponse.INVALID_CLIENT,
				MensagemResource.getMensagensProperties(MensagemResource.INVALIDO_CLIENTE_SENHA)));
	}

	private Response invalidoOAuthGrantTypeResponse() {
		return createResponse(
				JsonEnum.JSON_OAUTHAUTORIZATION_ERROR.getJson(OAuthError.TokenResponse.UNSUPPORTED_GRANT_TYPE,
						MensagemResource.getMensagensProperties(MensagemResource.INVALIDO_CLIENTE_CREDENCIAL)));
	}

	private boolean isClienteValido(String clienteId) {
		return repositorioAplicacao.isAplicacaoClienteValido(clienteId);
	}

	private boolean isSenhaValida(String clienteId, String secret) {
		return repositorioAplicacao.isAplicacaoSenhaValida(clienteId, secret);
	}
}