package br.gov.ce.sefaz.ciofi.web.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.model.entity.IndicadorFinanceiro;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/indicadoresfinanceiros")
@RequestScoped
@Api(value = "/indicadoresfinanceiros",
    description = "Efetua gestão da tabela de indicadores financeiros - SETA")
public interface IndicadorFinanceiroEndpoint {


  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Retorna todas os indicadores disponíveis", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Mensagem será uma lista de indicadores"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(@QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/find")
  @ApiOperation(value = "Obter todos as indicadores filtrados pelo exemplo",
      response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Mensagem será uma lista de indicadores"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(
      @ApiParam(value = "Indicador usada de exemplo",
          required = true) IndicadorFinanceiro indicadorFinanceiro,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);

  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro/1.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Retorna um indicador baseado no ID informado", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna um Indicador Financeiro Especifico"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findOne(
      @ApiParam(value = "ID do indicador financeiro", required = true) @PathParam("id") Long id);

  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro.
   * <p/>
   * salvar: {"codigo": null,"descricao":"TESTE REST" ,"sigla":"TST","tipo":"M","periodicidade":"D"}
   *
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Salva um indicador financeiro informado", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna o status de criação da entidade"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(@ApiParam(value = "Indicador financeiro que será criado",
      required = true) IndicadorFinanceiro indicadorFinanceiro);

  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro/1.
   * <p/>
   * salvar: {"codigo": null,"descricao":"TESTE REST" ,"sigla":"TST","tipo":"M","periodicidade":"D"}
   *
   */
  @PUT
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Atualiza um indicador Financeiro", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna a entidade atualizada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(
      @ApiParam(value = "Atualiza um indicador financeiro",
          required = true) IndicadorFinanceiro indicadorFinanceiro,
      @ApiParam(value = "ID do indicador", required = true) @PathParam("id") int id);


  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro?id=1.
   */
  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Remover um indicador baseado no ID informado", response = Response.class)
  @Path("{id}")
  @ApiResponses({ @ApiResponse(code = 200, message = "Remover um Indicador Financeiro Especifico"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response delete(@ApiParam(value = "Indicador financeiro que será removido",
      required = true) @PathParam("id") Long id);



}
