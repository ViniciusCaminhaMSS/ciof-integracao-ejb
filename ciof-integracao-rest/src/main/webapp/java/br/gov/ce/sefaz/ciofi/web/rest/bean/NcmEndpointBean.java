package br.gov.ce.sefaz.ciofi.web.rest.bean;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.springframework.data.domain.PageRequest;

import br.gov.ce.sefaz.ciofi.model.bean.NcmBo;
import br.gov.ce.sefaz.ciofi.model.entity.Ncm;
import br.gov.ce.sefaz.ciofi.web.rest.NcmEndpoint;
import br.gov.ce.sefaz.jee.util.ws.rest.RestLocalDateParam;

@Stateless
public class NcmEndpointBean implements NcmEndpoint {

  @Inject
  private NcmBo ncmBean;

  @Override
  public Response findAll(Integer page, Integer size) {
    try {
      return Response.ok(ncmBean.findAll(new PageRequest(page, size))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findAll(Ncm ncm, RestLocalDateParam dataInicial, RestLocalDateParam dataFinal,
      Integer page, Integer size) {
    try {
      LocalDate dataMin = dataInicial != null ? dataInicial.getDate() : null;
      LocalDate dataMax = dataFinal != null ? dataFinal.getDate() : null;
      return Response.ok(ncmBean.findAll(ncm, dataMin, dataMax, new PageRequest(page, size)))
          .build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response save(Ncm ncm) {
    try {
      ncmBean.save(ncm);
      return Response.created(null).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response save(Ncm ncm, Long id) {
    try {
      ncmBean.save(ncm);
      return Response.ok().build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findOne(Long id) {
    try {
      return Response.ok(ncmBean.findOne(id)).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response delete(Long id) {
    try {
      ncmBean.delete(id);
      return Response.ok().build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  private Map<String, String> error(Exception e) {
    Map<String, String> responseObj = new HashMap<>();
    responseObj.put("error", e.getMessage());
    return responseObj;
  }
}
