package br.gov.ce.sefaz.ciofi.web.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.model.entity.Ncm;
import br.gov.ce.sefaz.jee.util.ws.rest.RestLocalDateParam;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/ncms")
@RequestScoped
@Api(value = "/ncms", description = "Efetua gestão da tabela de NCMS - SETA")
public interface NcmEndpoint {

  /**
   * NCM. http://localhost:8080/seta/rest/ncm
   * 
   * @param page Pagina solicitada
   * @param size Tamanho da página
   * @return Entidades da página solicitada *
   * 
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Lista os ncms disponíveis", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna todos os ncms"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(@QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/find")
  @ApiOperation(value = "Obter todas as NCMs filtradas pelo exemplo", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Mensagem será uma lista de NCM"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(@ApiParam(value = "NCM usada de exemplo", required = true) Ncm ncm,
      @QueryParam("data_inicial") RestLocalDateParam dataInicial,
      @QueryParam("data_final") RestLocalDateParam dataFinal,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);


  /**
   * NCM. http://localhost:8080/seta/rest/ncm/
   * <p/>
   * salvar: {"codigo": null,"descricao":"TESTE REST" ,"sigla":"TST","tipo":"M","periodicidade":"D"}
   * 
   * @param ncm Entidade persistida
   * @return Mensagem da resposta
   */
  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Salvar NCM informado", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna ncm salvo"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(@ApiParam(value = "NCM que será salva", required = true) Ncm ncm);

  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro?id=1.
   */
  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Retorna o NCM solicitado", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "NCM Atualizado"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(
      @ApiParam(value = "Entidade NCM que será atualizada", required = true) Ncm ncm,
      @ApiParam(value = "Id da crt que será atualizada", required = true) @PathParam("id") Long id);

  /**
   * http://localhost:8080/seta/rest/ncm.
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Retorna o NCM solicitado", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Entidade NCM requisitada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findOne(@ApiParam(value = "Id do NCM", required = true) @PathParam("id") Long id);



  /**
   * http://localhost:8080/seta/rest/indicadorFinanceiro?id=1.
   */
  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Remove o NCM baeado em um di", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "NCM removido"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response delete(
      @ApiParam(value = "Id da crt que será removida", required = true) @PathParam("id") Long id);

}
