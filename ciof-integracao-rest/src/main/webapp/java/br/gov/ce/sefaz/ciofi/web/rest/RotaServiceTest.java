package br.gov.ce.sefaz.ciofi.web.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.rest.service.ExemplosService;

@RunWith(MockitoJUnitRunner.class)
public class RotaServiceTest {

	private static final String TESTE = "teste";

	private static final String NUMERO = "123";

	@Mock
	private HttpServletRequest request;

	@Mock
	private RotaAction routerAction;

	private ExemplosService rotaservice;

	@Before
	public void init() {
		rotaservice = new ExemplosService(routerAction);
	}

	@Test
	public void exemploComUmEndPointRetornarUmJson() {
		String jsonEsperado = getJson();
		when(routerAction.getOutput()).thenReturn(getJson());
		String jsonResposta = (String) rotaservice.exemploComUmEndPoint(request).getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}

	@Test
	public void exemploComUmEndPointRetornarNulo() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String jsonResposta = (String) rotaservice.exemploComUmEndPoint(request).getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void exemploComUmEndPointRetornarUmStringVazia() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn("");
		String jsonResposta = (String) rotaservice.exemploComUmEndPoint(request).getEntity();
		assertEquals(msgErroEsperada, jsonResposta);
	}

	@Test
	public void exemploComUmEndPointRetornaUmJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) rotaservice.exemploComUmEndPoint(request).getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}

	@Test
	public void exemploComDoisEndPointRetornarUmJson() {
		String jsonEsperado = getJson();
		when(routerAction.getOutput()).thenReturn(getJson());
		String jsonResposta = (String) rotaservice.exemploComDoisEndPoint(request, NUMERO).getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}

	@Test
	public void exemploComDoisEndPointRetornaUmJsonError() {
		String msgErroEsperada = jsonVazio();
		when(routerAction.getOutput()).thenReturn(null);
		String msgErrorResposta = (String) rotaservice.exemploComUmEndPoint(request).getEntity();
		assertEquals(msgErroEsperada, msgErrorResposta);
	}

	@Test
	public void exemploComTresEndPointUmJson() {
		String jsonEsperado = getJson();
		when(routerAction.getOutput()).thenReturn(getJson());
		String jsonResposta = (String) rotaservice.exemploComTresEndPoint(request, TESTE, TESTE, TESTE).getEntity();
		assertEquals(jsonEsperado, jsonResposta);
	}

	@Test
	public void testaConstrutorPadrao() {
		rotaservice = new ExemplosService();
		assertNotNull(rotaservice);
	}

	private String jsonVazio() {
		return "{\"codigo\":204,\"status\":\"sem resposta\"}";
	}

	private String getJson() {
		return "{\"cep\":\"01001-000\",\"uf\":\"SP\",\"unidade\":[],\"ibge\":\"3550308\",\"gia\":\"1004\"}";
	}
}
