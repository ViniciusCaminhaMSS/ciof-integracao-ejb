package br.gov.ce.sefaz.ciofi.web.rest.service;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumRegrasBRMS;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em forncer os serviços de integrações que valida as rebras
 * solicitadas pelo portal CIOF.
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
@Path("/regras")
@RequestScoped
// @Interceptors({ OAuthAutorizacaoInterceptor.class })
public class BrmsService extends ResponseBase {

	private RotaAction rotaAction;

	/**
	 * Contrutor default.
	 */
	public BrmsService() {

	}

	/**
	 * Construtor com Injeção de Dependências (DI).
	 *
	 * @param RotaAction
	 */
	@Inject
	public BrmsService(RotaAction rotaAction) {
		this.rotaAction = rotaAction;
	}

	@POST
	@Path("/valida/meta")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response verificaMetaHorasTrabalhada(@Context HttpServletRequest request, String json) {
		rotaAction.executar(RotaEnumRegrasBRMS.VALIDA_HORA_TRABALHADA.getName(), json);
		String jsonRequest = rotaAction.getOutput();
		return createResponse(jsonRequest);
	}
}
