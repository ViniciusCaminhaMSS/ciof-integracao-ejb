package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

public enum RotaEnumRegrasBRMS implements Serializable {
	VALIDA_HORA_TRABALHADA("validaHoraTrabalhada");

	private static final String DIRECT = "direct:";
	private String name;

	RotaEnumRegrasBRMS(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}
}
