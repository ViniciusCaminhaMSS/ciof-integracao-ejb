package br.gov.ce.sefaz.ciofi.web.security;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.oltu.oauth2.common.error.OAuthError;

import br.gov.ce.sefaz.ciofi.web.enuns.JsonEnum;
import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em interceptar as requisições aos servico do ciof valida e
 * autorização os acessos.
 *
 * @author carlos.lima
 *
 */
public class OAuthAutorizacaoInterceptor extends ResponseBase {

	@Inject
	private RepositorioAplicacoesOAuthCIOF bancoCIOF;

	@AroundInvoke
	public Object validaAutorizacao(InvocationContext context) throws Throwable {

		HttpServletRequest request = getHttpServletRequest(context);

		if (request != null) {
			String accessToken = request.getHeader("access_token");
			String clienteId = request.getHeader("client_id");
			if (!bancoCIOF.isAplicacaoClienteValido(clienteId)) {
				return createResponse(JsonEnum.JSON_OAUTHAUTORIZATION_ERROR.getJson(
						OAuthError.TokenResponse.UNAUTHORIZED_CLIENT,
						MensagemResource.getMensagensProperties(MensagemResource.AUTORIZACAO_CLIENTE_INVALIDO)));
			}
			if (!validaAccessToken(accessToken)) {
				return createResponse(JsonEnum.JSON_OAUTHAUTORIZATION_ERROR
						.getJson(OAuthError.ResourceResponse.INVALID_TOKEN, MensagemResource.INVALIDO_TOKENL));
			}
		}
		return context.proceed();
	}

	/**
	 *
	 * @param InvocationContext
	 * @return HttpServletRequest
	 */
	private HttpServletRequest getHttpServletRequest(InvocationContext context) {
		for (Object parameter : context.getParameters()) {
			if (parameter instanceof HttpServletRequest) {
				return (HttpServletRequest) parameter;
			}
		}
		return null;
	}

	/**
	 *
	 * @param accessToken
	 * @return boolean
	 */
	private boolean validaAccessToken(String accessToken) {
		boolean valido = false;
		if (accessToken != null && bancoCIOF.isTokenValido(accessToken)) {
			valido = true;
			if (!bancoCIOF.isTempoValido(accessToken)) {
				bancoCIOF.removeTokenExpirado(accessToken);
				valido = false;
			}
		}
		return valido;
	}
}
