package br.gov.ce.sefaz.ciofi.web.rest.service;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumChangePoint;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em forncer os serviços de integrações solicitadas pelo
 * portal CIOF.
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
@Path("/changepoint")
@RequestScoped
// @Interceptors({ OAuthAutorizacaoInterceptor.class })
public class ChangePointService extends ResponseBase {

	private RotaAction rotaAction;

	/**
	 * Contrutor default.
	 */
	public ChangePointService() {

	}

	/**
	 * Construtor com Injeção de Dependências (DI).
	 *
	 * @param RotaAction
	 */
	@Inject
	public ChangePointService(RotaAction routerAction) {
		this.rotaAction = routerAction;
	}

	/**
	 * http://localhost:8080/ciofi/rest/changepoint/solicitacao
	 *
	 * Realiza consulta de uma solicitação.
	 *
	 * @param idSolicitacao
	 */
	@GET
	@Path("/solicitacao/{idSolicitacao}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response consultaSolicitacao(@Context HttpServletRequest request,
			@PathParam("idSolicitacao") String idSolicitacao) {
		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("idSolicitacao", idSolicitacao);
		rotaAction.executar(RotaEnumChangePoint.CONSULTAR_SOLICITACAO.getName(), parametros);
		String json = rotaAction.getOutput();
		return createResponse(json);
	}

	@POST
	@Path("/solicitacao")
	@Consumes("application/json")
	@Produces(MediaType.APPLICATION_JSON)
	public Response savarSolicitacao(@Context HttpServletRequest request, String json) {
		rotaAction.executar(RotaEnumChangePoint.SALVAR_SOLICITACAO.getName(), json);
		String jsonRequest = rotaAction.getOutput();
		return createResponse(jsonRequest);
	}
}
