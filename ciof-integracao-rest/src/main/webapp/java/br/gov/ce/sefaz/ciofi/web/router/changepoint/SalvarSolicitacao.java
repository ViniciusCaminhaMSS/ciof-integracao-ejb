package br.gov.ce.sefaz.ciofi.web.router.changepoint;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

@ContextName("ciofi-context")
public class SalvarSolicitacao extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:SalvarSolicitacao").log("Nova solicitação").setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
				.setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
				.toD("http4://dese2.sefaz.ce.gov.br/changepoint-webservice/rest/solicitacao_sefaz/create")
				.log("Nova solicitação criada com sucesso").to("mock:result");
	}
}
