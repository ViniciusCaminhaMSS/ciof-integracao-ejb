package br.gov.ce.sefaz.ciofi.web.router.exemplo;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;
import org.apache.camel.model.dataformat.JsonLibrary;

import br.gov.ce.sefaz.ciofi.model.entity.MergeRota;
import br.gov.ce.sefaz.ciofi.model.entity.RootObject;
import br.gov.ce.sefaz.ciofi.web.processor.MergeDoisEndPoint;
import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

/**
 * Rota que executa 2 endpoints agregando o resultado em um unico json de
 * resposta.
 *
 * @author carlos.lima
 *
 */
@ContextName("ciofi-context")
public class ExemploComDoisEndPointRota extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:ExemploComDoisEndPointRota").routeId("Rota2").log("Rota 2 iniciada")
				.log("########################### Request 1 ##########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
				// .to("http4://viacep.com.br/ws/01001000/xml/")
				.to("http4://localhost:8081/restciof/cep/")
				// .convertBodyTo(String.class, "ISO-8859-1")
				.marshal().xmljson().unmarshal().json(JsonLibrary.Gson, MergeRota.class)
				.setProperty("rota", simple("${body}")).setBody(simple("")).removeHeaders("CamelHttp*")
				.log("########################### Request 2 ##########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
				// .to("http4://services.groupkt.com/state/get/IND/UP/")
				.to("http4://localhost:8081/restciof/country/")
				// .convertBodyTo(String.class, "ISO-8859-1")
				.unmarshal().json(JsonLibrary.Gson, RootObject.class).setProperty("jsonRootObjet", simple("${body}"))
				.process(new MergeDoisEndPoint()).marshal().json(JsonLibrary.Gson).to("mock:result");
	}
}
