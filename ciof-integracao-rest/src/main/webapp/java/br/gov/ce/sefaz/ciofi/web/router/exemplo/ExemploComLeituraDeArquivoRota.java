package br.gov.ce.sefaz.ciofi.web.router.exemplo;

import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.dataformat.xmljson.XmlJsonDataFormat;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

/**
 * Rota que realiza a leitura de um arquivo xml e retorna um json.
 *
 * @author carlos.lima
 *
 */
@ContextName("ciofi-context")
public class ExemploComLeituraDeArquivoRota extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		XmlJsonDataFormat xmlJsonFormat = new XmlJsonDataFormat();
		xmlJsonFormat.setEncoding("UTF-8");
		xmlJsonFormat.setTrimSpaces(true);
		xmlJsonFormat.setRemoveNamespacePrefixes(true);

		from("file://pedidos?noop=true").autoStartup(false).routeId("Rota Teste: ").log("#### inicio ####")
				.split(xpath("/pedido/itens")).marshal(xmlJsonFormat).log("${body}").to("mock:result");

	}

}
