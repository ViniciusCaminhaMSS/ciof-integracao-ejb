package br.gov.ce.sefaz.ciofi.web.rest.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.enums.TipoIndicadorFinanceiro;
import br.gov.ce.sefaz.ciofi.enums.TipoPeriodicidade;
import br.gov.ce.sefaz.ciofi.model.bean.CrtBo;
import br.gov.ce.sefaz.ciofi.web.rest.SetaEnumEndpoint;

@Stateless
public class EnumEndpointBean implements SetaEnumEndpoint {

  @Inject
  CrtBo bean;

  @Override
  public Response tiposIndicadorFinanceiro() {
    try {
      return Response.ok(TipoIndicadorFinanceiro.values()).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }


  @Override
  public Response tiposPeriodicidade() {
    try {
      return Response.ok(TipoPeriodicidade.values()).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  private Map<String, String> error(Exception e) {
    Map<String, String> responseObj = new HashMap<>();
    responseObj.put("error", e.getMessage());
    return responseObj;
  }
}
