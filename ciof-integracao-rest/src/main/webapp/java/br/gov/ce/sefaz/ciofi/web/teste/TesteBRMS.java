package br.gov.ce.sefaz.ciofi.web.teste;

import java.util.ArrayList;
import java.util.List;

import org.brms.myproject.Person;
import org.drools.core.command.impl.GenericCommand;
import org.kie.api.KieServices;
import org.kie.api.command.BatchExecutionCommand;
import org.kie.api.command.KieCommands;
import org.kie.server.api.marshalling.Marshaller;
import org.kie.server.api.marshalling.MarshallerFactory;
import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.api.model.ServiceResponse;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.RuleServicesClient;

public class TesteBRMS {

	private static final String URL = "http://localhost:8080/kie-server/services/rest/server";
	private static final String USER = "kieserver";
	private static final String PASSWORD = "kieserver1!";
	private static final String CONTAINER = "myContainer";
	private static final String KIESESSION = "mySession";

	@SuppressWarnings("deprecation")
	public static void main(String[] args) {

		KieServicesClient client = configure(URL, USER, PASSWORD);
		RuleServicesClient ruleClient = client.getServicesClient(RuleServicesClient.class);
		Person person = new Person();
		person.setFirstName("Carlos");
		person.setLastName("Roberto");
		person.setHourlyRate(5);
		person.setWage(40);
		/*BatchExecutionCommand command = CommandFactory.newBatchExecution(Arrays.asList(
				CommandFactory.newInsert(person,"person"),
				CommandFactory.newFireAllRules()
				), KIESESSION);

		ServiceResponse<ExecutionResults> results = ruleClient.executeCommandsWithResults(CONTAINER, command);
		if(results != null){
			Person personResult = (Person) results.getResult().getValue("person");
			System.out.println(personResult.toString());
		}else{
			System.out.println("Resultado vazio");
		}*/
		List<GenericCommand<?>> commands = new ArrayList<GenericCommand<?>>();
		KieCommands cmdFactory = KieServices.Factory.get().getCommands();
		commands.add((GenericCommand<?>) cmdFactory.newInsert(person, "person"));
		commands.add((GenericCommand<?>) cmdFactory.newFireAllRules());
		BatchExecutionCommand batchCommand = cmdFactory.newBatchExecution(commands, KIESESSION);

		Marshaller marshallerJson = MarshallerFactory.getMarshaller(MarshallingFormat.JSON,
				TesteBRMS.class.getClassLoader());
		String outJson = marshallerJson.marshall(batchCommand);
		System.out.println(outJson);

		System.out.println("======== server Response ========");
		ServiceResponse<String> response = ruleClient.executeCommands(CONTAINER, batchCommand);
		System.out.println(response.getResult());
		/*ServiceResponse<ExecutionResults> response = ruleClient.executeCommandsWithResults(CONTAINER, batchCommand);
		System.out.println(response.getResult().getValue("person").toString());*/

		/*String containerId = CONTAINER;
		System.out.println("== Sending commands to the server ==");
		RuleServicesClient rulesClient = configure(URL, USER, PASSWORD).getServicesClient(RuleServicesClient.class);
		KieCommands commandsFactory = KieServices.Factory.get().getCommands();

		Person person = new Person();
		person.setFirstName("Carlos");
		person.setLastName("Roberto");
		person.setHourlyRate(12);
		person.setWage(11);

		Command<?> insert = commandsFactory.newInsert(person, "Person insert ID");
		Command<?> fireAllRules = commandsFactory.newFireAllRules();
		Command<?> batchCommand = commandsFactory.newBatchExecution(Arrays.asList(insert, fireAllRules), KIESESSION);

		//ServiceResponse<String> executeResponse = rulesClient.executeCommands(containerId, batchCommand);
		ServiceResponse<ExecutionResults> executeResponse = rulesClient.executeCommandsWithResults(containerId, batchCommand);

		if (executeResponse.getType() == ResponseType.SUCCESS) {
			System.out.println("Commands executed with success! Response: ");
			Collection<String> identifiers = executeResponse.getResult().getIdentifiers();
			for (Iterator iterator = identifiers.iterator(); iterator.hasNext();) {
				String string = (String) iterator.next();
				System.out.println(string);
			}
			System.out.println(executeResponse.getResult().getValue("Person insert ID"));
			Person p = (Person) executeResponse.getResult().getValue("Person insert ID");
			System.out.println("========== print Person =========");
			System.out.println(p.getFirstName());
			System.out.println(p.getLastName());
			System.out.println(p.getHourlyRate());
			System.out.println(p.getWage());
			System.out.println(p.getRich());
		} else {
			System.out.println("Error executing rules. Message: ");
			System.out.println(executeResponse.getMsg());
		}*/
	}

	private static KieServicesClient configure(String url, String username, String password) {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(url, username, password);
		config.setMarshallingFormat(MarshallingFormat.JSON);
		return KieServicesFactory.newKieServicesClient(config);
	}
}
