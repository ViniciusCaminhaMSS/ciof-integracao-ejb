package br.gov.ce.sefaz.ciofi.web.rest.bean;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

import org.jboss.logging.Logger;
import org.springframework.data.domain.PageRequest;

import br.gov.ce.sefaz.ciofi.model.bean.CrtBo;
import br.gov.ce.sefaz.ciofi.model.entity.Crt;
import br.gov.ce.sefaz.ciofi.web.rest.CrtEndpoint;



@Stateless
public class CrtEndpointBean implements CrtEndpoint {

  @Inject
  private Logger log;

  @Inject
  CrtBo bean;

  @Override
  public Response findAll(Integer page, Integer size) {
    try {
      return Response.ok(bean.findAll(new PageRequest(page, size))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findAll(Crt crt, Integer page, Integer size) {
    try {
      return Response.ok(bean.findAll(crt, new PageRequest(page, size))).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response findOne(Long id) {
    try {
      Crt crt = bean.findOne(id);
      if (crt == null) {
        return Response.status(Response.Status.NOT_FOUND).build();
      }
      return Response.ok(crt).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response save(Crt crt) {
    try {
      bean.save(crt);
      return Response.status(Response.Status.CREATED).entity(crt).build();
    } catch (Exception e) {
      log.error("Erro ao criar nova CRT", e);
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response save(Crt crt, int id) {
    try {
      crt.setCodigo(id);
      return Response.ok(bean.save(crt)).build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  @Override
  public Response delete(Long id) {
    try {
      bean.delete(id);
      return Response.noContent().build();
    } catch (Exception e) {
      return Response.status(Response.Status.BAD_REQUEST).entity(error(e)).build();
    }
  }

  private Map<String, String> error(Exception e) {
    Map<String, String> responseObj = new HashMap<>();
    responseObj.put("error", e.getMessage());
    return responseObj;
  }
}
