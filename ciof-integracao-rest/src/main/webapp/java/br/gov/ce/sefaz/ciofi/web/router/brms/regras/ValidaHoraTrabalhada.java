package br.gov.ce.sefaz.ciofi.web.router.brms.regras;

import javax.ws.rs.core.MediaType;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

/**
 * Rota que executa regra no BRMS valida as horas trabalhadas.
 *
 * @author carlos.lima
 *
 */
@ContextName("ciofi-context")
public class ValidaHoraTrabalhada extends RouteBuilder {

	private static final String URL_BRMS = "http4://10.10.57.135:8080/kie-server/services/rest/server/containers/instances/myContainer";
	private static final String URL_PARAMETROS_BRMS = "?authMethod=Basic&authUsername=kieserver&authPassword=kieserver1!";

	@Override
	public void configure() throws Exception {
		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:validaHoraTrabalhada").setHeader(Exchange.HTTP_METHOD, HttpMethods.POST)
				.setHeader(Exchange.CONTENT_TYPE, constant(MediaType.APPLICATION_JSON))
				.to(URL_BRMS + URL_PARAMETROS_BRMS).end();
	}

}
