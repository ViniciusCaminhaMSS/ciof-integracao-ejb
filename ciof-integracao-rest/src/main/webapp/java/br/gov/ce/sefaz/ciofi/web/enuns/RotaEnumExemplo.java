package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

/**
 * Classe enum responsável por registrar os nomes de cada rota
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
public enum RotaEnumExemplo implements Serializable {
	EXEMPLO_COM_UM_END_POINT_ROTA("ExemploComUmEndPointRota"),
	EXEMPLO_COM_DOIS_END_POINT_ROTA("ExemploComDoisEndPointRota"),
	EXEMPLO_COM_TRES_END_POINT_ROTA("ExemploComTresEndPointRota");

	private static final String DIRECT = "direct:";
	private String name;

	RotaEnumExemplo(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}

}
