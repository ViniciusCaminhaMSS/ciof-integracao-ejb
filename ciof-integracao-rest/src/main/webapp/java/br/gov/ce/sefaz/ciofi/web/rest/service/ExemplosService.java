package br.gov.ce.sefaz.ciofi.web.rest.service;

import java.util.HashMap;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.lang.StringUtils;

import br.gov.ce.sefaz.ciofi.web.action.RotaAction;
import br.gov.ce.sefaz.ciofi.web.enuns.RotaEnumExemplo;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em forncer os serviços de integrações solicitadas pelo
 * portal CIOF.
 *
 * @author Carlos Roberto
 *
 * @version 1.0
 *
 */
@Path("/exemplo")
@RequestScoped
// @Interceptors({ OAuthAutorizacaoInterceptor.class })
public class ExemplosService extends ResponseBase {

	private RotaAction routerAction;

	/**
	 * Contrutor default.
	 */
	public ExemplosService() {

	}

	/**
	 * Construtor com Injeção de Dependências (DI).
	 *
	 * @param RotaAction
	 */
	@Inject
	public ExemplosService(RotaAction routerAction) {
		this.routerAction = routerAction;
	}

	/**
	 * http://localhost:8080/ciofi/rest/exemplo/rota1
	 *
	 * Rota simples com um endpoint que retorna json.
	 *
	 *
	 */
	@GET
	@Path("/rota1")
	@Produces(MediaType.APPLICATION_JSON)
	public Response exemploComUmEndPoint(@Context HttpServletRequest request) {
		routerAction.executar(RotaEnumExemplo.EXEMPLO_COM_UM_END_POINT_ROTA.getName(), StringUtils.EMPTY);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	/**
	 * http://localhost:8080/ciofi/rest/router/rota2/
	 *
	 * Rota com dois endpoint que retorna json.
	 *
	 * @param numeroPedido
	 * @return String JSON
	 */
	@GET
	@Path("/rota2/{numeroPedido}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response exemploComDoisEndPoint(@Context HttpServletRequest request,
			@PathParam("numeroPedido") String numeroPedido) {
		routerAction.executar(RotaEnumExemplo.EXEMPLO_COM_DOIS_END_POINT_ROTA.getName(), numeroPedido);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

	/**
	 * http://localhost:8080/ciofi/rest/router/rota3
	 *
	 * Rota com três endpoint e três parâmetros retornando um json.
	 *
	 * @param name
	 * @param key
	 * @param pwd
	 *
	 * @return Objeto JSON
	 */
	@POST
	@Path("/rota3")
	@Consumes("application/x-www-form-urlencoded")
	@Produces(MediaType.APPLICATION_JSON)
	public Response exemploComTresEndPoint(@Context HttpServletRequest request, @FormParam("name") String name,
			@FormParam("key") String key, @FormParam("pwd") String pwd) {
		Map<String, String> parametros = new HashMap<String, String>();
		parametros.put("name", name);
		parametros.put("key", key);
		parametros.put("pwd", pwd);
		routerAction.executar(RotaEnumExemplo.EXEMPLO_COM_TRES_END_POINT_ROTA.getName(), parametros);
		String json = routerAction.getOutput();
		return createResponse(json);
	}

}
