package br.gov.ce.sefaz.ciofi.web.router.exemplo;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

/**
 * Rota que executa um endpoints e o resultado é transformado em um json de
 * resposta.
 *
 * @author carlos.lima
 *
 */
@ContextName("ciofi-context")
public class ExemploComUmEndPointRota extends RouteBuilder {

	@Override
	public void configure() throws Exception {
		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:ExemploComUmEndPointRota").routeId("Rota com um endpoit").log("Rota 1 iniciada")
				.log("########################### Request 1 ###########################")
				.setHeader(Exchange.HTTP_METHOD, HttpMethods.GET).to("http4://localhost:8081/restciof/cep/")
				.convertBodyTo(String.class, "ISO-8859-1").marshal().xmljson().to("mock:result");
	}
}
