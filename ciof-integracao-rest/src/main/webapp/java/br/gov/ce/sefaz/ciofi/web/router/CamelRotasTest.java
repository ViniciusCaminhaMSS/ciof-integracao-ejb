package br.gov.ce.sefaz.ciofi.web.router;

import java.util.HashMap;
import java.util.Map;

import org.apache.camel.CamelContext;
import org.apache.camel.component.mock.MockEndpoint;
import org.apache.camel.impl.DefaultCamelContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import br.gov.ce.sefaz.ciofi.web.router.exemplo.ExemploComDoisEndPointRota;
import br.gov.ce.sefaz.ciofi.web.router.exemplo.ExemploComLeituraDeArquivoRota;
import br.gov.ce.sefaz.ciofi.web.router.exemplo.ExemploComTresEndPointRota;
import br.gov.ce.sefaz.ciofi.web.router.exemplo.ExemploComUmEndPointRota;

@RunWith(MockitoJUnitRunner.class)
public class CamelRotasTest {

	private CamelContext context;
	private ExemploComUmEndPointRota exemploComUmEndPonit;
	private ExemploComDoisEndPointRota exemploComDoisEndPointRota;
	private ExemploComTresEndPointRota exemploComTresEndPointRota;
	private ExemploComLeituraDeArquivoRota exemploComLeituraDeArquivoRota;

	@Before
	public void init() {
		exemploComUmEndPonit = new ExemploComUmEndPointRota();
		exemploComDoisEndPointRota = new ExemploComDoisEndPointRota();
		exemploComTresEndPointRota = new ExemploComTresEndPointRota();
		exemploComLeituraDeArquivoRota = new ExemploComLeituraDeArquivoRota();
	}

	@Test
	public void exemploComUmEndPointRota() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(exemploComUmEndPonit);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void exemploComDoisEndPointRota() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(exemploComDoisEndPointRota);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void exemploComTresEndPointRota() {
		try {
			Map<String, String> formParams = new HashMap<>();
			formParams.put("name", "teste");
			formParams.put("key", "key");
			formParams.put("pwd", "pwd");
			context = new DefaultCamelContext();
			context.addRoutes(exemploComTresEndPointRota);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void exemploComLeituraDeArquivoRota() {
		try {
			context = new DefaultCamelContext();
			context.addRoutes(exemploComLeituraDeArquivoRota);
			context.start();
			MockEndpoint mock = context.getEndpoint("mock:result", MockEndpoint.class);
			mock.assertIsSatisfied();
			context.stop();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
