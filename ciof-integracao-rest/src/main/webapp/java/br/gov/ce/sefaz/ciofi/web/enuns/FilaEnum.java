package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

public enum FilaEnum implements Serializable {

	ENVIAR_ALERTA_FISCAL("enviarAlertaFiscal"), ENVIAR_ALERTA_BPMS("enviarAlertaBPMS");

	private static final String DIRECT = "direct:";
	private String name;

	FilaEnum(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}
}
