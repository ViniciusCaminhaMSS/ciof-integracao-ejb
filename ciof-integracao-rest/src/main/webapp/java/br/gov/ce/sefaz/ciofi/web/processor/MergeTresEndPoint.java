package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.MergeRota;
import br.gov.ce.sefaz.ciofi.model.entity.RootObject;

/**
 * Exemplo de merege de treês endpoint.
 *
 * @author carlos.lima
 *
 */
public class MergeTresEndPoint implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {

		MergeRota rota = exchange.getProperty("uriCEP", MergeRota.class);
		RootObject ind = exchange.getProperty("uriIND", RootObject.class);
		RootObject usa = exchange.getProperty("uriUSA", RootObject.class);

		rota.setCountry(ind.getRestResponse().getResult().getCountry());
		rota.setName(usa.getRestResponse().getResult().getName());
		exchange.getOut().setBody(rota);

	}

}
