package br.gov.ce.sefaz.ciofi.web.router.fila.bpms;

import org.apache.camel.builder.RouteBuilder;

import br.gov.ce.sefaz.ciofi.web.processor.FilaCamelError;

public class RecebeBPMS extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new FilaCamelError()).to("stream:out");

		from("direct:receberAlertaBPMS", "sjms:queue:AlertaBPMS").process(exchange -> {
			if (exchange.getIn().getBody() != null && !exchange.getIn().getBody().toString().isEmpty()) {
				System.out.println("Recebendo da fila BPMS: " + exchange.getIn().getBody().toString());
			}
		}).end();
	}
}
