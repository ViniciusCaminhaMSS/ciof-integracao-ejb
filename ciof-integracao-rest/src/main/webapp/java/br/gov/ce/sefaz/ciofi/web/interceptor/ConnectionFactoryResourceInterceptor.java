package br.gov.ce.sefaz.ciofi.web.interceptor;

import javax.inject.Inject;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

import org.drools.core.util.StringUtils;

import br.gov.ce.sefaz.ciofi.web.context.FilaCamelContext;
import br.gov.ce.sefaz.ciofi.web.enuns.JsonEnum;
import br.gov.ce.sefaz.ciofi.web.helper.ConnectionFactoryResourceHelper;
import br.gov.ce.sefaz.ciofi.web.util.ResponseBase;

/**
 * Classe responsável em verifica se existe conexão com o servidor de fila do
 * ciof do contrário retorna uma resposta de erro.
 *
 * @author carlos.lima
 *
 */
public class ConnectionFactoryResourceInterceptor extends ResponseBase {

	@Inject
	private ConnectionFactoryResourceHelper connectionFactoryResourceHelper;

	@Inject
	private FilaCamelContext filaCamelContext;

	@AroundInvoke
	public Object validaConnectionFactoryAMQ(InvocationContext context) throws Throwable {

		if (connectionFactoryResourceHelper.isValidaConexaoComServidorAMQ()) {
			if (!connectionFactoryResourceHelper.isComponetSjmsIniciado()) {
				filaCamelContext.createCamelContext();
			}
		} else {
			/*
			 * TODO Possivelmente implementar segundo reguisito as chamada para
			 * a rota de contigencia para salvar mesagens que não foram
			 * entregues ao servidor de fila quando o mesmo permancer off line.
			 *
			 */
			return createResponse(JsonEnum.JSON_CONNECTIONFACTORY_AMQ_ERROR.getJson(StringUtils.EMPTY));
		}

		return context.proceed();
	}
}
