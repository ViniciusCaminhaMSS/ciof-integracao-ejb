package br.gov.ce.sefaz.ciofi.web.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import br.gov.ce.sefaz.ciofi.model.entity.MergeRota;
import br.gov.ce.sefaz.ciofi.model.entity.RootObject;

/**
 * Exemplo de merege de dois endpoint.
 *
 * @author carlos.lima
 *
 */
public class MergeDoisEndPoint implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		MergeRota rota = exchange.getProperty("rota", MergeRota.class);

		RootObject rootObject = exchange.getProperty("jsonRootObjet", RootObject.class);
		if (rootObject != null) {
			if (rootObject.getRestResponse() != null && rootObject.getRestResponse().getResult() != null
					&& rootObject.getRestResponse().getResult().getCountry() != null) {
				rota.setCountry(rootObject.getRestResponse().getResult().getCountry());
			}

			if (rootObject.getRestResponse() != null && rootObject.getRestResponse().getResult() != null
					&& rootObject.getRestResponse().getResult().getName() != null) {
				rota.setName(rootObject.getRestResponse().getResult().getName());
			}
		}
		exchange.getOut().setBody(rota);
	}
}
