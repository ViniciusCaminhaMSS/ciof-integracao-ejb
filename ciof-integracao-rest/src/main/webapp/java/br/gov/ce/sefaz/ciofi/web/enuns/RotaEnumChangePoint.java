package br.gov.ce.sefaz.ciofi.web.enuns;

import java.io.Serializable;

public enum RotaEnumChangePoint implements Serializable {
	CONSULTAR_SOLICITACAO("ConsultarSolicitacao"),
	SALVAR_SOLICITACAO("SalvarSolicitacao");

	private static final String DIRECT = "direct:";
	private String name;

	RotaEnumChangePoint(String name) {
		this.name = name;
	}

	public String getName() {
		return DIRECT + name;
	}
}
