package br.gov.ce.sefaz.ciofi.web.rest;


import javax.enterprise.context.RequestScoped;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.gov.ce.sefaz.ciofi.model.entity.Crt;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/crts")
@RequestScoped
@Api(value = "crts", description = "Efetua gestão da tabela de CRTs - SETA")
public interface CrtEndpoint {


  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Retorna todas as CRTs disponíveis", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Mensagem será uma lista de CRTs"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(@QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @Path("/find")
  @ApiOperation(value = "Obter todas as CRTs filtradas pelo exemplo", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Mensagem será uma lista de CRTs"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findAll(@ApiParam(value = "CRT usada de exemplo", required = true) Crt crt,
      @QueryParam("page") @DefaultValue("0") Integer page,
      @QueryParam("size") @DefaultValue("10") Integer size);

  @GET
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Recupera uma CRT especifica baseado no id", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna a CRT solicitada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response findOne(@ApiParam(value = "Id da CRT", required = true) @PathParam("id") Long id);

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Salva a CRT", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 201, message = "Retorna a CRT enviada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(@ApiParam(value = "CRT que será salva", required = true) Crt crt);

  @PUT
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Atualizar a CRT informada", response = Response.class)
  @Path("{id}")
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna a CRT solicitada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response save(@ApiParam(value = "CRT que será atualizada", required = true) Crt crt,
      @ApiParam(value = "ID da CRT", required = true) @PathParam("id") int id);

  @DELETE
  @Produces(MediaType.APPLICATION_JSON)
  @Path("{id}")
  @ApiOperation(value = "Remove uma CRT especifica baseado no id", response = Response.class)
  @ApiResponses({ @ApiResponse(code = 200, message = "Retorna a CRT solicitada"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response delete(@ApiParam(value = "Id da CRT", required = true) @PathParam("id") Long id);

}
