package br.gov.ce.sefaz.ciofi.web.router.changepoint;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.cdi.ContextName;
import org.apache.camel.component.http4.HttpMethods;

import br.gov.ce.sefaz.ciofi.web.processor.RotaCamelError;

@ContextName("ciofi-context")
public class ConsultarSolicitacao extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new RotaCamelError()).to("stream:out");

		from("direct:ConsultarSolicitacao").routeId("Consultar solicitação no change point")
				.log("Consulta da solicitação iniciada").setHeader(Exchange.HTTP_METHOD, HttpMethods.GET)
				.setHeader("idSolicitacao", simple("${body[idSolicitacao]}")).setBody(simple(""))
				.toD("http4://dese2.sefaz.ce.gov.br/changepoint-webservice/rest/solicitacao/${header.idSolicitacao}")
				.log("Consulta da solicitaçãoo finalizada").to("mock:result");
	}
}
