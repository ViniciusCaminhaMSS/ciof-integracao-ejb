package br.gov.ce.sefaz.ciofi.web.helper;

import javax.ejb.DependsOn;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.jms.Connection;
import javax.jms.JMSException;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.component.sjms.SjmsComponent;
import org.apache.camel.component.sjms.jms.ConnectionFactoryResource;
import org.apache.camel.impl.DefaultCamelContext;

import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;

/**
 * Responável em criar uma fabrica de conexão com o servidor de fila JBossAMQ.
 *
 * @author Carlos Roberto
 *
 */
@Startup
@Singleton
@DependsOn({ "MensagemResource" })
@ApplicationScoped
public class ConnectionFactoryResourceHelper {

	private static final String SJMS = "sjms";
	private boolean componetSjmsInicado = Boolean.FALSE;
	private boolean conexaoIniciada = Boolean.FALSE;

	private ActiveMQConnectionFactory connectionFactory;
	private SjmsComponent sjmsComponent;

	/**
	 * valiva se a conexão com o servidor de fila está funcionando.
	 *
	 */
	public boolean isValidaConexaoComServidorAMQ() {
		try {
			createConnnection();
			conexaoIniciada = Boolean.TRUE;
		} catch (JMSException e) {
			conexaoIniciada = Boolean.FALSE;
			componetSjmsInicado = Boolean.FALSE;
			System.out.println(e.getMessage());
			System.out.println(MensagemResource.getAplicacaoProperties(MensagemResource.SARVER_ERRO_AMQ));
		}
		return conexaoIniciada;
	}

	/**
	 * Cria a conexão com o servidor de fila.
	 *
	 */
	private void createConnnection() throws JMSException {
		if (connectionFactory == null) {
			createConnectionFactory();
		}
		Connection connection = connectionFactory.createConnection(
				MensagemResource.getAplicacaoProperties(MensagemResource.USUARIO_AMQ),
				MensagemResource.getAplicacaoProperties(MensagemResource.SENHA_AMQ));
		connection.close();
	}

	/**
	 * Cria o CamelContext.
	 *
	 */
	public CamelContext getCameleContext() {
		CamelContext camelContext = new DefaultCamelContext();
		camelContext.removeComponent(SJMS);
		camelContext.addComponent(SJMS, getSjmsComponent());
		componetSjmsInicado = Boolean.TRUE;
		return camelContext;
	}

	/**
	 * Cria o SjmComponent.
	 *
	 */
	private SjmsComponent getSjmsComponent() {
		sjmsComponent = new SjmsComponent();
		sjmsComponent.setConnectionResource(new ConnectionFactoryResource(10, connectionFactory,
				MensagemResource.getAplicacaoProperties(MensagemResource.USUARIO_AMQ),
				MensagemResource.getAplicacaoProperties(MensagemResource.SENHA_AMQ)));
		return sjmsComponent;
	}

	/**
	 * Cria uma ActiveMQConnectionFactory e otimizado a perfomace.
	 *
	 */
	private void createConnectionFactory() {
		connectionFactory = new ActiveMQConnectionFactory(
				MensagemResource.getAplicacaoProperties(MensagemResource.HOST_AMQ));
		connectionFactory.setCopyMessageOnSend(false);
		connectionFactory.setOptimizeAcknowledge(true);
		connectionFactory.setOptimizedMessageDispatch(true);
	}

	public boolean isConxexaoIniciada() {
		return conexaoIniciada;
	}

	public boolean isComponetSjmsIniciado() {
		return componetSjmsInicado;
	}

}
