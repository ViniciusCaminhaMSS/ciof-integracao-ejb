package br.gov.ce.sefaz.ciofi.web.router.fila.bpms;

import org.apache.camel.builder.RouteBuilder;

import br.gov.ce.sefaz.ciofi.web.processor.FilaCamelError;

public class EnviaBPMS extends RouteBuilder {

	@Override
	public void configure() throws Exception {

		onException(Exception.class).handled(true).process(new FilaCamelError()).to("stream:out");

		from("direct:enviarAlertaBPMS").process(exchange -> {
			if (exchange.getIn().getBody() != null && !exchange.getIn().getBody().toString().isEmpty()) {
				System.out.println("Enviando para a fila BPMS " + exchange.getIn().getBody().toString());
			}
		}).to("sjms:queue:AlertaBPMS").end();

	}
}
