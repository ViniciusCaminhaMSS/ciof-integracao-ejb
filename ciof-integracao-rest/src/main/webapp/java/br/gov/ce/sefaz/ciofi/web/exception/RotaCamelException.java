package br.gov.ce.sefaz.ciofi.web.exception;

import javax.ejb.ApplicationException;

import br.gov.ce.sefaz.ciofi.exception.CiofiException;

@ApplicationException
public class RotaCamelException extends CiofiException {

	private static final long serialVersionUID = 6720230679441726593L;

	public RotaCamelException(String mensagem, Object... parametros) {
		super(mensagem, parametros);
	}

	public RotaCamelException(String message, Throwable cause) {
		super(message, cause);
	}

	public RotaCamelException(String mensagem) {
		super(mensagem);
	}

	public RotaCamelException(Throwable e) {
		super(e);
	}

}
