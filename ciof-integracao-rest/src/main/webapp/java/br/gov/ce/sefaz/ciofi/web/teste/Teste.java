package br.gov.ce.sefaz.ciofi.web.teste;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;

import br.gov.ce.sefaz.ciofi.model.entity.Aplicacao;
import br.gov.ce.sefaz.ciofi.web.util.MensagemResource;

public class Teste {

	private static Map<String, Aplicacao> portais = new HashMap<String, Aplicacao>();

	public static void main(String[] args) throws ParseException {

		String datahora = "Tue Aug 29 14:15:18 GMT-03:00 2017";
		System.out.println("data " + new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(datahora));

		/*Instant agora = Instant.now();
		System.out.println("Instant " + agora.toString());

		// System.out.println("Instant: " + agora.toString());

		LocalDateTime dataAtual = LocalDateTime.now();
		System.out.println("Data e hora atual: " + dataAtual.toString());
		dataAtual = dataAtual.plusHours(Long.parseLong("0"));
		dataAtual = dataAtual.plusMinutes(Long.parseLong("2"));
		System.out.println("Data e hora atual: " + dataAtual.toString());
		// Long tempo =
		// dataAtual.atZone(ZoneId.of("America/Sao_Paulo")).toInstant().toEpochMilli();
		Long tempo = dataAtual.atZone(ZoneId.of("America/Sao_Paulo")).toInstant().toEpochMilli();
		System.out.println(tempo);

		GregorianCalendar dataAtual2 = new GregorianCalendar();
		System.out.println("Data e hora atual: " + dataAtual2.getTime().toString());
		dataAtual2.setTime(Calendar.getInstance().getTime());
		dataAtual2.add(Calendar.HOUR, Integer.parseInt("0"));
		dataAtual2.add(Calendar.MINUTE, Integer.parseInt("2"));
		System.out.println("Data e hora atual: " + dataAtual2.getTime().toString());
		System.out.println("long " + dataAtual2.getTimeInMillis());

		System.out.println("ctm: " + System.currentTimeMillis());*/

		/*
		 * String[] carct
		 * ={"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f","g"
		 * ,"h","i","j","k","l","m","n","o","p","q","r","s","t",
		 * "u","v","w","x","y","z","A","B","C","D","E","F","G","H","I","J","K",
		 * "L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"}; String
		 * senha=""; for (int x=0; x < 32; x++){ int j = (int)
		 * (Math.random()*carct.length); senha += carct[j]; }
		 * System.out.println("A SENHA GERADA É: "+senha);
		 */

		/*
		 * int count = 1; Portal portal = new Portal(); ResourceBundle resource
		 * = ResourceBundle.getBundle("portais", new Locale("pt","BR"));
		 * Set<String> keySet = resource.keySet();
		 *
		 * for (int i = 0; i < keySet.size() / 3; i++) {
		 * portal.setNome(resource.getString("portal" + (i + 1) + ".nome"));
		 * portal.setClienteId(resource.getString("client" + (i + 1) + ".id"));
		 * portal.setClienteSenha(resource.getString("client" + (i + 1) +
		 * ".secret")); portais.put(portal.getClienteId(), portal); portal = new
		 * Portal(); }
		 *
		 * Set<String> chaves = portais.keySet();
		 *
		 * for (String chave : chaves) { Portal p = portais.get(chave);
		 * System.out.println("nome: " + p.getNome());
		 * System.out.println("cliente: " + p.getClienteId());
		 * System.out.println("secret: " + p.getClienteSenha());
		 * System.out.println(""); }
		 */

		//
		// Map<String, String> jsonMap = new LinkedHashMap<String, String>();
		// jsonMap.put("customerId", "CIOFE MODULO - 007745CX");
		// jsonMap.put("engagementId", "DB9B5B4F-018E-4732-8E68-53EFB4AA5D90");
		// jsonMap.put("requestType", "CS - CIOFE");
		// jsonMap.put("requestPriority", "Alta");
		// jsonMap.put("requestStatus", "NEW");
		// jsonMap.put("product", "ATIVO INDISPONÍVEL");
		// jsonMap.put("description", "TESTE CIOF");
		// jsonMap.put("details", "Webservice em desenvolvimento");
		// JSONObject json = new JSONObject(jsonMap);
		// System.out.println(json.toString(2));

	}

}
