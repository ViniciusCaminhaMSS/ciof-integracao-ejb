package br.gov.ce.sefaz.ciofi.web.rest;

import javax.enterprise.context.RequestScoped;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Path("/enum")
@RequestScoped
@Api(value = "enumeration", description = "Fornece as enumerations do SETA")
public interface SetaEnumEndpoint {


  @GET
  @Path("/tiposIndicadorFinanceiro")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Retorna os tipos de Indicadores Financeiros", response = Response.class)
  @ApiResponses({
      @ApiResponse(code = 200,
          message = "Mensagem será uma lista de tipos de Indicadores Financeiros"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response tiposIndicadorFinanceiro();

  @GET
  @Path("/tiposPeriodicidade")
  @Produces(MediaType.APPLICATION_JSON)
  @ApiOperation(value = "Retorna os tipos de Periodicidade", response = Response.class)
  @ApiResponses({
      @ApiResponse(code = 200, message = "Mensagem será uma lista de tipos de Periodicidade"),
      @ApiResponse(code = 400, message = "Falha ao processar a solicitação") })
  public Response tiposPeriodicidade();

}
