package br.gov.ce.ciof.diligenciacentral.rest.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.ciof.diligenciacentral.dto.AcaoFiscal;
import br.gov.ce.ciof.diligenciacentral.dto.AutoInfracao;
import br.gov.ce.ciof.diligenciacentral.dto.Contador;
import br.gov.ce.ciof.diligenciacentral.dto.Contribuinte;
import br.gov.ce.ciof.diligenciacentral.dto.Diligencia;
import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS2;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS3;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS4;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS5;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS6;
import br.gov.ce.ciof.diligenciacentral.dto.DossiePlacaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.Empresa;
import br.gov.ce.ciof.diligenciacentral.dto.Endereco;
import br.gov.ce.ciof.diligenciacentral.dto.MandadoSeguranca;
import br.gov.ce.ciof.diligenciacentral.dto.NotaFiscalDTO;
import br.gov.ce.ciof.diligenciacentral.dto.RegimeTributacao;
import br.gov.ce.ciof.diligenciacentral.dto.TransitoLivre;
import br.gov.ce.ciof.diligenciacentral.dto.Veiculo;
import br.gov.ce.ciof.diligenciacentral.rest.DiligenciaCentralEndPoint;

@RestController
@RequestMapping("/mock-diligencia-central")
public class MockDiligenciaCentralController implements DiligenciaCentralEndPoint {

	@Override
	public ResponseEntity<List<Empresa>> buscarEmpresa(String cgf, String cnpj, String razaoSocial, String placa,
			String chaveAcesso, String numeroNF) {

		List<Empresa> lista = new ArrayList<>();
		Endereco endereco = new Endereco("Rua teste", "2221", "casa 2", "Centro", 6000000L, "CE", "Fortaleza");

		lista.add(new Empresa(56078565000146L, 345654324L, "Razão Social 01", endereco, "Ativo",
				"0134-2/00 Cultivo de café"));
		lista.add(new Empresa(56078565000146L, 234245443L, "Razão Social 02", endereco, "Ativo",
				"0134-2/00 Cultivo de café"));
		lista.add(new Empresa(56078565000146L, 435664577L, "Razão Social 03", endereco, "Ativo",
				"0134-2/00 Cultivo de café"));

		return new ResponseEntity<List<Empresa>>(lista, HttpStatus.OK);

	}

	@Override
	public ResponseEntity<List<NotaFiscalDTO>> buscarPorNFeOuChaveAcesso(String chaveAcesso, String numeroNfe) {
		List<NotaFiscalDTO> lista = new ArrayList<>();

		lista.add(new NotaFiscalDTO(123L, "Teste emitente", "RN", "Teste destinatario", "CE"));
		lista.add(new NotaFiscalDTO(123L, "Teste emitente", "SP", "Teste destinatario", "CE"));
		lista.add(new NotaFiscalDTO(123L, "Teste emitente", "RJ", "Teste destinatario", "CE"));

		return new ResponseEntity<List<NotaFiscalDTO>>(lista, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossiePlacaWS1> buscarPlaca(String placa) {

		DossiePlacaWS1 dossiePlaca = new DossiePlacaWS1();
		List<NotaFiscalDTO> nfsVinculadasMDFe = new ArrayList<>();
		List<NotaFiscalDTO> nfsVinculadasMDFePassagemCE = new ArrayList<>();
		List<NotaFiscalDTO> registroPlacaNF = new ArrayList<>();
		List<AutoInfracao> empresasVinculadasAutoInfracao = new ArrayList<>();
		List<AcaoFiscal> pendenciasTransitoLivre = new ArrayList<>();

		nfsVinculadasMDFe
				.add(new NotaFiscalDTO(12343234l, "Emitente da nota", "Ceará", "Destinatario da nota", "Ceará"));
		nfsVinculadasMDFePassagemCE
				.add(new NotaFiscalDTO(12343234l, "Emitente da nota", "Ceará", "Destinatario da nota", "Ceará"));
		registroPlacaNF.add(new NotaFiscalDTO(12343234l, "Emitente da nota", "Ceará", "Destinatario da nota", "Ceará"));
		empresasVinculadasAutoInfracao
				.add(new AutoInfracao("234534", "Orgão emitente do auto", new Empresa(), new Diligencia(1236534L)));
		pendenciasTransitoLivre.add(new AcaoFiscal("1234321412", 1999.90));

		dossiePlaca.setVeiculo(new Veiculo("HTT2321", "Juliano Castro", false, false, 289.00));
		dossiePlaca.setRegistroPassagemSitram(new Date());
		dossiePlaca.setEmpresasVinculadasAutoInfracao(empresasVinculadasAutoInfracao);
		dossiePlaca.setNfsVinculadasMDFe(nfsVinculadasMDFePassagemCE);
		dossiePlaca.setNfsVinculadasMDFePassagemCE(nfsVinculadasMDFePassagemCE);
		dossiePlaca.setPendenciasTransitoLivre(pendenciasTransitoLivre);
		dossiePlaca.setRegistroPlacaNF(registroPlacaNF);

		return new ResponseEntity<DossiePlacaWS1>(dossiePlaca, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> buscarVeiculoIPVA(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarNFe24HorasComRegistroSitram(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarNFeCearaSemRegistroSitram(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarRegistroVinculadoAutoInfracao(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieCPFWS1> buscarContribuinte(String param) {
		DossieCPFWS1 dossieCPF = new DossieCPFWS1();
		Endereco endereco = new Endereco("Rua teste do endereco", "123", "casa 1", "Centro", 6000000L, "CE",
				"Fortaleza");
		Contribuinte contribuinte = new Contribuinte("Fulano de Tal", 1231241L, endereco);
		Empresa empresa = new Empresa(123456789000112l, 123456789l, "Razao Social de teste", endereco, "ATIVO",
				"atividade CNAE");
		List<Empresa> participacaoEmpresas = new ArrayList<>();
		List<Empresa> participouEmpresas = new ArrayList<>();
		List<Empresa> destinatarioEmpresas = new ArrayList<>();
		List<Empresa> remetenteEmpresas = new ArrayList<>();

		participacaoEmpresas.add(empresa);
		participouEmpresas.add(empresa);
		destinatarioEmpresas.add(empresa);
		remetenteEmpresas.add(empresa);

		dossieCPF.setContribuinte(contribuinte);
		dossieCPF.setParticipacaoEmpresas(participacaoEmpresas);
		dossieCPF.setParticipouEmpresas(participouEmpresas);

		return new ResponseEntity<DossieCPFWS1>(dossieCPF, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieCPFWS2> buscarNFePorChaveAcesso(List<String> chavesAcesso) {
		DossieCPFWS2 dossieCPF = new DossieCPFWS2();
		Endereco endereco = new Endereco("Rua teste do endereco", "123", "casa 1", "Centro", 6000000L, "CE",
				"Fortaleza");
		Empresa empresa = new Empresa(123456789000112l, 123456789l, "Razao Social de teste", endereco, "ATIVO",
				"atividade CNAE");
		List<Empresa> destinatarioEmpresas = new ArrayList<>();
		List<Empresa> remetenteEmpresas = new ArrayList<>();

		destinatarioEmpresas.add(empresa);
		remetenteEmpresas.add(empresa);

		dossieCPF.setDestinatarioEmpresas(destinatarioEmpresas);
		dossieCPF.setRemetenteEmpresas(remetenteEmpresas);
		dossieCPF.setValorTotalDestinatario(123123.90);
		dossieCPF.setValorTotalRemetente(123123123.89);

		return new ResponseEntity<DossieCPFWS2>(dossieCPF, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieEmpresaWS1> buscarSocios(String param) {
		DossieEmpresaWS1 dossieEmpresa = new DossieEmpresaWS1();

		List<Empresa> participacaoOutrasEmpresas = new ArrayList<>();
		List<Empresa> socioOutrasEmpresas = new ArrayList<>();

		Endereco endereco = new Endereco("Rua teste do endereco", "123", "casa 1", "Centro", 6000000L, "CE",
				"Fortaleza");

		Empresa empresa = new Empresa(123456789000112l, 123456789l, "Razao Social de teste", endereco, "ATIVO",
				"atividade CNAE");
		participacaoOutrasEmpresas.add(empresa);
		socioOutrasEmpresas.add(empresa);

		Set<String> fones = new HashSet<String>();
		fones.add("(85) 2322-7767");
		fones.add("(85) 98878-5389");

		dossieEmpresa.setEmpresa(empresa);
		dossieEmpresa.setContador(new Contador("Contador teste", fones, 89897698L));
		dossieEmpresa.setParticipacaoOutrasEmpresas(participacaoOutrasEmpresas);
		dossieEmpresa.setSocioOutrasEmpresas(socioOutrasEmpresas);

		return new ResponseEntity<DossieEmpresaWS1>(dossieEmpresa, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<List<RegimeTributacao>> buscarRegimeTributacao(String param) {
	

		List<RegimeTributacao> regimeTributacao = new ArrayList<>();
		List<MandadoSeguranca> mandadoSeguranca = new ArrayList<>();

		regimeTributacao.add(new RegimeTributacao(123123, "Assunto regime tributação"));
		mandadoSeguranca.add(new MandadoSeguranca(1231231, "Objeto do mandado"));

		return new ResponseEntity<List<RegimeTributacao>>(regimeTributacao, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieEmpresaWS3> buscarEmpresaPendenciaTransitoLivre(String param) {
		DossieEmpresaWS3 dossieEmpresa = new DossieEmpresaWS3();

		List<TransitoLivre> pendenciaTransitoLivre = new ArrayList<>();
		pendenciaTransitoLivre.add(new TransitoLivre(1235433l, 898765568877l));

		dossieEmpresa.setPendenciaTransitoLivre(pendenciaTransitoLivre);

		return new ResponseEntity<DossieEmpresaWS3>(dossieEmpresa, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieEmpresaWS4> buscarNfeSemRegistro(String param) {
		DossieEmpresaWS4 dossieEmpresa = new DossieEmpresaWS4();
		List<NotaFiscalDTO> nfsSemRegistroSitram = new ArrayList<>();
		nfsSemRegistroSitram.add(
				new NotaFiscalDTO(123123123l, "Emitente da nota fiscal", "CE", "Destinatario da nota fiscal", "CE"));

		dossieEmpresa.setNfsSemRegistroSitram(nfsSemRegistroSitram);

		return new ResponseEntity<DossieEmpresaWS4>(dossieEmpresa, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieEmpresaWS5> buscarAutoInfracao(String param) {
		DossieEmpresaWS5 dossieEmpresa = new DossieEmpresaWS5();
		List<AutoInfracao> autoInfracaoLavrados = new ArrayList<>();

		Endereco endereco = new Endereco("Rua teste do endereco", "123", "casa 1", "Centro", 6000000L, "CE",
				"Fortaleza");

		Empresa empresa = new Empresa(123456789000112l, 123456789l, "Razao Social de teste", endereco, "ATIVO",
				"atividade CNAE");

		autoInfracaoLavrados.add(new AutoInfracao("123234", "342341234", empresa, new Diligencia(12312231231L)));

		dossieEmpresa.setAutoInfracaoLavrados(autoInfracaoLavrados);

		return new ResponseEntity<DossieEmpresaWS5>(dossieEmpresa, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<DossieEmpresaWS6> buscarTransportadoras(String param) {
		List<Empresa> transportadoras = new ArrayList<>();
		DossieEmpresaWS6 dossieEmpresa = new DossieEmpresaWS6();

		Endereco endereco = new Endereco("Rua teste do endereco", "123", "casa 1", "Centro", 6000000L, "CE",
				"Fortaleza");

		Empresa empresa = new Empresa(123456789000112l, 123456789l, "Razao Social de teste", endereco, "ATIVO",
				"atividade CNAE");

		transportadoras.add(empresa);

		dossieEmpresa.setTransportadoras(transportadoras);

		return new ResponseEntity<DossieEmpresaWS6>(dossieEmpresa, HttpStatus.OK);
	}

	@Override
	public ResponseEntity<?> buscarPendenciaTransitoLivre(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

}
