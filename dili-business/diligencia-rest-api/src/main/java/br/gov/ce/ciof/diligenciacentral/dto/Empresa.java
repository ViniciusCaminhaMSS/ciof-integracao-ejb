package br.gov.ce.ciof.diligenciacentral.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;

public class Empresa {

	@ApiModelProperty(notes = "Número do cnpj")
	private Long cnpj;
	@ApiModelProperty(notes = "Número do cgf")
	private Long cgf;
	@ApiModelProperty(notes = "Razão social do contribuinte")
	private String razaoSocial;
	@ApiModelProperty(notes = "Endereço")
	private Endereco endereco;
	@ApiModelProperty(notes = "Situação do contribuinte")
	private String situacaoCadastral;
	@ApiModelProperty(notes = "Numero mais a descrição: 9999-9/99 - Texto")
	private String atividadeCNAE;
	@ApiModelProperty(notes = "Regime de Recolhimento")
	private String regimeRecolhimento;
	@ApiModelProperty(notes = "Contador da empresa")
	private Contador contador;
	@ApiModelProperty(notes = "Socios da empresa")
	private List<Pessoa> socios;

	public Empresa() {
	}

	public Empresa(Long cnpj, Long cgf, String razaoSocial, Endereco endereco, String situacaoCadastral,
			String atividadeCNAE) {
		this.cnpj = cnpj;
		this.cgf = cgf;
		this.razaoSocial = razaoSocial;
		this.endereco = endereco;
		this.situacaoCadastral = situacaoCadastral;
		this.atividadeCNAE = atividadeCNAE;
	}

	public Long getCnpj() {
		return cnpj;
	}

	public void setCnpj(Long cnpj) {
		this.cnpj = cnpj;
	}

	public Long getCgf() {
		return cgf;
	}

	public void setCgf(Long cgf) {
		this.cgf = cgf;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getSituacaoCadastral() {
		return situacaoCadastral;
	}

	public void setSituacaoCadastral(String situacaoCadastral) {
		this.situacaoCadastral = situacaoCadastral;
	}

	public String getAtividadeCNAE() {
		return atividadeCNAE;
	}

	public void setAtividadeCNAE(String atividadeCNAE) {
		this.atividadeCNAE = atividadeCNAE;
	}

	public Contador getContador() {
		return contador;
	}

	public void setContador(Contador contador) {
		this.contador = contador;
	}

	public List<Pessoa> getSocios() {
		return socios;
	}

	public void setSocios(List<Pessoa> socios) {
		this.socios = socios;
	}

}
