package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS2 implements Serializable {

	private static final long serialVersionUID = 6426876017565509129L;

	private List<RegimeTributacao> regimeTributacao;
	private List<MandadoSeguranca> mandadoSeguranca;

	public List<RegimeTributacao> getRegimeTributacao() {
		return regimeTributacao;
	}

	public void setRegimeTributacao(List<RegimeTributacao> regimeTributacao) {
		this.regimeTributacao = regimeTributacao;
	}

	public List<MandadoSeguranca> getMandadoSeguranca() {
		return mandadoSeguranca;
	}

	public void setMandadoSeguranca(List<MandadoSeguranca> mandadoSeguranca) {
		this.mandadoSeguranca = mandadoSeguranca;
	}
}
