package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class Veiculo implements Serializable {

	private static final long serialVersionUID = 6912098473554715529L;

	private String placa;
	private String proprietario;
	private boolean debitoIPVA = false;
	private boolean isRoubado = false;
	private Double totalDebitos;

	public Veiculo() {
	}

	public Veiculo(String placa, String proprietario, boolean debitoIPVA, boolean isRoubado, Double totalDebitos) {
		this.placa = placa;
		this.proprietario = proprietario;
		this.debitoIPVA = debitoIPVA;
		this.isRoubado = isRoubado;
		this.totalDebitos = totalDebitos;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getProprietario() {
		return proprietario;
	}

	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}

	public boolean isDebitoIPVA() {
		return debitoIPVA;
	}

	public void setDebitoIPVA(boolean debitoIPVA) {
		this.debitoIPVA = debitoIPVA;
	}

	public boolean isRoubado() {
		return isRoubado;
	}

	public void setRoubado(boolean isRoubado) {
		this.isRoubado = isRoubado;
	}

	public Double getTotalDebitos() {
		return totalDebitos;
	}

	public void setTotalDebitos(Double totalDebitos) {
		this.totalDebitos = totalDebitos;
	}

}
