package br.gov.ce.ciof.diligenciacentral.dto;

public class RegimeTributacao {

	private Integer termoAcordo;
	private String assunto;

	public RegimeTributacao() {

	}

	public RegimeTributacao(Integer termoAcordo, String assunto) {
		this.termoAcordo = termoAcordo;
		this.assunto = assunto;
	}

	public Integer getTermoAcordo() {
		return termoAcordo;
	}

	public void setTermoAcordo(Integer termoAcordo) {
		this.termoAcordo = termoAcordo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

}
