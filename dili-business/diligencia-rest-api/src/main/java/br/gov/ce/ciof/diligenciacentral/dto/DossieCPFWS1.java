package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieCPFWS1 implements Serializable {

	private static final long serialVersionUID = -1180759587635376425L;

	private Contribuinte contribuinte;
	private List<Empresa> participacaoEmpresas;
	private List<Empresa> participouEmpresas;

	public Contribuinte getContribuinte() {
		return contribuinte;
	}

	public void setContribuinte(Contribuinte contribuinte) {
		this.contribuinte = contribuinte;
	}

	public List<Empresa> getParticipacaoEmpresas() {
		return participacaoEmpresas;
	}

	public void setParticipacaoEmpresas(List<Empresa> participacaoEmpresas) {
		this.participacaoEmpresas = participacaoEmpresas;
	}

	public List<Empresa> getParticipouEmpresas() {
		return participouEmpresas;
	}

	public void setParticipouEmpresas(List<Empresa> participouEmpresas) {
		this.participouEmpresas = participouEmpresas;
	}

}
