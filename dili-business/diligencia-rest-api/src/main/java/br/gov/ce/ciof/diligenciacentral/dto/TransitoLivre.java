package br.gov.ce.ciof.diligenciacentral.dto;

public class TransitoLivre {

	private Long numeroNFeChaveAcesso;
	private Long numeroAcaoFiscal;
	private Double valorAcaoFiscal;

	public TransitoLivre() {

	}

	public TransitoLivre(Long numeroNFeChaveAcesso, Long numeroAcaoFiscal) {
		this.numeroNFeChaveAcesso = numeroNFeChaveAcesso;
		this.numeroAcaoFiscal = numeroAcaoFiscal;
	}

	public Long getNumeroNFeChaveAcesso() {
		return numeroNFeChaveAcesso;
	}

	public void setNumeroNFeChaveAcesso(Long numeroNFeChaveAcesso) {
		this.numeroNFeChaveAcesso = numeroNFeChaveAcesso;
	}

	public Long getNumeroAcaoFiscal() {
		return numeroAcaoFiscal;
	}

	public void setNumeroAcaoFiscal(Long numeroAcaoFiscal) {
		this.numeroAcaoFiscal = numeroAcaoFiscal;
	}

	public Double getValorAcaoFiscal() {
		return valorAcaoFiscal;
	}

	public void setValorAcaoFiscal(Double valorAcaoFiscal) {
		this.valorAcaoFiscal = valorAcaoFiscal;
	}

}
