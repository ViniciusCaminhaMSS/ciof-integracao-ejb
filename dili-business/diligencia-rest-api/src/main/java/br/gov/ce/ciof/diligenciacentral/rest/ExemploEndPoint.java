package br.gov.ce.ciof.diligenciacentral.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.gov.ce.ciof.diligenciacentral.dto.NotaFiscalDTO;
import br.gov.ce.ciof.diligenciacentral.util.CustomMsgType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Diligência Central")
public interface ExemploEndPoint {

	@ApiOperation(value = "Busca empresa de acordo com os parametro informado.", response = NotaFiscalDTO.class, notes = "Realiza busca de notas importantes")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retorna notas no formato JSON.", response = NotaFiscalDTO.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/buscarNotasImportantes", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNotasImportantes();
}
