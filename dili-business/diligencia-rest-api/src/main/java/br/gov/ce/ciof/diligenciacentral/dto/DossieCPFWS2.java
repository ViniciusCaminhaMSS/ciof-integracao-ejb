package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieCPFWS2 implements Serializable {

	private static final long serialVersionUID = 5238592557564579999L;

	private List<Empresa> destinatarioEmpresas;
	private List<Empresa> remetenteEmpresas;
	private Double valorTotalDestinatario;
	private Double valorTotalRemetente;

	public List<Empresa> getDestinatarioEmpresas() {
		return destinatarioEmpresas;
	}

	public void setDestinatarioEmpresas(List<Empresa> destinatarioEmpresas) {
		this.destinatarioEmpresas = destinatarioEmpresas;
	}

	public List<Empresa> getRemetenteEmpresas() {
		return remetenteEmpresas;
	}

	public void setRemetenteEmpresas(List<Empresa> remetenteEmpresas) {
		this.remetenteEmpresas = remetenteEmpresas;
	}

	public Double getValorTotalDestinatario() {
		return valorTotalDestinatario;
	}

	public void setValorTotalDestinatario(Double valorTotalDestinatario) {
		this.valorTotalDestinatario = valorTotalDestinatario;
	}

	public Double getValorTotalRemetente() {
		return valorTotalRemetente;
	}

	public void setValorTotalRemetente(Double valorTotalRemetente) {
		this.valorTotalRemetente = valorTotalRemetente;
	}
}
