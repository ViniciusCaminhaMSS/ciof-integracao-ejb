package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class AutoInfracao implements Serializable {

	private static final long serialVersionUID = -7717010030261324909L;

	private String numero;
	private String nomeOrgaoAtuante;
	private Empresa empresa;
	private Diligencia diligencia;

	public AutoInfracao(String numero, String nomeOrgaoAtuante, Empresa empresa, Diligencia diligencia) {
		this.numero = numero;
		this.nomeOrgaoAtuante = nomeOrgaoAtuante;
		this.empresa = empresa;
		this.diligencia = diligencia;
	}

	public AutoInfracao() {
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNomeOrgaoAtuante() {
		return nomeOrgaoAtuante;
	}

	public void setNomeOrgaoAtuante(String nomeOrgaoAtuante) {
		this.nomeOrgaoAtuante = nomeOrgaoAtuante;
	}

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Diligencia getDiligencia() {
		return diligencia;
	}

	public void setDiligencia(Diligencia diligencia) {
		this.diligencia = diligencia;
	}

}
