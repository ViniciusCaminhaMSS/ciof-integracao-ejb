package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class AcaoFiscal implements Serializable {

	private static final long serialVersionUID = -7708983104255064509L;

	private String numero;
	private Double valorTotal;

	public AcaoFiscal(String numero, Double valorTotal) {
		this.numero = numero;
		this.valorTotal = valorTotal;
	}

	public AcaoFiscal() {
		// TODO Auto-generated constructor stub
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
}
