package br.gov.ce.ciof.diligenciacentral;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DiligenciaCentralApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiligenciaCentralApplication.class, args);
	}
}
