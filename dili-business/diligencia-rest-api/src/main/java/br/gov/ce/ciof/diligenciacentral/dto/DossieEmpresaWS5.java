package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS5 implements Serializable {

	private static final long serialVersionUID = -8585113813067933375L;

	private List<AutoInfracao> autoInfracaoLavrados;

	public List<AutoInfracao> getAutoInfracaoLavrados() {
		return autoInfracaoLavrados;
	}

	public void setAutoInfracaoLavrados(List<AutoInfracao> autoInfracaoLavrados) {
		this.autoInfracaoLavrados = autoInfracaoLavrados;
	}
}
