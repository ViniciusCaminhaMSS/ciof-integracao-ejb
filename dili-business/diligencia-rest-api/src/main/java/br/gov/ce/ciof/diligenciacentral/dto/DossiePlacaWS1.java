package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class DossiePlacaWS1 implements Serializable {

	private static final long serialVersionUID = -191919627587396758L;

	private Veiculo veiculo;
	private Date registroPassagemSitram;
	private List<NotaFiscalDTO> nfsVinculadasMDFe;
	private List<NotaFiscalDTO> nfsVinculadasMDFePassagemCE;
	private List<NotaFiscalDTO> registroPlacaNF;
	private List<AutoInfracao> empresasVinculadasAutoInfracao;
	private List<AcaoFiscal> pendenciasTransitoLivre;

	public Veiculo getVeiculo() {
		return veiculo;
	}

	public void setVeiculo(Veiculo veiculo) {
		this.veiculo = veiculo;
	}

	public Date getRegistroPassagemSitram() {
		return registroPassagemSitram;
	}

	public void setRegistroPassagemSitram(Date registroPassagemSitram) {
		this.registroPassagemSitram = registroPassagemSitram;
	}

	public List<NotaFiscalDTO> getNfsVinculadasMDFe() {
		return nfsVinculadasMDFe;
	}

	public void setNfsVinculadasMDFe(List<NotaFiscalDTO> nfsVinculadasMDFe) {
		this.nfsVinculadasMDFe = nfsVinculadasMDFe;
	}

	public List<NotaFiscalDTO> getNfsVinculadasMDFePassagemCE() {
		return nfsVinculadasMDFePassagemCE;
	}

	public void setNfsVinculadasMDFePassagemCE(List<NotaFiscalDTO> nfsVinculadasMDFePassagemCE) {
		this.nfsVinculadasMDFePassagemCE = nfsVinculadasMDFePassagemCE;
	}

	public List<NotaFiscalDTO> getRegistroPlacaNF() {
		return registroPlacaNF;
	}

	public void setRegistroPlacaNF(List<NotaFiscalDTO> registroPlacaNF) {
		this.registroPlacaNF = registroPlacaNF;
	}

	public List<AutoInfracao> getEmpresasVinculadasAutoInfracao() {
		return empresasVinculadasAutoInfracao;
	}

	public void setEmpresasVinculadasAutoInfracao(List<AutoInfracao> empresasVinculadasAutoInfracao) {
		this.empresasVinculadasAutoInfracao = empresasVinculadasAutoInfracao;
	}

	public List<AcaoFiscal> getPendenciasTransitoLivre() {
		return pendenciasTransitoLivre;
	}

	public void setPendenciasTransitoLivre(List<AcaoFiscal> pendenciasTransitoLivre) {
		this.pendenciasTransitoLivre = pendenciasTransitoLivre;
	}

}
