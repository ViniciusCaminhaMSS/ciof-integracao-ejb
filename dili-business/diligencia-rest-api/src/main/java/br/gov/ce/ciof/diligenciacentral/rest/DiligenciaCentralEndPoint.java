package br.gov.ce.ciof.diligenciacentral.rest;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS2;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS2;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS3;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS4;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS5;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS6;
import br.gov.ce.ciof.diligenciacentral.dto.DossiePlacaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.Empresa;
import br.gov.ce.ciof.diligenciacentral.dto.NotaFiscalDTO;
import br.gov.ce.ciof.diligenciacentral.dto.RegimeTributacao;
import br.gov.ce.ciof.diligenciacentral.util.CustomMsgType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Api(value = "Diligência Central")
public interface DiligenciaCentralEndPoint {

	@ApiOperation(value = "Busca empresa de acordo com os parametro informado.", response = Empresa.class, notes = "Realiza busca por placa, cgf, cnpj")
	@ApiResponses({ @ApiResponse(code = 200, message = "Retorna empresas no formato JSON.", response = Empresa.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/buscarEmpresa", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<Empresa>> buscarEmpresa(
			@ApiParam(value = "cgf", required = false) @RequestParam(value = "cgf", defaultValue = "") String cgf,
			@ApiParam(value = "cnpj", required = false) @RequestParam(value = "cnpj", defaultValue = "") String cnpj,
			@ApiParam(value = "razaoSocial", required = false) @RequestParam(value = "razaoSocial", defaultValue = "") String razaoSocial,
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa,
			@ApiParam(value = "chaveAcesso", required = false) @RequestParam(value = "chaveAcesso", defaultValue = "") String chaveAcesso,
			@ApiParam(value = "nf", required = false) @RequestParam(value = "nf", defaultValue = "") String nf);

	@ApiOperation(value = "Busca uma nota fiscal.", response = Empresa.class, notes = "Realiza busca por nfe e chave de acesso")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações da nota fiscal no formato JSON.", response = NotaFiscalDTO.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/buscarNfeOuChaveAcesso", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<NotaFiscalDTO>> buscarPorNFeOuChaveAcesso(
			@ApiParam(value = "chaveAcesso", required = false) @RequestParam(value = "chaveAcesso", defaultValue = "") String chaveAcesso,
			@ApiParam(value = "numeroNfe", required = false) @RequestParam(value = "numeroNfe", defaultValue = "") String numeroNfe);

	@ApiOperation(value = "Busca informações da placa para o dossiê por placa.", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarPlaca", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossiePlacaWS1> buscarPlaca(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Busca informações do veículo para montar o dossiê por placa.", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarVeiculoIPVA", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarVeiculoIPVA(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Busca informações da nota fiscal das ultimas 24Hs com registro no SIRAM.", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarNFe24PassagemSitram", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNFe24HorasComRegistroSitram(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Busca informações da nota fiscal quando o Ceará não possui registro no SITRAM.", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarNFeSemPassagemSitram", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarNFeCearaSemRegistroSitram(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Busca informações vinculado ao auto-infração ", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarRegistroVinculadoAutoInfracao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarRegistroVinculadoAutoInfracao(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Busca informações com pendencia de transito Livre.", response = Empresa.class, notes = "Serviço que retorna parte das informações do dossiê por placa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por placa no formato JSON.", response = DossiePlacaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossiePlaca/buscarPendenciaTransitoLivre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> buscarPendenciaTransitoLivre(
			@ApiParam(value = "placa", required = false) @RequestParam(value = "placa", defaultValue = "") String placa);

	@ApiOperation(value = "Gera um dossiê por CPF. Preenche grid com Contribuite e Empresa que participa e participou.", response = DossieCPFWS1.class, notes = "Realiza busca para gerar um dossiê por CPF.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieCPFWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "dossieCPF/buscarContribuinte", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieCPFWS1> buscarContribuinte(
			@ApiParam(value = "cpf", required = false) @RequestParam(value = "cpf", defaultValue = "") String cpf);

	@ApiOperation(value = "Gera um dossiê por CPF. Preenche grind com empresa destinatária, remetente e suas respequitivas despesas", response = DossieCPFWS1.class, notes = "Realiza busca para gerar um dossiê por CPF.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieCPFWS2.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieCPF/buscarIdentificadoNFe", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieCPFWS2> buscarNFePorChaveAcesso(
			@ApiParam(value = "chaveAcesso", required = false) @RequestParam(value = "chaveAcesso", defaultValue = "") List<String> chavesAcesso);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com empresa, socios, participação em outras emrpesas.", response = Empresa.class, notes = "Realiza busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieEmpresaWS1.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarSocios", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieEmpresaWS1> buscarSocios(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com regime de tributação e mandado de segurança.", response = Empresa.class, notes = "Realiza busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieEmpresaWS2.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarRegimeTributacao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<RegimeTributacao>> buscarRegimeTributacao(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com as pendências de trânsito livre", response = Empresa.class, notes = "Realiza  busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieEmpresaWS3.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarPendenciaTransitoLivre", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieEmpresaWS3> buscarEmpresaPendenciaTransitoLivre(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com NFe sem registro no SITRAM.", response = Empresa.class, notes = "Realiza busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieEmpresaWS4.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarNfeSemRegistro", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieEmpresaWS4> buscarNfeSemRegistro(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com auto de infração lavrado.", response = Empresa.class, notes = "Realiza busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquis por CPF no formato JSON.", response = DossieEmpresaWS5.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarAutoInfracao", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieEmpresaWS5> buscarAutoInfracao(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

	@ApiOperation(value = "Gera um dossiê por Empresa. Preenche grid com as transportadoras utilizadas.", response = Empresa.class, notes = "Realiza  busca para gerar um dossiê por Empresa.")
	@ApiResponses({
			@ApiResponse(code = 200, message = "Retorna informações do dossiê pesquisa por CPF no formato JSON.", response = DossieEmpresaWS6.class),
			@ApiResponse(code = 404, message = "Nenhuma informação localizada.", response = CustomMsgType.class),
			@ApiResponse(code = 500, message = "Servidor não responde.", response = CustomMsgType.class) })
	@RequestMapping(value = "/dossieEmpresa/buscarTransportadoras", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<DossieEmpresaWS6> buscarTransportadoras(
			@ApiParam(value = "param", required = false) @RequestParam(value = "param", defaultValue = "") String param);

}
