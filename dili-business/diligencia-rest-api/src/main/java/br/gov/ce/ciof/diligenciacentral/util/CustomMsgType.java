package br.gov.ce.ciof.diligenciacentral.util;

public class CustomMsgType {
	private String mensagem;

	public CustomMsgType(String mensagem) {
		this.mensagem = mensagem;
	}

	public String getMensagem() {
		return mensagem;
	}
}
