package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class DossiePlacaWS2 implements Serializable {

	private static final long serialVersionUID = -8026466606358118234L;

	private Empresa empresa;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}
}
