package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class DTCES02 implements Serializable {

	private static final long serialVersionUID = 1L;
	private Boolean veiculoRoubado;
	private String uf;

	public Boolean getVeiculoRoubado() {
		return veiculoRoubado;
	}

	public void setVeiculoRoubado(Boolean veiculoRoubado) {
		this.veiculoRoubado = veiculoRoubado;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

}
