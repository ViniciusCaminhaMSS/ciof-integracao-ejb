package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class CDTRS16 implements Serializable {

	private static final long serialVersionUID = 8521459425012155479L;

	private List<Empresa> empresaDestinatario;
	private List<Empresa> empresaRemetente;

	public List<Empresa> getEmpresaDestinatario() {
		return empresaDestinatario;
	}

	public void setEmpresaDestinatario(List<Empresa> empresaDestinatario) {
		this.empresaDestinatario = empresaDestinatario;
	}

	public List<Empresa> getEmpresaRemetente() {
		return empresaRemetente;
	}

	public void setEmpresaRemetente(List<Empresa> empresaRemetente) {
		this.empresaRemetente = empresaRemetente;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
