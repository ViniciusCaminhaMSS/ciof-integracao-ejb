package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS6 implements Serializable {

	private static final long serialVersionUID = -3688353278664411232L;

	private List<Empresa> transportadoras;

	public List<Empresa> getTransportadoras() {
		return transportadoras;
	}

	public void setTransportadoras(List<Empresa> transportadoras) {
		this.transportadoras = transportadoras;
	}

}
