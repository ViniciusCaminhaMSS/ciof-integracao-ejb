package br.gov.ce.ciof.diligenciacentral.dto;

public class MandadoSeguranca {

	private Integer numeroProcesso;
	private Object objeto;

	public MandadoSeguranca() {

	}

	public MandadoSeguranca(Integer numeroProcesso, Object objeto) {
		this.numeroProcesso = numeroProcesso;
		this.objeto = objeto;
	}

	public Integer getNumeroProcesso() {
		return numeroProcesso;
	}

	public void setNumeroProcesso(Integer numeroProcesso) {
		this.numeroProcesso = numeroProcesso;
	}

	public Object getObjeto() {
		return objeto;
	}

	public void setObjeto(Object objeto) {
		this.objeto = objeto;
	}

}
