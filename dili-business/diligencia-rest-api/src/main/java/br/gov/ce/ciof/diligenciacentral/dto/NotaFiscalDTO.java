package br.gov.ce.ciof.diligenciacentral.dto;

public class NotaFiscalDTO {

	private Long numeroChaveAcesso;
	private String emitente;
	private String ufEmitente;
	private String destinatario;
	private String ufDestinatario;

	public NotaFiscalDTO() {
	}

	public NotaFiscalDTO(Long numeroChaveAcesso, String emitente, String ufEmitente, String destinatario,
			String ufDestinatario) {
		this.numeroChaveAcesso = numeroChaveAcesso;
		this.emitente = emitente;
		this.ufEmitente = ufEmitente;
		this.destinatario = destinatario;
		this.ufDestinatario = ufDestinatario;
	}

	public Long getNumeroChaveAcesso() {
		return numeroChaveAcesso;
	}

	public void setNumeroChaveAcesso(Long numeroChaveAcesso) {
		this.numeroChaveAcesso = numeroChaveAcesso;
	}

	public String getEmitente() {
		return emitente;
	}

	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}

	public String getUfEmitente() {
		return ufEmitente;
	}

	public void setUfEmitente(String ufEmitente) {
		this.ufEmitente = ufEmitente;
	}

	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	public String getUfDestinatario() {
		return ufDestinatario;
	}

	public void setUfDestinatario(String ufDestinatario) {
		this.ufDestinatario = ufDestinatario;
	}

}
