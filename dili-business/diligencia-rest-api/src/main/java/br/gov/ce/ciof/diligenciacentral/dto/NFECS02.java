package br.gov.ce.ciof.diligenciacentral.dto;

import java.util.Set;

public class NFECS02 {

	private String nome;
	private Set<String> telefones;
	private Long cpf;

	public NFECS02() {
	}

	public NFECS02(String nome, Set<String> telefones, Long cpf) {
		this.nome = nome;
		this.telefones = telefones;
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<String> getTelefones() {
		return telefones;
	}

	public void setTelefones(Set<String> telefones) {
		this.telefones = telefones;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

}
