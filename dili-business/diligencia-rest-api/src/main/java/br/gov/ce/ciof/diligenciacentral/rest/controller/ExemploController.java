package br.gov.ce.ciof.diligenciacentral.rest.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.ciof.diligenciacentral.dto.NotaFiscalDTO;
import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;
import br.gov.ce.ciof.diligenciacentral.rest.ExemploEndPoint;
import br.gov.ce.ciof.diligenciacentral.service.GestaoFiscalService;

@RestController
@RequestMapping("/exemplo")
public class ExemploController implements ExemploEndPoint {

	@Autowired
	private ModelMapper modelMapper;

	@Autowired
	private GestaoFiscalService gestaoFiscalService;

	@Override
	public ResponseEntity<?> buscarNotasImportantes() {
		List<NotaFiscal> notas = gestaoFiscalService.listNotasImportante();

		// Mapeando entidade para o DTO
		List<NotaFiscalDTO> resultado = notas.stream().map(nota -> modelMapper.map(nota, NotaFiscalDTO.class))
				.collect(Collectors.toList());

		return new ResponseEntity<List<NotaFiscalDTO>>(resultado, HttpStatus.OK);
	}

}
