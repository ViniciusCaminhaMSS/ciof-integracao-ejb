package br.gov.ce.ciof.diligenciacentral.rest.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieCPFWS2;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS3;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS4;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS5;
import br.gov.ce.ciof.diligenciacentral.dto.DossieEmpresaWS6;
import br.gov.ce.ciof.diligenciacentral.dto.DossiePlacaWS1;
import br.gov.ce.ciof.diligenciacentral.dto.Empresa;
import br.gov.ce.ciof.diligenciacentral.dto.NotaFiscalDTO;
import br.gov.ce.ciof.diligenciacentral.dto.RegimeTributacao;
import br.gov.ce.ciof.diligenciacentral.rest.DiligenciaCentralEndPoint;

@RestController
@RequestMapping("/diligencia-central")
public class DiligenciaCentralController implements DiligenciaCentralEndPoint {

	@Override
	public ResponseEntity<?> buscarPendenciaTransitoLivre(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<Empresa>> buscarEmpresa(String cgf, String cnpj, String razaoSocial, String placa,
			String chaveAcesso, String nf) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<NotaFiscalDTO>> buscarPorNFeOuChaveAcesso(String chaveAcesso, String numeroNfe) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossiePlacaWS1> buscarPlaca(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarVeiculoIPVA(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarNFe24HorasComRegistroSitram(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarNFeCearaSemRegistroSitram(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<?> buscarRegistroVinculadoAutoInfracao(String placa) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieCPFWS1> buscarContribuinte(String cpf) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieCPFWS2> buscarNFePorChaveAcesso(List<String> chavesAcesso) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieEmpresaWS1> buscarSocios(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<List<RegimeTributacao>> buscarRegimeTributacao(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieEmpresaWS3> buscarEmpresaPendenciaTransitoLivre(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieEmpresaWS4> buscarNfeSemRegistro(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieEmpresaWS5> buscarAutoInfracao(String param) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ResponseEntity<DossieEmpresaWS6> buscarTransportadoras(String param) {
		// TODO Auto-generated method stub
		return null;
	}

}
