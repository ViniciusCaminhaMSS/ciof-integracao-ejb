package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS4 implements Serializable {

	private static final long serialVersionUID = 237349044581699691L;

	private List<NotaFiscalDTO> nfsSemRegistroSitram;

	public List<NotaFiscalDTO> getNfsSemRegistroSitram() {
		return nfsSemRegistroSitram;
	}

	public void setNfsSemRegistroSitram(List<NotaFiscalDTO> nfsSemRegistroSitram) {
		this.nfsSemRegistroSitram = nfsSemRegistroSitram;
	}
}
