package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS1 implements Serializable {

	private static final long serialVersionUID = 8521459425012155479L;

	private Empresa empresa;
	private Contador contador;
	private List<Empresa> participacaoOutrasEmpresas;
	private List<Empresa> socioOutrasEmpresas;

	public Empresa getEmpresa() {
		return empresa;
	}

	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	public Contador getContador() {
		return contador;
	}

	public void setContador(Contador contador) {
		this.contador = contador;
	}

	public List<Empresa> getParticipacaoOutrasEmpresas() {
		return participacaoOutrasEmpresas;
	}

	public void setParticipacaoOutrasEmpresas(List<Empresa> participacaoOutrasEmpresas) {
		this.participacaoOutrasEmpresas = participacaoOutrasEmpresas;
	}

	public List<Empresa> getSocioOutrasEmpresas() {
		return socioOutrasEmpresas;
	}

	public void setSocioOutrasEmpresas(List<Empresa> socioOutrasEmpresas) {
		this.socioOutrasEmpresas = socioOutrasEmpresas;
	}
}
