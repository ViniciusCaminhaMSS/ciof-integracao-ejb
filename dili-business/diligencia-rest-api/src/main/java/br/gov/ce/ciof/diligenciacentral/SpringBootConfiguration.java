package br.gov.ce.ciof.diligenciacentral;

import java.util.Arrays;
import java.util.Collections;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.builders.ResponseMessageBuilder;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ResponseMessage;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SpringBootConfiguration extends WebMvcConfigurationSupport {

	private final ResponseMessage MSG_201 = simpleMessage(201, "Recurso criado");
	private final ResponseMessage MSG_204 = simpleMessage(204, "Operação realizada com sucesso.");
	private final ResponseMessage MSG_403 = simpleMessage(403, "Não autorizado.");
	private final ResponseMessage MSG_404 = simpleMessage(404, "Nenhuma informação localizada");
	private final ResponseMessage MSG_422 = simpleMessage(422, "Erro de validação");
	private final ResponseMessage MSG_500 = simpleMessage(500, "Erro inesperado");
	
	@Bean
	public ModelMapper modelMapper() {
	    return new ModelMapper();
	}

	@Bean
	public Docket productApi() {

		return new Docket(DocumentationType.SWAGGER_2).groupName("Diligencia-Central-Api")
				.useDefaultResponseMessages(false)
				.globalResponseMessage(RequestMethod.GET, Arrays.asList(MSG_403, MSG_404, MSG_500))
				.globalResponseMessage(RequestMethod.POST, Arrays.asList(MSG_201, MSG_403, MSG_422, MSG_500))
				.globalResponseMessage(RequestMethod.PUT, Arrays.asList(MSG_204, MSG_403, MSG_404, MSG_422, MSG_500))
				.globalResponseMessage(RequestMethod.DELETE, Arrays.asList(MSG_204, MSG_403, MSG_404, MSG_500)).select()
				.apis(RequestHandlerSelectors.basePackage("br.gov.ce.ciof.diligenciacentral"))
				.paths(PathSelectors.any()).build().apiInfo(this.apiInfo());

	}

	private ApiInfo apiInfo() {
		return new ApiInfo("API REST para o Diligência Central",
				"API para integrações do Diligência central no projeto CIOF", "Versão 1.0",
				"http://www.sefaz.ce.gov.br", null, null, null, Collections.emptyList());
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/api/v2/api-docs", "/v2/api-docs");
		registry.addRedirectViewController("/api/swagger-resources/configuration/ui",
				"/swagger-resources/configuration/ui");
		registry.addRedirectViewController("/api/swagger-resources/configuration/security",
				"/swagger-resources/configuration/security");
		registry.addRedirectViewController("/api/swagger-resources", "/swagger-resources");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/swagger-ui.html**")
				.addResourceLocations("classpath:/META-INF/resources/swagger-ui.html");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	private ResponseMessage simpleMessage(int code, String msg) {
		return new ResponseMessageBuilder().code(code).message(msg).build();
	}
}
