package br.gov.ce.ciof.diligenciacentral.dto;

public class Contribuinte {
	private String nome;
	private Long cpf;
	private Endereco endereco;

	public Contribuinte() {

	}

	public Contribuinte(String nome, Long cpf, Endereco endereco) {
		this.nome = nome;
		this.cpf = cpf;
		this.endereco = endereco;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Long getCpf() {
		return cpf;
	}

	public void setCpf(Long cpf) {
		this.cpf = cpf;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
}
