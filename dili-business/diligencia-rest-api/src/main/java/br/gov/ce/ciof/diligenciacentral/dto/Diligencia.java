package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;

public class Diligencia implements Serializable {

	private static final long serialVersionUID = -9019122266594076336L;

	private Long numero;

	public Diligencia(Long numero) {
		this.numero = numero;
	}

	public Diligencia() {

	}

	public Long getNumero() {
		return numero;
	}

	public void setNumero(Long numero) {
		this.numero = numero;
	}
}
