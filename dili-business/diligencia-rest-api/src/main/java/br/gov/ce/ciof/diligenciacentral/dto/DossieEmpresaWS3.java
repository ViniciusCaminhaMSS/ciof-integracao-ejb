package br.gov.ce.ciof.diligenciacentral.dto;

import java.io.Serializable;
import java.util.List;

public class DossieEmpresaWS3 implements Serializable {

	private static final long serialVersionUID = 253576729203954156L;

	private List<TransitoLivre> pendenciaTransitoLivre;

	public List<TransitoLivre> getPendenciaTransitoLivre() {
		return pendenciaTransitoLivre;
	}

	public void setPendenciaTransitoLivre(List<TransitoLivre> pendenciaTransitoLivre) {
		this.pendenciaTransitoLivre = pendenciaTransitoLivre;
	}

}
