package br.gov.ce.ciof.diligenciacentral.model;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;

@RunWith(SpringRunner.class)
@DataJpaTest
public class NotaFiscalBoImplTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private NotaFiscalBo notaFiscalBo;

	@Before
	public void setUp() throws Exception {

		entityManager.persist(new NotaFiscal(1l, 32l, "PETROBRAS", "RJ", "POSTO BR", "CE"));
		entityManager.persist(new NotaFiscal(2l, 35l, "PETROBRAS", "RJ", "POSTO BR", "CE"));
		entityManager.flush();
	}

	@Test
	public void test() {
		List<NotaFiscal> notas = notaFiscalBo.findNotasIntervaloNumeracao(30l, 40l);
		assert (notas.size() == 2);

		long chave = notas.get(0).getNumeroChaveAcesso();
		assert (chave > 30 && chave < 40);

		chave = notas.get(1).getNumeroChaveAcesso();
		assert (chave > 30 && chave < 40);
	}

}
