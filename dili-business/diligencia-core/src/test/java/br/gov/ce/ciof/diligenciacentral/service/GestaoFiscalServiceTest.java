package br.gov.ce.ciof.diligenciacentral.service;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import br.gov.ce.ciof.diligenciacentral.model.NotaFiscalBo;
import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;

@RunWith(SpringRunner.class)
public class GestaoFiscalServiceTest {

	@TestConfiguration
	static class GestaoFiscalServiceTestContextConfiguration {

		@Bean
		public GestaoFiscalService gestaoFiscalService() {
			return new GestaoFiscalService();
		}
	}

	@Autowired
	private GestaoFiscalService gestaoFiscalService;

	@MockBean
	private NotaFiscalBo notaFiscalBo;

	@Before
	public void setUp() throws Exception {

		List<NotaFiscal> list = new ArrayList<>();
		list.add(new NotaFiscal(1l, 12l, "PETROBRAS", "RJ", "POSTO BR", "CE"));
		list.add(new NotaFiscal(2l, 15l, "PETROBRAS", "RJ", "POSTO BR", "CE"));
		Mockito.when(notaFiscalBo.findNotasIntervaloNumeracao(10l, 20l)).thenReturn(list);
	}

	@Test
	public void listNotasImportante() {
		List<NotaFiscal> listNotasImportante = gestaoFiscalService.listNotasImportante();
		
		assert(listNotasImportante.size() == 2);
		
		long chave  = listNotasImportante.get(0).getNumeroChaveAcesso(); 
		assert(chave > 10 && chave <20);
		
		chave  = listNotasImportante.get(1).getNumeroChaveAcesso(); 
		assert(chave > 10 && chave <20);
		
	}

}
