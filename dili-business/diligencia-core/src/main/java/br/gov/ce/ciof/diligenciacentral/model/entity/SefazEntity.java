package br.gov.ce.ciof.diligenciacentral.model.entity;

import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

@MappedSuperclass
public abstract class SefazEntity {

	/*
	 * Version da entidade para controle de concorrencia otimista do Hibernate
	 */
	private Long version;


	@Version
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}
}