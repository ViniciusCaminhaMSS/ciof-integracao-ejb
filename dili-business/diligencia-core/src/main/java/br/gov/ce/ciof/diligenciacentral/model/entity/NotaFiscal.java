package br.gov.ce.ciof.diligenciacentral.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.envers.Audited;

@Audited
@Entity
@Table(name = "NOTA_FISCAL")
public class NotaFiscal extends SefazEntity {

	private Long id;

	private Long numeroChaveAcesso;

	private String emitente;

	private String ufEmitente;
	
	private String destinatario;
	
	private String ufDestinatario;
	
	

	public NotaFiscal(Long id, Long numeroChaveAcesso, String emitente, String ufEmitente, String destinatario, String ufDestinatario) {
		super();
		this.id = id;
		this.numeroChaveAcesso = numeroChaveAcesso;
		this.emitente = emitente;
		this.ufEmitente = ufEmitente;
		this.destinatario = destinatario;
		this.ufDestinatario = ufDestinatario;
	}

	public NotaFiscal() {
		super();
	}



	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@NotNull
	@Column(name = "COD_CHAVE_ACESSO")
	@Digits(fraction = 0, integer = 15)
	public Long getNumeroChaveAcesso() {
		return numeroChaveAcesso;
	}

	public void setNumeroChaveAcesso(Long numeroChaveAcesso) {
		this.numeroChaveAcesso = numeroChaveAcesso;
	}

	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "DSC_EMITENTE")
	public String getEmitente() {
		return emitente;
	}

	public void setEmitente(String emitente) {
		this.emitente = emitente;
	}

	@NotNull
	@Size(min = 2, max = 2)
	@Column(name = "DSC_UF_EMITENTE")
	public String getUfEmitente() {
		return ufEmitente;
	}

	public void setUfEmitente(String ufEmitente) {
		this.ufEmitente = ufEmitente;
	}
	
	
	@NotNull
	@Size(min = 1, max = 60)
	@Column(name = "DSC_DESTINATARIO")
	public String getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(String destinatario) {
		this.destinatario = destinatario;
	}

	@NotNull
	@Size(min = 2, max = 2)
	@Column(name = "DSC_UF_DESTINATARIO")
	public String getUfDestinatario() {
		return ufDestinatario;
	}

	public void setUfDestinatario(String ufDestinatario) {
		this.ufDestinatario = ufDestinatario;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
