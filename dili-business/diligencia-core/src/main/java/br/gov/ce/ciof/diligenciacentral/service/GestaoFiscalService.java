package br.gov.ce.ciof.diligenciacentral.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.gov.ce.ciof.diligenciacentral.model.NotaFiscalBo;
import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;

@Service
public class GestaoFiscalService {

	@Autowired
	private NotaFiscalBo notaFiscalBo;

	public List<NotaFiscal> listNotasImportante() {

		return notaFiscalBo.findNotasIntervaloNumeracao(10l, 20l);
	}
}
