package br.gov.ce.ciof.diligenciacentral.model;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.ciof.diligenciacentral.model.entity.Empresa;

public interface EmpresaBo extends JpaRepository<Empresa, Long> {

}
