package br.gov.ce.ciof.diligenciacentral.model;

import org.springframework.data.jpa.repository.JpaRepository;

import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;
import br.gov.ce.ciof.diligenciacentral.model.repository.NotaFiscalRepository;

public interface NotaFiscalBo extends NotaFiscalRepository, JpaRepository<NotaFiscal, Long> {

}
