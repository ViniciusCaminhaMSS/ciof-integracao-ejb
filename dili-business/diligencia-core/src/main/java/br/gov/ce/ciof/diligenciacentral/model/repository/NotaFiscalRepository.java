package br.gov.ce.ciof.diligenciacentral.model.repository;

import java.util.List;

import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;

public interface NotaFiscalRepository {

	public List<NotaFiscal> findNotasIntervaloNumeracao(Long numInicio, Long numFim);

}
