package br.gov.ce.ciof.diligenciacentral.model;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import br.gov.ce.ciof.diligenciacentral.model.entity.NotaFiscal;
import br.gov.ce.ciof.diligenciacentral.model.repository.NotaFiscalRepository;

public class NotaFiscalBoImpl implements NotaFiscalRepository {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<NotaFiscal> findNotasIntervaloNumeracao(Long numInicio, Long numFim) {
		TypedQuery<NotaFiscal> query = em.createQuery("SELECT nf FROM NotaFiscal nf WHERE nf.numeroChaveAcesso > :inicio  AND nf.numeroChaveAcesso < :fim",
				NotaFiscal.class);
		query.setParameter("inicio", numInicio);
		query.setParameter("fim", numFim);
		return query.getResultList();
	}
}
